package com.wio.worker.radio.redshift.dbutil;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties; 

//changed to 'final' as PMD report says class with private constructor should be declared as final
public final class ConnectionFactory 
{	
    private static ConnectionFactory instance = new ConnectionFactory();
    public static final String DRIVER_CLASS = "com.amazon.redshift.jdbc42.Driver"; 
 
    //private constructor
    private ConnectionFactory() 
    {
        try 
        {
            Class.forName(DRIVER_CLASS);
        } 
        catch (ClassNotFoundException e) 
        {
            e.printStackTrace();
        }
    }
 
    private Connection createConnection() 
    {
        Connection connection = null;
        ClassLoader classLoader = getClass().getClassLoader();
        try 
        {
        	Properties prop=new Properties();
        	//prop.load(classLoader.getResourceAsStream("/com/bayonette/waves/server/statusupdate/util/dbconnect.properties"));
        	prop.load(getClass().getResourceAsStream("/com/wio/worker/radio/redshift/dbutil/dbconnect.properties"));
        	//prop.load(getClass().getResourceAsStream("dbproperties/dbconnect.properties"));
            String url = prop.getProperty("jdbc.url");
            String user = prop.getProperty("jdbc.username");
            String password = prop.getProperty("jdbc.password");
            connection = DriverManager.getConnection(url, user, password);
        } 
        catch (SQLException e) {
            System.out.println("ERROR: Unable to Connect to Database.");
            e.printStackTrace();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return connection;
    }   
 
    public static Connection getConnection() 
    {
        return instance.createConnection();
    }
}