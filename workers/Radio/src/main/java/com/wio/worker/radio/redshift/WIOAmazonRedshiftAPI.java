package com.wio.worker.radio.redshift;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;

import com.wio.worker.radio.redshift.dbutil.ConnectionFactory;
import com.wio.worker.radio.request.RFMetrics;

public class WIOAmazonRedshiftAPI 
{
	private static WIOAmazonRedshiftAPI instance = null;
	/*static final String dbURL = "jdbc:redshift://wio-dev-instance.crxz3sqocgpg.us-east-1.redshift.amazonaws.com:5439/wiodb"; 
	static final String MasterUsername = "admin";
	static final String MasterUserPassword = "Admin123";*/

	public String save(RFMetrics metrics, Connection connection) throws Exception 
	{
		//Connection connection = null;
		PreparedStatement preparedStmt = null;
		//String sql = null;
		try{

			//Class.forName("com.amazon.redshift.jdbc42.Driver");
			System.out.println("Storing to Redshift...");
			/*Properties props = new Properties();

			props.setProperty("user", MasterUsername);
			props.setProperty("password", MasterUserPassword);*/
			
			connection = ConnectionFactory.getConnection();
			
			//Insert query
			//sql=  "insert into wio_rf_survey_sample values ("+metrics.getDeviceID()+", "+metrics.getiFace()+", "+metrics.getTime()+", "+metrics.getValue()+");";
			preparedStmt = connection.prepareStatement("insert into wio_rf_survey_sample values('"+metrics.getDeviceID()+"', '"+metrics.getiFace()+"', '"+metrics.getTime()+"', '"+metrics.getValue()+"');");
			
			preparedStmt.execute();
	        
			System.out.println("Value inserted in given table...");
			
			preparedStmt.close();
			connection.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(preparedStmt!=null)
					preparedStmt.close();
			}catch(Exception ex){
			}
			try{
				if(connection!=null)
					connection.close();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		System.out.println("Finished insert query.");	

        return metrics.getDeviceID();
	}
	
	public static WIOAmazonRedshiftAPI getInstance() 
	{
        if(instance == null) 
            instance = new WIOAmazonRedshiftAPI();

        return instance;
    }    
}
