package com.wio.worker.radio.request;

public class RFMetrics 
{
	private String deviceID;
	private String time;
	private String iFace;
	private String value;
	
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getiFace() {
		return iFace;
	}
	public void setiFace(String iFace) {
		this.iFace = iFace;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	

}
