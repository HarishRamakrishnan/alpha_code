package com.wio.worker.auth.handler;

import java.io.InputStream;
import java.io.OutputStream;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.wio.worker.auth.mqtt.MQTTSubscriber;

public class AuthWorkerHandler
{
	public static void invokeAuthWorker(InputStream request, OutputStream response, Context context)
	{
		LambdaLogger logger = context.getLogger();
		
		logger.log("Auth handler invoked...");
		
		MQTTSubscriber sub = new MQTTSubscriber();
		
		sub.connect();
		
		//MQTTPublisher.publishMessage("Woring fine...", "rajesh");
		
	}


}