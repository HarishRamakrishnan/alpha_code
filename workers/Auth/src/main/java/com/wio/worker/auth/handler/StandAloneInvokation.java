package com.wio.worker.auth.handler;

import com.wio.worker.auth.mqtt.MQTTSubscriber;

public class StandAloneInvokation 
{
	public static void main(String[] args) 
	{
		MQTTSubscriber sub = new MQTTSubscriber();		
		sub.connect();
	}
}
