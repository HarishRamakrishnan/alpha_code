package com.wio.microservice.framework.mqtt.client;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.impl.ConfigProtoData;
import com.wio.microservice.util.WIOConstants;

public class JMSMessageHandler {
	
	public static void main(String args[]){
		
		JMSPublisher pub = new JMSPublisher("configAck");
		
		Thread success = new Thread(new Runnable() {
			
			@Override
			public void run() {
				MqttMessage mesg = null;
				for(int i=0; i < 1 ; i++) {
					
					ConfigProtoData proto = constructProtoBufMesg();
					mesg = new MqttMessage();
					mesg.setPayload(proto.getConfigUpdateByteString().toByteArray());
					System.out.println("byte length = "+mesg.getPayload().length);
					pub.setMessage(mesg);
					pub.publishProtobufMesg();
				}		
			}
		});
		
		Thread failure = new Thread(new Runnable() {
			
			@Override
			public void run() {
				MqttMessage mesg = null;
				for(int i=0; i < 25 ; i++) {
					
					ConfigProtoData proto = constructFailureProtoBufMesg();
					mesg = new MqttMessage();
					mesg.setPayload(proto.getConfigUpdateByteString().toByteArray());
					System.out.println("byte length = "+mesg.getPayload().length);
					pub.setMessage(mesg);
					pub.publishProtobufMesg();
				}		
			}
		});

	success.start();
	//failure.start();
		
	}
	
	private static ConfigProtoData constructProtoBufMesg() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		ConfigProtoData proto = (ConfigProtoData) protocolType.getMesgSerializer(WIOConstants.CONFIG_UPDATE_PROTO);
		proto.messageConstructor();
		proto.setConfigUpdateDetails("Config", "12:20", "06aa74-f801-a401-b6ytCWMP7117", "Success", "ffc3e01a-dad1-4b76-b307-fc0453a73d4e");
		proto.setConfigParamDetails("network", "LAN");
		proto.serialize();
		String output = proto.deSerializeToString(proto.getConfigUpdateByteString().toByteArray());
		return proto;
	}
	
	private static ConfigProtoData constructFailureProtoBufMesg() {
		IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");
		ConfigProtoData proto = (ConfigProtoData) protocolType.getMesgSerializer(WIOConstants.CONFIG_UPDATE_PROTO);
		proto.messageConstructor();
		proto.setConfigUpdateDetails("Config", "1:39", "device"+Math.random(), "Failure", "ffc3e01a-dad1-4b76-b307-fc0453a73d4e");
		proto.setConfigParamDetails("MACADDR", "00:979:6667:jh989:jkshs:898");
		proto.setConfigParamDetails("Encryption", "PAS");
		proto.serialize();
		String output = proto.deSerializeToString(proto.getConfigUpdateByteString().toByteArray());
		return proto;
	}
	
	
}
