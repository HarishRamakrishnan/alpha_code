package com.wio.worker.mqtt.client;

import java.io.IOException;
import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.http.client.ClientProtocolException;

import com.wio.common.exception.DAOException;
import com.wio.common.protobuf.framework.IMessageSerializer;
import com.wio.common.protobuf.framework.MessageProtocolFactory;
import com.wio.common.protobuf.impl.WioLogProtoData;
import com.wio.worker.log.handler.SaveLogMessages;

public class RecieverRunner implements Runnable {

	private String taskName;
	private int latchCount;

	public RecieverRunner(String threadName, int latchCount) {
		taskName = threadName;
		Thread.currentThread().setName(taskName);
		this.latchCount = latchCount;
	}

	@Override
	public void run() {
		System.out.println(taskName+ " will be reading the mesg..");
		try {
			while (true) {

				JMSReciever recv = new JMSReciever(latchCount);
				System.out.println(taskName +" is going to process for the message :- "+recv.mesgRecieved(taskName));

				/*JsonReader jsonReader = Json.createReader(new StringReader(recv.mesgRecieved(taskName)));
				JsonObject message = jsonReader.readObject();*/

				try {
					IMessageSerializer protocolType = MessageProtocolFactory.getFactoryInst().getMessageProtocol("PROTPBUF");

					WioLogProtoData proto = (WioLogProtoData) protocolType.getMesgSerializer("WIO_LOG");      
					byte[] message=  recv.mesgRecieved(taskName);
					String logMessages = proto.deSerialize(message).getLog();
					System.out.println("Recieved the Logs : "+logMessages);
					String deviceId=proto.deSerialize(message).getDeviceid();
					SaveLogMessages.processMessage(logMessages,deviceId );
					//UpdateElasticSearch.processMessage(message, "/logs/");
				} catch (DAOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} /*s*/


				Thread.sleep(10);
				continue;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(taskName+ " Task executed..");
	}
}