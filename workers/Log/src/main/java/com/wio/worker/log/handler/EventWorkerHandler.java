package com.wio.worker.log.handler;

import java.io.InputStream;
import java.io.OutputStream;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.wio.worker.log.mqtt.MQTTSubscriber;

public class EventWorkerHandler
{
	public static void invokeEventWorker(InputStream request, OutputStream response, Context context)
	{
		LambdaLogger logger = context.getLogger();
		
		logger.log("Event handler invoked...");
		
		MQTTSubscriber sub = new MQTTSubscriber();
		
		sub.connect();
		
		//MQTTPublisher.publishMessage("Woring fine...", "rajesh");
		
	}


}