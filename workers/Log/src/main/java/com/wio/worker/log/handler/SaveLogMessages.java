package com.wio.worker.log.handler;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.json.JsonObject;

import org.aicer.grok.dictionary.GrokDictionary;
import org.aicer.grok.util.Grok;
import org.apache.http.client.ClientProtocolException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wio.common.config.model.Device;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.worker.auth.elasticsearch.HttpClientUtil;
import com.wio.worker.vo.Collect;
public class SaveLogMessages {
	
	final static String LOGSTASH_EXPRESSION = "^(?<timestamp>%{DAY} %{SPACE}%{MONTH} %{SPACE}%{MONTHDAY} %{SPACE}%{TIME} %{SPACE}%{YEAR})\\s+(%{WORD:module}.%{LOGLEVEL:loglevel}\\s+)+%{SYSLOGPROG:logsource}?%{GREEDYDATA:message}";
	

	public static void  processMessage(String logMessages ,String deviceId) throws ClientProtocolException, IOException, DAOException{
		System.out.println("In SaveLogMessages Processing of MQTT Message started." );
		if(logMessages!=null){

			String host = null;
			String deviceName = null;
			TreeMap<String, String> awsHeaders = new TreeMap<String, String>();
			GenericDAO  genDAO = GenericDAOFactory.getGenericDAO();
			System.out.println("The device Id is:"+deviceId);

			if(deviceId!=null ){
				
				Device device = genDAO.getGenericObject(Device.class, deviceId);
				if(device!=null){
					String serviceName = device.getServiceName();
					deviceName = device.getDeviceName();
					System.out.println("Device Id is found . "+"The serviceName is: "+serviceName +"the Device name is :"+deviceName);

					host=HttpClientUtil.getEndpoint(serviceName);		
					awsHeaders.put("host", host);	
						processNSaveLogMessage(logMessages ,deviceId ,host ,deviceName ,awsHeaders);
					
				}
				else{
					System.out.println("Device Id is not found in database .The id is :"+deviceId);
				}
			}
		}
		System.out.println("In SaveLogMessages Processing of MQTT Message Ends. " );
		

	}


	

	public static void processNSaveLogMessage(String logMessages, String deviceId, String host, String deviceName, TreeMap<String, String> awsHeaders) throws  IOException {
		System.out.println("In ProcessNSaveLogMessage method .");
		GrokDictionary dictionary = new GrokDictionary();
		dictionary.addBuiltInDictionaries();		
		dictionary.bind();	
		
		Grok compiledPattern = dictionary.compileExpression(LOGSTASH_EXPRESSION);

		String messages[] = logMessages.split("\n");
		
		for(String message:messages){

			Collect collect=getMessageObject(compiledPattern.extractNamedGroups(message.trim()));
			System.out.println("The Collect Object is : "+collect);

			System.out.println("Parsing Collect Objects In JSON starts.");
			
			ObjectMapper objectMapper = new ObjectMapper();
			String objToJson = objectMapper.writeValueAsString(collect);
			System.out.println("Parsing Collect Objects In JSON  ENDS. Resulting JSON is : "+objToJson);
			@SuppressWarnings("deprecation")
			String timeStamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date(collect.getTimestamp()));	
			//apending timestamp to device name to make a unique records each time
			String endpoint= "/logs" + "/"+deviceName+"/" + timeStamp;
			String elasticSearchEndpoint = "https://" + host + endpoint;
			
			System.out.println("Sending Parsed data to ES , Endpoint Is :"+elasticSearchEndpoint);
			HttpClientUtil.postRequest(objToJson, elasticSearchEndpoint, awsHeaders,endpoint);
			/*TreeMap<String, String> queryMap =new TreeMap();
			queryMap.put("deviceId", deviceName+timeStamp);
			GetESData.getRequest( "https://" + host + query, awsHeaders,queryMap);
*/
		}
		System.out.println("ProcessNSaveLogMessage END.");
		
	
	}
	private static Collect getMessageObject(final Map<String, String> results) {
		Collect collect=new Collect();
		if (results != null) {
			collect.setTimestamp(results.get("timestamp"));
			collect.setModule(results.get("module"));
			collect.setLoglevel(results.get("loglevel"));
			collect.setLogsource(results.get("logsource"));
			collect.setPid(results.get("pid"));
			collect.setMessage(results.get("message"));
		}
		return collect;
	}

}
