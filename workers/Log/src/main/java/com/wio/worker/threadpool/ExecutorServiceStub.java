package com.wio.worker.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import com.wio.worker.mqtt.client.JMSMessageHandler;
import com.wio.worker.mqtt.client.JMSPublisher;
import com.wio.worker.mqtt.client.RecieverRunner;

public class ExecutorServiceStub {
	
	JMSMessageHandler jmsHandler = new JMSMessageHandler();
	
	JMSPublisher pub = new JMSPublisher();
	
	public static void main(String args[]){
		ExecutorServiceStub stub = new ExecutorServiceStub();
		stub.testExecService();
	}

	private void testExecService() {
		WIOExecutorServiceCreation create = WIOExecutorServiceCreation.getInst();
		ExecutorService ex = create.createNewFixedThreadPool(5);
		WIOExecutorService service = new WIOExecutorService(ex);
		
		//service.execute(new PublisherRunner("Thread -5"));
		service.execute(new RecieverRunner("LMS-5",1));
		service.execute(new RecieverRunner("LMS-6",1));
		service.execute(new RecieverRunner("LMS-7",1));
		service.execute(new RecieverRunner("LMS-8",1));
		//service.execute(new RecieverRunner("Thread-5",1));
		
		
		//System.out.println("service is going to shutdown..");
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
