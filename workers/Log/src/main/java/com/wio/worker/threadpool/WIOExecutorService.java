package com.wio.worker.threadpool;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class WIOExecutorService {
	
	private ExecutorService exeService = null;
	
	public WIOExecutorService(final ExecutorService exeService){
		this.exeService = exeService;
	}

	public void execute(Runnable command) {
		exeService.execute(command);
	}

	public void shutdown() {
		exeService.shutdown();
	}

	public List<Runnable> shutdownNow() {
		System.out.println("Immediate shutdown will be called..");
		return exeService.shutdownNow();
	}

	public boolean isShutdown() {
		return exeService.isShutdown();
	}
	
	public boolean isTerminated() {
		boolean flag = false;
		
		if(!exeService.isTerminated()){
			flag = exeService.isTerminated();
			return flag;
		}else{
			return flag;
		}
	}

	public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
		return exeService.awaitTermination(timeout, unit);
	}

	public <T> Future<T> submit(Callable<T> task) {
		return exeService.submit(task);
	}

	public <T> Future<T> submit(Runnable task, T result) {
		return exeService.submit(task, result);
	}

	public Future<?> submit(Runnable task) {
		return exeService.submit(task);
	}

	

}
