package com.wio.worker.log.mqtt;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class MQTTSubscriber extends AbstractMqttClient implements MqttCallback
{	
	final static Logger logger = Logger.getLogger(MQTTSubscriber.class);

   public static AtomicLong count = new AtomicLong();   
   
   MqttClient client;
   MqttConnectOptions connOpt;
   MemoryPersistence persistence = new MemoryPersistence();
   
   public MQTTSubscriber()
   {		
	   super();
   }	   
	  	   
   public void connect()
   {
   	 try 
   	 {	   		 
   		 logger.info("Intiating Broker:: "+broker);
   		 client = new MqttClient(broker, clientId, persistence);
   		 
   		 logger.info("Setting up connection properties...");
	     this.connOpt = new MqttConnectOptions();
	     
	     // MQTT Configurations
	     //this.connOpt.setMqttVersion(4);
	     this.connOpt.setCleanSession(true);
	     //this.connOpt.setKeepAliveInterval(0);
	     this.connOpt.setAutomaticReconnect(true);
	     this.connOpt.setUserName(userName);
	     this.connOpt.setPassword(password.toCharArray());
	     
	     logger.info("Specifying callback...");
	     this.client.setCallback(this);
	     
	     logger.info("Connecting...");
	     this.client.connect(connOpt);
	     
	     logger.info("Connected ! Subscribing to topic: "+topic);
	     this.client.subscribe(topic, 1);
	     
	     logger.info("Subscription completed !");
   	 }
   	 catch(Exception error)
   	 {
   		logger.info("Exception occured while connecting to broker");
   	 }
   }

	   
   /**
	 * 
	 * connectionLost
	 * This callback is invoked upon losing the MQTT connection.
	 * 
	 */
	public void connectionLost(Throwable t) {
		logger.info("Connection lost!");
		logger.error(t.getStackTrace());
	}

	/**
	 * 
	 * deliveryComplete
	 * This callback is invoked when a message published by this client
	 * is successfully received by the broker.
	 * 
	 */
	public void deliveryComplete(IMqttDeliveryToken token) {
		//logger.info("Pub complete" + new String(token.getMessage().getPayload()));
		
	}
	
	//@Override
	public void connectComplete(boolean reconnect, java.lang.String serverURI){
		logger.info("Re-Connection Attempt " + reconnect);
		if(reconnect) {
			try {
				this.client.subscribe(topic, 1);
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * messageArrived
	 * This callback is invoked when a message is received on a subscribed topic.
	 * 
	 */
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		
		logger.info("-------------------------------------------------");
		logger.info("| Topic:" + topic);
		logger.info("| Message: " + new String(message.getPayload()));
		logger.info("-------------------------------------------------");
		
		/*logger.info("Publishing to topic 'confirmation'...");
		
		MQTTPublisher.publishMessage(message.toString(), "conf");*/
		
		count.incrementAndGet();
	}

}
