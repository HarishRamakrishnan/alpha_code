package com.wio.worker.auth.elasticsearch;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.amazonaws.services.elasticsearch.AWSElasticsearch;
import com.amazonaws.services.elasticsearch.AWSElasticsearchClientBuilder;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.DomainInfo;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesRequest;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesResult;
import com.wio.common.staticinfo.SystemConfigMessages;

public class HttpClientUtil {
	private static int msgCount;

	/*static String ACCESS_KEY = "aa";
	static String SECRET_KEY = "ss+LS8HSaVzyNVM9fMR";
*/
	public static void postRequest(String jsonObject ,String elasticSearchEndpoint ,TreeMap<String, String> awsHeaders , String queryURl ) throws ClientProtocolException, IOException{
	

		AWSV4Auth aWSV4Auth = new AWSV4Auth.Builder(System.getenv("ACCESS_KEY"),System.getenv("SECRET_KEY"))
				.regionName(SystemConfigMessages.SYSTEM_AWS_REGION)
				.serviceName("es") // es - elastic search. use your service name
				.httpMethodName("POST") //GET, PUT, POST, DELETE, etc...
				.canonicalURI(queryURl) //end point
				.queryParametes(null) //query parameters if any
				.awsHeaders(awsHeaders) //aws header parameters
				.payload(jsonObject) // payload if any
				.debug() // turn on the debug mode
				.build();

		HttpPost httpPost = new HttpPost(elasticSearchEndpoint);
		StringEntity requestEntity = new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON);
		httpPost.setEntity(requestEntity);

		/* Get header calculated for request */
		Map<String, String> header = aWSV4Auth.getHeaders();
		for (Map.Entry<String, String> entrySet : header.entrySet()) {

			httpPost.addHeader(entrySet.getKey(), entrySet.getValue());
		}
		httpPostRequest(httpPost);
	}
	
	static void httpPostRequest(HttpPost httpPost) throws ClientProtocolException, IOException {
		/* Create object of CloseableHttpClient */
		CloseableHttpClient httpClient = HttpClients.createDefault();

		/* Response handler for after request execution */
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				/* Get status code */
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					msgCount++;
					/* Convert response to String */
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			}
		};
		/* Execute URL and attach after execution response handler */
		String strResponse = httpClient.execute(httpPost, responseHandler);
		/* Print the response */
		System.out.println("Response: " + strResponse);
		System.out.println("No Of Messages Saved : "+msgCount);

	}
	public static String getEndpoint(String service) {
		AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
				.withRegion("us-east-1").build();

		ListDomainNamesResult listDomainNamesResult = awsElasticsearch.listDomainNames(new ListDomainNamesRequest());
		String endPoint = "";

		for (DomainInfo domain : listDomainNamesResult.getDomainNames()) {

			if(domain.toString().contains(service)){
				DescribeElasticsearchDomainResult esDomainResult = awsElasticsearch.describeElasticsearchDomain(
						new DescribeElasticsearchDomainRequest().withDomainName(domain.getDomainName()));	
				endPoint = esDomainResult.getDomainStatus().getEndpoint();
				break;
			}
		}	
		return endPoint;
	}
}

