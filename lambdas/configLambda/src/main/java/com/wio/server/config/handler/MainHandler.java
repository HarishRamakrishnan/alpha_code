package com.wio.server.config.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sns.model.InternalErrorException;
import com.google.gson.Gson;
import com.wio.common.config.model.ConfiguredValuesMO;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.protobuf.impl.WIOConfigData;
import com.wio.template.mo.DefaultMO;
import com.wio.template.mo.DhcpLanMO;
import com.wio.template.mo.DhcpMO;
import com.wio.template.mo.DhcpWanMO;
import com.wio.template.mo.FirewallMO;
import com.wio.template.mo.ForwardingMO;
import com.wio.template.mo.InterfaceMO;
import com.wio.template.mo.LanMO;
import com.wio.template.mo.NetworkMO;
import com.wio.template.mo.RadioMO;
import com.wio.template.mo.ScriptMO;
import com.wio.template.mo.VlanMO;
import com.wio.template.mo.WanMO;
import com.wio.template.mo.WirelessMO;
import com.wio.template.mo.ZonesMO;
import com.wio.template.mqtt.JMSMessageHandler;
import com.wio.template.mqtt.JMSPublisher;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import freemarker.template.Version;

public class MainHandler {

	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	static DynamoDBMapper mapper = new DynamoDBMapper(client);
	
	static String CONFIG_UPDATES = "CONFIG_UPDATES";
	static String CONFIG_TEMPLATES = "CONFIG_TEMPLATES";

	static LambdaLogger logger = null;

//	createItems();
	
	public static void main(String[] args) {
		try {
			
			String transactionId = "";

			ConfiguredValuesMO configuredValuesMO = retrieveItem(transactionId);

			DeviceConfiguration deviceConfiguration = configuredValuesMO.getConfiguration();

			ScriptMO templateMO = deviceToScriptMO(deviceConfiguration);

			Map<String, Object> templateData = new HashMap<String, Object>();
			templateData.put("network", templateMO.getNetwork());
			templateData.put("wireless", templateMO.getWireless());
			templateData.put("dhcp", templateMO.getDhcp());
			templateData.put("firewall", templateMO.getFirewall());

			getTemplate(configuredValuesMO.getPlatform(), configuredValuesMO.getActionType(), configuredValuesMO.getFwVersion());

			StringWriter out = generateTemplate(templateData);

			String script = (out.getBuffer().toString());
			System.out.println(script);
			
			JMSPublisher pub = new JMSPublisher("defaulttest");

			WIOConfigData proto = JMSMessageHandler.constructProtoBufMesg(transactionId, script, configuredValuesMO.getActionType());
			MqttMessage mesg = new MqttMessage();
			mesg.setPayload(proto.getWIOConfigByteString().toByteArray());

			pub.setMessage(mesg);
			pub.publishProtobufMesg();

			out.flush();
		} catch (InternalErrorException e) {
//			logger.log(e.toString());
		} catch (IOException e) {
//			logger.log(e.toString());
		} catch (TemplateException e) {
//			logger.log(e.toString());
		}
//		return "Success";
	}

	private static void getTemplate(String platform, String actionType, String version) throws IOException {

		Table table = dynamoDB.getTable("CONFIG_TEMPLATES");

        Map<String, Object> eav = new HashMap<String, Object>();
        eav.put(":val1", platform);
        eav.put(":val2", actionType);
        eav.put(":val3", version);
        
        ItemCollection<ScanOutcome> items = table.scan(
        		"PLATFORM = :val1 AND ACTION_TYPE = :val2 AND VERSION = :val3",
                "TEMPLATE",
                null,
                eav);
        
        String iterator = items.iterator().next().getString("TEMPLATE");
        
		ByteArrayInputStream templateStream = new ByteArrayInputStream(iterator.getBytes());
		
		Path path = Paths.get("src/main/resources/template/temp.ftl");
		Files.copy(templateStream, path, StandardCopyOption.REPLACE_EXISTING);
	}

	private static StringWriter generateTemplate(Map<String, Object> templateData) 
			throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {

		Configuration cfg = new Configuration(new Version("2.3.23"));

		cfg.setClassForTemplateLoading(MainHandler.class, "/template");
		cfg.setDefaultEncoding("UTF-8");

		Template template = null;

		StringWriter out = new StringWriter();

		template = cfg.getTemplate("temp.ftl");
		template.process(templateData, out);
		
		return out;
	}

	private static ConfiguredValuesMO retrieveItem(String transactionId) {

		ConfiguredValuesMO configuredValuesMO = mapper.load(ConfiguredValuesMO.class, transactionId);   
		return configuredValuesMO;
	}

	static private String javaToJson(ScriptMO obj) {
		Gson gson = new Gson();
		return gson.toJson(obj);
	}

	static private ScriptMO jsonToJava(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, ScriptMO.class);
	}

	private static String createItems() {

		ScriptMO templateMO = setData();
		
		ConfiguredValuesMO configuredValuesMO = new ConfiguredValuesMO();
		configuredValuesMO.setDeviceId("D2");
//		configuredValuesMO.setConfiguration(javaToJson(templateMO));

		mapper.save(configuredValuesMO);   
		
		return (configuredValuesMO.getTransactionId());
	}

	static private ScriptMO setData() {

		LanMO lanMO = new LanMO();
//		lanMO.setIfname("eth0.1");
//		lanMO.setIp6assign("60");
//		lanMO.setIpaddr("192.168.1.1");
//		lanMO.setNetmask("255.255.255.0");
//		lanMO.setProto("static");
//		lanMO.setType("bridge");
		lanMO.setIndex("0");
//
		WanMO wanMO = new WanMO();
//		wanMO.setIfname("eth0.2");
//		wanMO.setProto("dhcp");
		wanMO.setIndex("0");
//
		VlanMO vlanMO = new VlanMO();
//		vlanMO.setDevice("switch0");
//		vlanMO.setPorts("1");
//		vlanMO.setVlan("0 1 2 3 6t");
		vlanMO.setIndex("0");

		RadioMO radioMO = new RadioMO();
//		radioMO.setChannel("11");
//		radioMO.setDisabled("1s");
//		radioMO.setHtmode("HT20");
//		radioMO.setHwmode("11g");
//		radioMO.setType("mac80211");
		radioMO.setIndex("0");
//
		InterfaceMO interfaceMO = new InterfaceMO();
//		interfaceMO.setDevice("radio0");
//		interfaceMO.setEap_type("SIM");
//		interfaceMO.setEncryption("wpa2");
//		interfaceMO.setMode("ap");
//		interfaceMO.setName("lan");
//		interfaceMO.setNetwork("lan");
		interfaceMO.setSsid("TEST SSID");
		interfaceMO.setIndex("0");
//
		DhcpLanMO dhcpLanMO = new DhcpLanMO();
//		dhcpLanMO.setDhcpv6("server");
//		dhcpLanMO.setIface("lan");
//		dhcpLanMO.setLeasetime("4h");
//		dhcpLanMO.setLimit("150");
//		dhcpLanMO.setRa("server");
//		dhcpLanMO.setStart("100");
		dhcpLanMO.setIndex("0");
//
		DhcpWanMO dhcpWanMO = new DhcpWanMO();
//		dhcpWanMO.setIface("wan");
//		dhcpWanMO.setIgnore("1");
		dhcpWanMO.setIndex("0");
//
		DefaultMO defaultMO = new DefaultMO();
//		defaultMO.setForward("REJECT");
//		defaultMO.setInput("ACCEPT");
//		defaultMO.setOutput("ACCEPT");
//		defaultMO.setSyn_flood("1");
		defaultMO.setIndex("0");
//
		ZonesMO zonesMO = new ZonesMO();
//		zonesMO.setForward("ACCEPT");
//		zonesMO.setName("lan");
//		zonesMO.setNetwork("lan");
//		zonesMO.setOutput("ACCEPT");
//		zonesMO.setInput("ACCEPT");
		zonesMO.setIndex("0");
//
		ForwardingMO forwardingMO = new ForwardingMO();
//		forwardingMO.setDest("wan");
//		forwardingMO.setSrc("lan");
		forwardingMO.setIndex("0");

		ScriptMO templateMO = new ScriptMO();

//		templateMO.getNetwork().getLans().add(lanMO);
//		templateMO.getNetwork().getWans().add(wanMO);
//		templateMO.getNetwork().getVlans().add(vlanMO);
//
//		templateMO.getWireless().getRadios().add(radioMO);
		templateMO.getWireless().getInterfaces().add(interfaceMO);
//
//		templateMO.getDhcp().getDhcpLans().add(dhcpLanMO);
//		templateMO.getDhcp().getDhcpWans().add(dhcpWanMO);
//
//		templateMO.getFirewall().getDefaults().add(defaultMO);
//		templateMO.getFirewall().getZones().add(zonesMO);
//		templateMO.getFirewall().getForwardings().add(forwardingMO);

		return templateMO;
	}

	private static ScriptMO deviceToScriptMO(DeviceConfiguration deviceConfiguration) {
		ScriptMO scriptMO = new ScriptMO();
		
		NetworkMO networkMO = new NetworkMO();
		
		List<LanMO> lans = new ArrayList<LanMO>();
		for (LANSettings iterableElement : deviceConfiguration.getLanSettings()) {
			LanMO lanMO = new LanMO();
			
			lanMO.setIfname(iterableElement.getIfName());
			lanMO.setIp6assign("");
			lanMO.setIpaddr(iterableElement.getIpV4());
			lanMO.setNetmask(iterableElement.getSubnetMask());
			lanMO.setProto(iterableElement.getDhcp());
			lanMO.setType("");
			lanMO.setIndex(iterableElement.getIndex());
			
			lans.add(lanMO);
		}
		
		networkMO.setLans(lans);
		
		List<WanMO> wans = new ArrayList<WanMO>();
		WANSettings wanSettings = deviceConfiguration.getWanSetting();
		
		WanMO wanMO = new WanMO();
		wanMO.setIfname(wanSettings.getIfName());
		wanMO.setProto(wanSettings.getWanType());
		wanMO.setIndex("");
		
		wans.add(wanMO);
		
		networkMO.setWans(wans);
		
		
		List<VlanMO> vlanMOs = new ArrayList<VlanMO>();
		for (LANSettings iterableElement : deviceConfiguration.getLanSettings()) {	// edit needed
			VlanMO vlanMO = new VlanMO();
			
			vlanMO.setDevice("");
			vlanMO.setPorts("");
			vlanMO.setVlan("");
			vlanMO.setIndex(iterableElement.getIndex());
			
			vlanMOs.add(vlanMO);
		}
		
		networkMO.setVlans(vlanMOs);
		
		scriptMO.setNetwork(networkMO);
		
		WirelessMO wireless = new WirelessMO();
		
		List<RadioMO> radios = new ArrayList<RadioMO>();
		for (DeviceRadioSettings iterableElement : deviceConfiguration.getRadioSettings()) {
			RadioMO radioMO = new RadioMO();
			
			radioMO.setChannel(iterableElement.getChannel());
			radioMO.setDisabled(iterableElement.getRadioStatus());
			radioMO.setHtmode(iterableElement.getBandwidth());
			radioMO.setHwmode(iterableElement.getRadioMode());
			radioMO.setType("");
			radioMO.setIndex(iterableElement.getIndex());
			
			radios.add(radioMO);
		}
		wireless.setRadios(radios);

		List<InterfaceMO> interfaces = new ArrayList<InterfaceMO>();
		for (WiFiSettings iterableElement : deviceConfiguration.getWifiSettings()) {
			InterfaceMO interfaceMO = new InterfaceMO();
			
			interfaceMO.setDevice("");
			interfaceMO.setEap_type(iterableElement.getEapType());
			interfaceMO.setEncryption(iterableElement.getAuthenticationType());
			interfaceMO.setMode(iterableElement.getMode());
			interfaceMO.setName(iterableElement.getWifiName());
			interfaceMO.setNetwork("");
			interfaceMO.setSsid(iterableElement.getSsid());
			interfaceMO.setIndex(iterableElement.getIndex());
			
			interfaces.add(interfaceMO);
		}
		wireless.setInterfaces(interfaces);
		
		scriptMO.setWireless(wireless);
		
		DhcpMO dhcp = new DhcpMO();
		
		List<DhcpLanMO> dhcpLans = new ArrayList<DhcpLanMO>();
		for (WiFiSettings iterableElement : deviceConfiguration.getWifiSettings()) {
			DhcpLanMO dhcpLanMO = new DhcpLanMO();
			
			dhcpLanMO.setDhcpv6("");
			dhcpLanMO.setIface("");
			dhcpLanMO.setLeasetime("");
			dhcpLanMO.setLimit("");
			dhcpLanMO.setRa("");
			dhcpLanMO.setStart("");
			dhcpLanMO.setIndex(iterableElement.getIndex());
			
			dhcpLans.add(dhcpLanMO);
		}		
		dhcp.setDhcpLans(dhcpLans);
		
		List<DhcpWanMO> dhcpWans = new ArrayList<DhcpWanMO>();
		for (WiFiSettings iterableElement : deviceConfiguration.getWifiSettings()) {
			DhcpWanMO dhcpWanMO = new DhcpWanMO();
						
			dhcpWanMO.setIface("");
			dhcpWanMO.setIgnore("");
			dhcpWanMO.setIndex(iterableElement.getIndex());
			
			dhcpWans.add(dhcpWanMO);
		}
		dhcp.setDhcpWans(dhcpWans);
		
		scriptMO.setDhcp(dhcp);
		
		FirewallMO firewall = new FirewallMO();
		
		List<DefaultMO> defaults = new ArrayList<DefaultMO>();
		for (WiFiSettings iterableElement : deviceConfiguration.getWifiSettings()) {
			DefaultMO  defaultMO = new DefaultMO();
			
		    defaultMO.setForward("");
		    defaultMO.setInput("");
			defaultMO.setOutput("");
		    defaultMO.setSyn_flood("");
			defaultMO.setIndex(iterableElement.getIndex());
			
			defaults.add(defaultMO);
		}
		firewall.setDefaults(defaults);
		
		
		List<ZonesMO> zones = new ArrayList<ZonesMO>();
		for (WiFiSettings iterableElement : deviceConfiguration.getWifiSettings()) {
			ZonesMO zonesMO = new ZonesMO();
		
			zonesMO.setForward("");
			zonesMO.setName("");
			zonesMO.setNetwork("");
			zonesMO.setOutput("");
			zonesMO.setInput("");
			zonesMO.setIndex(iterableElement.getIndex());
			
			zones.add(zonesMO);
		}
		firewall.setZones(zones);
		List<ForwardingMO> forwardings = new ArrayList<ForwardingMO>();
		for (WiFiSettings iterableElement : deviceConfiguration.getWifiSettings()) {
			ForwardingMO forwardingMO = new ForwardingMO();
			
			forwardingMO.setDest("");
			forwardingMO.setSrc("");
			forwardingMO.setIndex(iterableElement.getIndex());
			
			forwardings.add(forwardingMO);
		}
		firewall.setForwardings(forwardings);
		scriptMO.setFirewall(firewall);
		return scriptMO;
	}
	
}
