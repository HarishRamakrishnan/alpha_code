package com.wio.server.config.handler;

//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.eclipse.paho.client.mqttv3.MqttClient;
//
//import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDB;

//import com.amazonaws.services.lambda.runtime.Context;
//import com.amazonaws.services.lambda.runtime.LambdaLogger;
//import com.amazonaws.services.lambda.runtime.RequestHandler;
//import com.google.gson.Gson;
//import com.wio.common.Enums.Constants;
//import com.wio.common.aws.api.es.ElasticSearchAPI;
//import com.wio.common.aws.api.lambda.LambdaClientAPI;
//import com.wio.common.config.api.DeviceConfigurationAPI;
//import com.wio.common.config.request.DeviceConfigurationRequest;
//import com.wio.common.exception.BadRequestException;
//import com.wio.common.exception.InternalErrorException;
//import com.wio.common.mqtt.MQTTConnection;
//import com.wio.common.mqtt.MQTTPublisher;
//import com.wio.common.validation.RequestValidator;

//public class ConfigLambdaHandler implements RequestHandler<DynamoDB, Integer> {
//
//	@Override
//	public Integer handleRequest(DynamoDB arg0, Context arg1) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	private static LambdaLogger logger = null;
//	
//    @Override
//	public Integer handleRequest(DynamodbEvent event, Context context) {
//    	logger = context.getLogger();
//    	
//        List<Item> listOfItem = new ArrayList<>();
//        List<Map<String, AttributeValue>> listOfMaps = null;
//        
//		for(DynamodbStreamRecord record : event.getRecords()) {
//			
//            listOfMaps = new ArrayList<Map<String, AttributeValue>>();
//            listOfMaps.add(record.getDynamodb().getNewImage());
//            listOfItem = InternalUtils.toItemList(listOfMaps);
//            
//            System.out.println(listOfItem);
//
//            Item item = listOfItem.get(0);
//                
//			String updatedBy = item.asMap().get("UPDATED_BY").toString();
//			String deviceID = item.asMap().get("DEVICE_ID").toString();
//			String domainName = item.asMap().get("SERVICE_NAME").toString();
//			
//			boolean devVal = RequestValidator.isValidAlphaNumaricWithHypenFormat(deviceID);
//			boolean updVal = Constants.Source.contains(updatedBy);
//			
//			System.out.println("devVal = " +devVal + " updVal = " +updVal);
//			logger.log("devVal = " +devVal + " updVal = " +updVal);
//			
//			if ( RequestValidator.isValidAlphaNumaricWithHypenFormat(deviceID) && (Constants.Source.contains(updatedBy)) ) {
//				
//				logger.log("Inside if for fetching Device config");
//				System.out.println("Inside if for fetching Device config");
//				String newDeviceConfig = "";
//				try {
//					newDeviceConfig = DeviceConfigurationAPI.fetchConfig(deviceID, context);
//					logger.log("Device Config is " +newDeviceConfig);
//					System.out.println("Device Config is " +newDeviceConfig);
//					if(RequestValidator.isEmptyField(newDeviceConfig)) {
//						continue;
//					}
//				} catch (InternalErrorException e1) {
//					e1.printStackTrace();
//					continue;
//				} catch (BadRequestException e1) {
//					e1.printStackTrace();
//					continue;
//				}
//				
//				String mqttResponse = "";
//				String elasticResponse = "";
//				String cwmpResponse = "";
//				
//				switch (updatedBy) {
//
//				case "CWMP":
//
//					logger.log("Publishing to Broker...");
//					mqttResponse = publishMessage(newDeviceConfig);
//					
//					if (mqttResponse.equals("SUCCESS")) {
//						logger.log("Updating to ElasticSearch...");
//						try {
//							elasticResponse = updateES(deviceID, domainName, newDeviceConfig);
//							
//							if (elasticResponse.equals("FAILURE")) {
//								logger.log("Error while updating to ElasticSearch for device " +deviceID + ". Proceeding with configuration for next device.");
//							}
//						} catch (UnsupportedEncodingException uee) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - UnsupportedEncodingException\n" + uee);
//							continue;
//						} catch (ClientProtocolException cpe) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - ClientProtocolException\n" + cpe);
//							continue;
//						} catch (IOException e) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - IOException\n" + e);
//							continue;
//						} 
//					} else {
//						logger.log("Error while publishing message to MQTT. Proceeding with configuration for next device. ");
//						continue;
//					}
//					
//					break;
//
//				case "RESTAPI":
//
//					logger.log("Publishing to Broker...");
//					mqttResponse = publishMessage(newDeviceConfig);
//					
//					if (mqttResponse.equals("SUCCESS")) {
//						logger.log("Updating to ElasticSearch...");
//						try {
//							elasticResponse = updateES(deviceID, domainName, newDeviceConfig);
//							
//							if (elasticResponse.equals("FAILURE")) {
//								logger.log("Error while updating to ElasticSearch for device " +deviceID + ". Proceeding with configuration for next device.");
//								continue;
//							}
//						} catch (UnsupportedEncodingException uee) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - UnsupportedEncodingException\n" + uee);
//							continue;
//						} catch (ClientProtocolException cpe) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - ClientProtocolException\n" + cpe);
//							continue;
//						} catch (IOException e) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - IOException\n" + e);
//							continue;
//						}
//					} else {
//						logger.log("Error while publishing message to MQTT. Proceeding with configuration for next device. ");
//						continue;
//					}
//					
//					if (elasticResponse.equals("SUCCESS")) {
//						logger.log("Invoking CWMP Client...");
//						cwmpResponse = invokeCWMP(deviceID);
//						if (cwmpResponse.equals("FAILURE")) {
//							logger.log("Error while invoking CWMPLambda. Proceeding with configuration for next device. ");
//							continue;
//						}
//					}
//					break;
//
//				case "AP":
//
//					logger.log("Updating to ElasticSearch...");
//					try {
//						elasticResponse = updateES(deviceID, domainName, newDeviceConfig);
//						
//						if (elasticResponse.equals("FAILURE")) {
//							logger.log("Error while updating to ElasticSearch for device " +deviceID + ". Proceeding with configuration for next device.");
//							continue;
//						}
//						
//					} catch (UnsupportedEncodingException uee) {
//						logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - UnsupportedEncodingException\n" + uee);
//						continue;
//					} catch (ClientProtocolException cpe) {
//						logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - ClientProtocolException\n" + cpe);
//						continue;
//					} catch (IOException e) {
//						logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - IOException\n" + e);
//						continue;
//					}
//
//					if (elasticResponse.equals("SUCCESS")) {
//						logger.log("Invoking CWMP Client...");
//						cwmpResponse = invokeCWMP(deviceID);
//						if (cwmpResponse.equals("FAILURE")) {
//							logger.log("Error while invoking CWMPLambda. Proceeding with configuration for next device. ");
//							continue;
//						}
//					}
//					break;
//
//				case "UI":
//
//					logger.log("Publishing to Broker...");
//					mqttResponse = publishMessage(newDeviceConfig);
//					
//					if (mqttResponse.equals("SUCCESS")) {
//						logger.log("Updating to ElasticSearch...");
//						try {
//							elasticResponse = updateES(deviceID, domainName, newDeviceConfig);
//							
//							if (elasticResponse.equals("FAILURE")) {
//								logger.log("Error while updating to ElasticSearch for device " +deviceID + ". Proceeding with configuration for next device.");
//								continue;
//							}
//						} catch (UnsupportedEncodingException uee) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - UnsupportedEncodingException\n" + uee);
//							continue;
//						} catch (ClientProtocolException cpe) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - ClientProtocolException\n" + cpe);
//							continue;
//						} catch (IOException e) {
//							logger.log("Catch block - ConfigLambda - handleRequest() - updateES() - IOException\n" + e);
//							continue;
//						}
//					}else {
//						logger.log("Error while publishing message to MQTT. Proceeding with configuration for next device. ");
//						continue;
//					}
//					
//					if (elasticResponse.equals("SUCCESS")) {
//						logger.log("Invoking CWMP Client...");
//						cwmpResponse = invokeCWMP(deviceID);
//						if (cwmpResponse.equals("FAILURE")) {
//							logger.log("Error while invoking CWMPLambda. Proceeding with configuration for next device. ");
//							continue;
//						}
//					}
//					break;
//				}
//			
//			}
//			
//		}
//		
//		return event.getRecords().size();
//	}
//
//	private String invokeCWMP(String deviceID) {
//		String source = "ConfigLambda";
//		String lambdaFunctionName = "CWMPLambda";
//		String deviceJson = "{ \"params\": { \"path\": {}, \"header\": { \"userName\": \"\", \"password\": \"\", \"calledBy\": \"" +source +"\" }, \"querystring\": { \"deviceID\": \"" +deviceID + "\" } } }";
//		logger.log(deviceJson);
//		return LambdaClientAPI.invoke(lambdaFunctionName, deviceJson);
//	}
//
//	private String updateES(String deviceID, String domainName, String newDeviceConfig)
//			throws UnsupportedEncodingException, ClientProtocolException, IOException {
//		String domainEndPoint = ElasticSearchAPI.getESEndpoint(domainName);
//		if (!RequestValidator.isEmptyField(domainEndPoint)) {
//			String elasticSearchEndpoint = "https://" +domainEndPoint +"/config/devices/" +deviceID;
//			StringEntity payload = new StringEntity(newDeviceConfig.toString());
//			HttpPost httpPost = new HttpPost(elasticSearchEndpoint);
//			httpPost.setEntity(payload);
//			return ElasticSearchAPI.updateES(httpPost);
//		} else {
//			logger.log("Endpoint for domain " +domainName +" is empty. Unable to update Elastic Search for  device " +deviceID);
//			return "FAILURE";
//		}
//	}
//
//	private String publishMessage(String newDeviceConfig) {
//		String clientID = "configLambda";
//		//String topic = "configLambda";
//		int qos = 1;
//		Gson gson = new Gson();
//		DeviceConfigurationRequest deviceConfigReq = gson.fromJson(newDeviceConfig, DeviceConfigurationRequest.class);
//		String topic = deviceConfigReq.getDefaultTopic();
//		logger.log("MQTT Topic is " +topic);
//		if (!RequestValidator.isEmptyField(topic)) {
//			MQTTConnection mqttconn = new MQTTConnection();
//			MqttClient mqttClient = mqttconn.getConnection(clientID, 0, false, false);
//			return MQTTPublisher.publishMessage(mqttClient, newDeviceConfig, topic, qos, true);
//		} else {
//			logger.log("Topic is empty. Unable to publish the message");
//			return "FAILURE";
//		}
//	}
//	
//}