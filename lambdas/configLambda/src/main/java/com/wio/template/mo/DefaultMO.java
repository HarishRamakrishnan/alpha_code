package com.wio.template.mo;

public class DefaultMO {
	
	public String syn_flood;	
	public String input;	
	public String output;	
	public String forward;
	public String index;
	
	public String getSyn_flood() {
		return syn_flood;
	}
	public void setSyn_flood(String syn_flood) {
		this.syn_flood = syn_flood;
	}
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}	
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}

	
	
}
