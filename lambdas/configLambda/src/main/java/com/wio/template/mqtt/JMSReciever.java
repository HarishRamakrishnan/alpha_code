package com.wio.template.mqtt;

import java.util.concurrent.CountDownLatch;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.wio.common.mqtt.MQTTConnection;

public class JMSReciever {

	private MqttClient mqttClient = null;
	// Latch used for synchronizing b/w threads
	private CountDownLatch latch = null;
	// Topic filter the client will subscribe to
	private String subTopic = "$share/config/updateAck";
	private String strMessage = "";
	private byte[] mesgBytes = null;
	private MqttMessage mqttMessage = null;
	
	public JMSReciever(String topic, int countDown,String clientID){
		this.subTopic = topic;
		latch = new CountDownLatch(countDown);
		Thread.currentThread().setName(clientID);
	}
	
	public String mesgRecieved(final String clientID){
		//mqttClient = MQTTConnection.getConnection("Test", 50, true, false);
		MQTTConnection conn = new MQTTConnection();
		mqttClient = conn.getConnection(clientID, 50, true, false);
		try {
			mqttClient.setCallback(new MqttCallback() {
				
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					mqttMessage = message;
					mesgBytes = new byte[message.getPayload().length];
					strMessage = new String(message.getPayload());
					System.out.println(Thread.currentThread().getName()+" MQttMesg ==>>>>>>>>>>>>>"+mqttMessage.getPayload().length);
			        System.out.println(Thread.currentThread().getName()+"|"+clientID+ "\tReceived a Message!" +
			            "\tTime:    " + System.currentTimeMillis() +
			            "\tTopic:   " + topic + 
			            "\tQoS:     " + message.getQos() + "\n");
			        
			        latch.countDown();
				}
				
				public void deliveryComplete(IMqttDeliveryToken arg0) {
					
				}
				
				public void connectionLost(Throwable cause) {
					System.out.println(Thread.currentThread().getName()+"|"+clientID+ cause.getMessage());
					latch.countDown();						
				}
			});
			
			// Subscribe client to the topic filter and a QoS level of 0
            System.out.println(Thread.currentThread().getName()+"|"+clientID+" Subscribing client to topic: " + subTopic);
            mqttClient.subscribe(subTopic, 1);

            // Wait for the message to be received
            try {
                latch.await(); 
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName()+"|"+clientID+" someone awaked me!!");
            }
            
            if(mqttClient.isConnected()){
            	// Disconnect the client
            	mqttClient.disconnect();
            }
            
            System.out.println(Thread.currentThread().getName()+"|"+clientID+" Exiting..");
            return strMessage;
            //System.exit(0);
		} catch (MqttException e) {
			try {
				mqttClient.disconnect();
			} catch (MqttException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return strMessage;
	}
	
	public byte[] getMesgBytes() {
		return mesgBytes;
	}

	public MqttMessage getMqttMessage() {
		return mqttMessage;
	}
	
	public String getStringMessage() {
		return strMessage;
	}

}
