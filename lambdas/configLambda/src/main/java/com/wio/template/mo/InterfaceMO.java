package com.wio.template.mo;

public class InterfaceMO {

	public String device;
	public String network;
	public String mode;
	public String ssid;
	public String encryption;
	public String eap_type;
	public String name;
	public String index;
	
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.name = device.split("radio")[0];
		this.device = device;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getEncryption() {
		return encryption;
	}
	public void setEncryption(String encryption) {
		this.encryption = encryption;
	}
	public String getEap_type() {
		return eap_type;
	}
	public void setEap_type(String eap_type) {
		this.eap_type = eap_type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	
	
}
