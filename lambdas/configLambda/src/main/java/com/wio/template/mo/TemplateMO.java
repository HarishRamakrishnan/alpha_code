package com.wio.template.mo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "CONFIG_TEMPLATES")
public class TemplateMO {

	private String platform;
	private String version;
	private String template;
	private String templateId;
	private String actionType;
	
	@DynamoDBHashKey(attributeName = "TEMPLATE_ID")
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	@DynamoDBAttribute(attributeName = "PLATFORM")
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	@DynamoDBAttribute(attributeName = "VERSION")
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@DynamoDBAttribute(attributeName = "TEMPLATE")
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	@DynamoDBAttribute(attributeName = "ACTION_TYPE")
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}	
}
