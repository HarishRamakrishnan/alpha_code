package com.wio.template.mo;

import java.util.ArrayList;
import java.util.List;

public class FirewallMO {

	public List<DefaultMO> defaults = new ArrayList<DefaultMO>();
	public List<ZonesMO> zones = new ArrayList<ZonesMO>();
	public List<ForwardingMO> forwardings = new ArrayList<ForwardingMO>();
	
	public List<DefaultMO> getDefaults() {
		return defaults;
	}
	public void setDefaults(List<DefaultMO> defaults) {
		this.defaults = defaults;
	}
	public List<ZonesMO> getZones() {
		return zones;
	}
	public void setZones(List<ZonesMO> zones) {
		this.zones = zones;
	}
	public List<ForwardingMO> getForwardings() {
		return forwardings;
	}
	public void setForwardings(List<ForwardingMO> forwardings) {
		this.forwardings = forwardings;
	}
	
	
	
}
