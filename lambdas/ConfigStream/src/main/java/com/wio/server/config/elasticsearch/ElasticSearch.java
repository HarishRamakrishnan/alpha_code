package com.wio.server.config.elasticsearch;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.amazonaws.services.elasticsearch.AWSElasticsearch;
import com.amazonaws.services.elasticsearch.AWSElasticsearchClientBuilder;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.DomainInfo;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesRequest;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesResult;

public class ElasticSearch {


	public static void update(JSONObject newImage) throws ClientProtocolException, IOException{
        
		String serviceName = newImage.getJSONObject("SERVICE_NAME").getString("s");
		String deviceName = newImage.getJSONObject("DEVICE_NAME").getString("s");
		
		String elasticSearchEndpoint = "https://" + getEndpoint(serviceName) + "/config/devices/" + deviceName;
//
//		JSONObject newImageMap = new JSONObject();
//		
//		newImageMap.put("SERVICE_NAME", newImage.getJSONObject("SERVICE_NAME").getString("s"));
//		newImageMap.put("HP_NAME", newImage.getJSONObject("HP_NAME").getString("s"));
//		newImageMap.put("HNCP", newImage.getJSONObject("HNCP").getString("s"));
//		newImageMap.put("STATUS", newImage.getJSONObject("STATUS").getString("s"));
//		newImageMap.put("CREATION_TIME", newImage.getJSONObject("CREATION_TIME").getString("s")); 
//		newImageMap.put("LAST_UPDATE_TIME", newImage.getJSONObject("LAST_UPDATE_TIME").getString("s"));
//		newImageMap.put("DEVICE_NAME", newImage.getJSONObject("DEVICE_NAME").getString("s"));
//		newImageMap.put("MODEL", newImage.getJSONObject("MODEL").getString("s"));
//		newImageMap.put("UUID", newImage.getJSONObject("UUID").getString("s"));
//		newImageMap.put("SQS_URL", newImage.getJSONObject("SQS_URL").getString("s"));
//		newImageMap.put("MAC_ADDRESS", newImage.getJSONObject("MAC_ADDRESS").getString("s"));
//
//		System.out.println(newImage);
//		System.out.println(newImageMap);
//		System.out.println(elasticSearchEndpoint);
		StringEntity payload = new StringEntity(newImage.toString());

		HttpPost httpPost = new HttpPost(elasticSearchEndpoint);
        httpPost.setEntity(payload);

        httpPostRequest(httpPost);
    }

    private static void httpPostRequest(HttpPost httpPost) throws ClientProtocolException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

            @Override
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {

                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {

                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        httpClient.execute(httpPost, responseHandler);
    }
    
	private static String getEndpoint(String service) {
		AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
				.withRegion("us-east-1").build();
		
		ListDomainNamesResult listDomainNamesResult = awsElasticsearch.listDomainNames(new ListDomainNamesRequest());
		String endPoint = "";
		
		for (DomainInfo domain : listDomainNamesResult.getDomainNames()) {
			
			if(domain.toString().contains(service)){
				DescribeElasticsearchDomainResult esDomainResult = awsElasticsearch.describeElasticsearchDomain(
			            new DescribeElasticsearchDomainRequest().withDomainName(domain.getDomainName()));	
			    endPoint = esDomainResult.getDomainStatus().getEndpoint();
			    break;
			}
		}	
		return endPoint;
	}

}
