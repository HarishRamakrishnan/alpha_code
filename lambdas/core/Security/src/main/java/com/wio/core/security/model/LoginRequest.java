package com.wio.core.security.model;

/**
 * Bean for the user login request.
 */
public class LoginRequest {
	private String userID;
	private String password;
	private String service;
	
	
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}

	
}
