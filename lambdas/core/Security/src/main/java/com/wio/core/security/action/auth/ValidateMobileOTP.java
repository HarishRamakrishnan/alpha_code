package com.wio.core.security.action.auth;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.aws.api.sns.OTPTransaction;
import com.wio.common.aws.api.sns.SNSAPI;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;

public class ValidateMobileOTP extends AbstractAction 
{
    private LambdaLogger logger;
    
    GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
    //private final String SERVICE = "service";

    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
                      
        OTPTransaction input = getGson().fromJson(getBodyFromRequest(request), OTPTransaction.class);
        
        OutputMessage result = SNSAPI.validateOTP(input.getMessageId(), input.getOtp());
        
        return getGson().toJson(result, OutputMessage.class);  
    }
        
}

