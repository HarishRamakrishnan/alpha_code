package com.wio.core.security.action;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;
import com.wio.core.security.model.LoginRequest;
import com.wio.core.user.helper.PasswordHelper;

/**
 * Action used to verify a user credentials
 * <p/>
 * POST to /login/
 */
public class LoginAction extends AbstractAction 
{
	private LambdaLogger logger;
	
	GenericDAO genDAO = GenericDAOFactory.getGenericDAO();

	public User handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{	
		logger = lambdaContext.getLogger();

		JsonObject requestBody = request.get("body").getAsJsonObject();

		LoginRequest loginRequest = getGson().fromJson(requestBody, LoginRequest.class);

		if(loginRequest == null)
			throw new BadRequestException("Input should not be empty/null || "+ExceptionMessages.EX_INVALID_INPUT);
		
		if(RequestValidator.isEmptyField(loginRequest.getUserID()))
			throw new BadRequestException("UserID cannot be empty/null || "+ExceptionMessages.EX_INVALID_USERNAME);
		
		if(RequestValidator.isEmptyField(loginRequest.getPassword()))
			throw new BadRequestException("Password should not be empty/null || "+ExceptionMessages.EX_INVALID_PASSWORD);
		
		
		User loggedUser = null;
		//String userRole = null;

		try 
		{	
			//userRole = dao.getUserRoleByUid(Formatter.createUserId(input.getService(), input.getUsername()));
			
			/*userRole = dao.getUserRoleByUserName(input.getUsername());
			
			if(userRole.equals(UserRoleType.USER_ROLE_GLOBAL_USER))
			{
				if(input.getService() != null)
					throw new BadRequestException("Global user does not require any service... "+ExceptionMessages.EX_INVALID_INPUT);
					
				input.setService("global");
			}
			else if(userRole.equals(UserRoleType.USER_ROLE_WAVES_USER))
			{
				if(input.getService() != null)
					throw new BadRequestException("Waves user does not require any service... "+ExceptionMessages.EX_INVALID_INPUT);
				
				input.setService("waves");
			}
			else if(input.getService() == null || input.getService().trim().equals(""))
				throw new BadRequestException("Service should not be empty / null for Service/Network user || "+ExceptionMessages.EX_INVALID_SERVICE);
			
			//if(!UserMgmtValidator.isServiceExist(input.getService())){
				
				//throw new BadRequestException("Service does not exist || "+ExceptionMessages.EX_INVALID_SERVICE);
		//	}*/
			
			loggedUser = genDAO.getGenericObject(User.class, loginRequest.getUserID());
		} 
		catch (final DAOException e) 
		{
			logger.log("Error while getting logged user info...\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}
		
		if (null == loggedUser) 
			throw new InternalErrorException("User does not exist! ");
				
		try 
		{
			if (!PasswordHelper.authenticate(loginRequest.getPassword(), loggedUser.getPasswordBytes(), loggedUser.getSaltBytes())) 
			{
				throw new InternalErrorException(ExceptionMessages.EX_INVALID_PASSWORD);
			}
		} 
		catch (final NoSuchAlgorithmException e) 
		{
			logger.log("No algrithm found for password encryption\n" + e.getMessage());
			throw new InternalErrorException(ExceptionMessages.EX_PWD_SALT);
		} 
		catch (final InvalidKeySpecException e) 
		{
			logger.log("No KeySpec found for password encryption\n" + e.getMessage());
			throw new InternalErrorException(ExceptionMessages.EX_PWD_ENCRYPT);
		}

		// if we reach this point we assume that the user is authenticated.
		
		
		// Need to generate security token for further authentication
		String securityToken = generateToken(loggedUser.getUserID());
		
        loggedUser.setSecurityToken(securityToken);
		loggedUser.setAuthenticatedUser(true);

		return loggedUser;
	}
	
	
	public String generateToken(String userID)
	{	  
		try 
		{
			User user = genDAO.getGenericObject(User.class, userID);
			
		    SimpleDateFormat sdf =  new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT); // 30-12-2016 03:58:21 PM
			    
		    Date date = new Date();
		    Calendar cal = Calendar.getInstance();
		    cal.setTime(date);
		     
		    String token = String.valueOf(cal.getTimeInMillis());
		     
		    cal.add(Calendar.HOUR, Integer.parseInt(SystemConfigMessages.SYSTEM_SECURITY_TOKEN_TIMEOUT));
	
		    java.util.Date endDate = cal.getTime();
		         
		     
		    user.setSecurityToken(token);	
		    user.setTokenStartTime(sdf.format(new Date()));
		    user.setTokenEndTime(sdf.format(endDate));	     
	     
	    	genDAO.saveGenericObject(user);	
	    	 
			return token;
		 } 
	     catch (DAOException e) 
	     {	
	    	 logger.log("Error while generating security token at "+this.getClass().getName());
			e.printStackTrace();
		 }
		return null;
	}	
}
