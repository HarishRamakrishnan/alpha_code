package com.wio.core.security.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.helper.ActionMapper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.PrivilegedAction;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.user.action.user.UserValidator;
import com.wio.core.user.request.UserObject;
import com.wio.core.user.request.UserRequest;

public class AuthorizeUserAction extends AbstractAction 
{
    private LambdaLogger logger;
    
    GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
    //private final String SERVICE = "service";

    public User handle(JsonObject jsonRequest, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        
        LoginAction loginAction = new LoginAction();
        
        String userRole = null;        
        String token=null;
        
        String action = jsonRequest.get("action").getAsString();        
        JsonObject body = jsonRequest.get("body").getAsJsonObject();
        
        //If action is not 'login' or 'validateDevice', check for security token in request body
        if(!action.equalsIgnoreCase(ActionMapper.SECURITY_MGMT_LOGIN) 
        		&& !action.equalsIgnoreCase(ActionMapper.CONFIGURATION_MGMT_GET_DEVICE_TOPICS)) 
        {
        	if(null == body.get("securityToken") || body.get("securityToken").getAsString().equals(""))
        	{
        		logger.log("Security Token cannot be empty or null !");
        		throw new BadRequestException("Security Token cannot be empty or null !");        		
        	}
        	
        	token =  body.get("securityToken").getAsString();
        }
        
        User securityToken = null;
        
        User loginUserRespose=new User();        
              
        if(token != null)
        {       	
          	try 
          	{          		
          		List<FilterCondition> filterConditions = new ArrayList<FilterCondition>(); 
        	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SECURITY_TOKEN, token, ComparisonOperator.EQ));        
        	    
          		List<User> userToken = genDAO.getGenericObjects(User.class, filterConditions);
          		
          		if(userToken.isEmpty())
          		{
          			logger.log("Securtity token "+token+" not found !");
          			throw new InternalErrorException(ExceptionMessages.EX_NO_TOKEN);	
          		}
          		
          		securityToken = userToken.get(0);          		
          	} 
          	catch (DAOException e) 
          	{				
				e.printStackTrace();
			}
         
        	        	
        	if(isTokenExpire(securityToken.getTokenEndTime().toString()))
        	{
        		loginAction.generateToken(securityToken.getUserID());
        		throw new InternalErrorException(ExceptionMessages.EX_TOKEN_EXPIRED);
        	}
        	
        	loginUserRespose.setAuthenticatedUser(true);
        	userRole = securityToken.getUserRole();              	
        }
        else
        {
        	if(action.equalsIgnoreCase("login"))
        		loginUserRespose = loginAction.handle(jsonRequest, lambdaContext);
        	
        	userRole = loginUserRespose.getUserRole();
        }
        
        
        
        if(!action.equalsIgnoreCase("login")) // No need to check privilege for login action
        {        
	        boolean authorizedUser = false;
	        
	        if(userRole.equals(UserRoleType.USER_ROLE_WAVES_ADMIN))
	        	loginUserRespose.setAuthorizedUser(true);
	        else
	        {
	        	try 
		        {	
	        		String inputRole = null;
	        		
	        		if(action.equals(ActionMapper.USER_MGMT_ADD_USER))
	        		{
	        			UserRequest usersList = getGson().fromJson(body, UserRequest.class);
		           			        			
		           		for(UserObject user: usersList.getUsers())
		           		{
		           		  UserValidator.validateOnAdd(user);
		           		    			 
		           		  if(inputRole == null)
		           			inputRole = user.getUserRole();
		           		  else if(!inputRole.equalsIgnoreCase(user.getUserRole())) 
		           		    throw new BadRequestException("All the users should have same user role while adding ! "+ExceptionMessages.EX_INVALID_INPUT);
		           		}
	        		}
	        		else if(action.equals(ActionMapper.USER_MGMT_MODIFY_USER) ||  action.equals(ActionMapper.USER_MGMT_DELETE_USER))
	        		{
	        			User input = getGson().fromJson(body, User.class);
	        			User user = null;
	        			
	        			if(RequestValidator.isEmptyField(input.getUserID()))
	        				throw new BadRequestException("UserID cannont be empty!");
	       			 
	       			 	user = genDAO.getGenericObject(User.class, input.getUserID());
	       			 
		       			if(null == user)
		       				throw new DAOException("User does not exist!");
	   	  	         
		       			inputRole = user.getUserRole();
	        		}
	        		
	        		
		        	if(null == inputRole)
		        		authorizedUser = DataValidator.isPrivilegedRole(getPrivilege(action, null), userRole);
		        	else
		        		authorizedUser = DataValidator.isPrivilegedRole(getPrivilege(action, inputRole), userRole);
		        		
		        	
		        		
		        		
		        	/*logger.log("Logged in user UserRole : "+userRole+" User Role "+getPrivilegeString(action,jsonRequest,securityToken));
		        	authorizedUser = RolePrivilegeDBValidation.isRoleAndPrivilegeMapped(userRole, getPrivilegeString(action,jsonRequest,securityToken));
		        	logger.log("Authorized user : "+authorizedUser);*/
		        	loginUserRespose.setAuthorizedUser(authorizedUser);
		        	
		        } catch (final DAOException e) {
		            logger.log("Error while checking the privilege\n" + e.getMessage());
		            throw new InternalErrorException(e.getMessage());
		        }
	        }
        }               
        return loginUserRespose;  
    }
    
    public String getPrivilege(String action, String inputRole) throws DAOException
    {
    	GenericDAO dao = GenericDAOFactory.getGenericDAO();
    	
    	List<FilterCondition> filterCondition = new ArrayList<FilterCondition>();
		filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.ACTION, action, ComparisonOperator.EQ));
		
		if(null != inputRole)
			filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.INPUT_ROLE, inputRole, ComparisonOperator.EQ));
		
    	List<PrivilegedAction> pList = dao.getGenericObjects(PrivilegedAction.class, filterCondition);
    	
    	if(pList.isEmpty())
    		throw new DAOException("Not privileged to perform action: "+action);    	
    	
    	/*if(pList.size()>1)
    		throw new DAOException("More than one privilege found for action: "+action);
    	*/
    	return pList.get(0).getPrivilege();
    }
    
/*    public String getPrivilegeString(String action, JsonObject request, User securityToken) throws InternalErrorException, BadRequestException {
    	
    	JsonObject body = request.get("body").getAsJsonObject();
    	String privilegeStr = null;
    	//String userRole = body.get("userRole").getAsString();
    	
    	//ActionPrivilegeMapDAO dao = DAOFactory.getActionPrivilegeMapDAO();
    	
    	try 
    	{
    		
    	 if(action.equals(ActionMapper.USER_MGMT_ADD_USER))
    	 {
    		 System.out.println("add user body:: "+body.toString());
    		 UserRequest usersList = getGson().fromJson(body, UserRequest.class);
    		 
    		 String role = null;
    		 int loopCount =0;
    		 
    		 for(UserObject user: usersList.getUsers())
 	         {
    			 ValidateUserOnAdd.validate(user);
    			 
    			 loopCount++;
    			 
    			 if(loopCount == 1)
    			 {
    				 role = user.getUserRole();
    			 }
    			 else if(!role.equalsIgnoreCase(user.getUserRole()))
    			 {
    				 throw new BadRequestException("All the users should have same user role while adding ! "+ExceptionMessages.EX_INVALID_INPUT);
    			 }
 	         }    		     		 
    		 return ActionMapper.getUserMgmtPrivilegeforManage(role);    		 	 
    	 }
    	 else if(action.equals(ActionMapper.USER_MGMT_DELETE_USER))
    	 {
    		 User input = getGson().fromJson(body, User.class);
    		 User user = null;
    		 String role = null;
    		 
    		 try 
  	         {
    			 if(RequestValidator.isEmptyField(input.getUserID()))
    				 throw new BadRequestException("UserID cannont be empty!");
    			 
    			 user = genDAO.getGenericObject(User.class, input.getUserID());
    			 
    			 if(null == user)
    				 throw new DAOException("Username does not exist!");
	  	         
	  	         role = user.getUserRole();
  	         }
    		 catch ( DAOException e) 
    		 {
   	            logger.log("Error while deleting the user...\n" + e.getMessage());
   	            throw new InternalErrorException(e.getMessage());
   	         }
  	         return ActionMapper.getUserMgmtPrivilegeforManage(role); 
    	 }
    	 else if(action.equals(ActionMapper.USER_MGMT_MODIFY_USER) || action.equals(ActionMapper.USER_MGMT_VIEW_USER))
    	 {
    		 User input = getGson().fromJson(body, User.class);
    		 User user = null;
    		 
  	         try 
  	         {
  	        	 if(action.equals(ActionMapper.USER_MGMT_VIEW_USER))
  	        	 {
  	        		 if(RequestValidator.isEmptyField(input.getUserID()))
  	        			 return ActionMapper.getUserMgmtPrivilegeforView(null);  	  	  	    	
  		         }
  	        	   	   
	        	 user = genDAO.getGenericObject(User.class, input.getUserID());	       	

	  	         if (user == null) 
	  	            throw new DAOException("Username does not exist");
	  	         
	  	         User loggedUser = user;
	  	         

	  	         if(!loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_WAVES_USER)) 
	  	         {
	  	        	 if(!loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_GLOBAL_USER))
	  	        	 {
	  	        		 if(!user.getServiceName().equals(loggedUser.getServiceName()))
	  	        			 throw new BadRequestException("Invalid User, Authentication/Authorization failed");
	  	        	 }
	  	         }
	        
  	        	

	  	        if(securityToken.getUserID().equals(user.getUserID()))
	  	        {
	 	        	if(action.equals(ActionMapper.USER_MGMT_MODIFY_USER)) 
	 	        		return  ActionMapper.getUserMgmtPrivilegeforModify(null);
	 	        	else if(action.equals(ActionMapper.USER_MGMT_VIEW_USER))
	 	        		return  ActionMapper.getUserMgmtPrivilegeforView(null);
		        }
	  	        else
	  	        	return ActionMapper.getUserMgmtPrivilegeforManage(user.getUserRole()); 	         
  	        }
  	        catch ( DAOException e) 
  	        {
  	            logger.log("Error while retrieving user\n" + e.getMessage());
  	            throw new InternalErrorException(e.getMessage());
  	        } 	        
  	       
    	 }
    	 else
    	 {
    		 List<FilterCondition> filterCondition= new ArrayList<FilterCondition>();
    		 filterCondition.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.ACTION, action, ComparisonOperator.EQ));
    	    	
			 ActionPrivilegeMap privilege = genDAO.getGenericObjects(ActionPrivilegeMap.class, filterCondition).get(0);
			 privilegeStr = privilege.getUserPrivilege();
	     }
		} 
    	catch (DAOException e) 
    	{
			throw new InternalErrorException(e.getMessage());
		}
        
    	return privilegeStr;
        switch (request.get("action").getAsString())
        		{	
        	case Formatter.USER_MGMT_ADD_USER:
        		return Formatter.getUserMgmtPrivilegeStr(body.get("userRole").getAsString());
        		
        	case Formatter.USER_MGMT_MODIFY_USER:
        		return "ModifyUser";
        	case Formatter.USER_MGMT_CHANGE_PASSWORD:
        		return "ChangePassword";
        	case Formatter.USER_MGMT_DELETE_USER:
        		
        		
        		return "DeleteUser";
        		
        	case Formatter.USER_MGMT_VIEW_USER:
        		return "ViewUser";
        	case Formatter.USER_MGMT_VIEW_ALL_USER:
        		return "ViewAllUsers";
        		
        	case Formatter.PROVISIONING_MGMT_ADD_AP:
			case Formatter.PROVISIONING_MGMT_MODIFY_AP:
			case Formatter.PROVISIONING_MGMT_DELETE_AP:
			case Formatter.PROVISIONING_MGMT_VIEW_AP:
			case Formatter.PROVISIONING_MGMT_LINK_AP:
			case Formatter.PROVISIONING_MGMT_DELINK_AP:
				return "ManageAP";
			case Formatter.PROVISIONING_MGMT_ADD_DEVICEGROUP:
			case Formatter.PROVISIONING_MGMT_MODIFY_DEVICEGROUP:
			case Formatter.PROVISIONING_MGMT_DELETE_DEVICEGROUP:
			case Formatter.PROVISIONING_MGMT_VIEW_DEVICEGROUP:
				return "ManageDeviceGroup";
			case Formatter.PROVISIONING_MGMT_ADD_NETWORK_ZONE:	
			case Formatter.PROVISIONING_MGMT_MODIFY_NETWORK_ZONE:	
			case Formatter.PROVISIONING_MGMT_DELETE_NETWORK_ZONE:	
			case Formatter.PROVISIONING_MGMT_VIEWALL_NETWORK_ZONE:	
				return "ManageMobilityDomians";
			case Formatter.PROVISIONING_MGMT_ADD_NETWORK:	
			case Formatter.PROVISIONING_MGMT_MODIFY_NETWORK:	
			case Formatter.PROVISIONING_MGMT_DELETE_NETWORK:	
			case Formatter.PROVISIONING_MGMT_VIEWALL_NETWORK:	
				return "ManageNetworks";
			case Formatter.PROVISIONING_MGMT_ADD_NETWORK_SETTINGS:
			case Formatter.PROVISIONING_MGMT_MODIFY_NETWORK_SETTINGS:
			case Formatter.PROVISIONING_MGMT_VIEW_NETWORK_SETTINGS:
			case Formatter.PROVISIONING_MGMT_DELETE_NETWORK_SETTINGS:
				return "ManageNetworkSettings";
			case Formatter.PROVISIONING_MGMT_ADD_SERVICE:	
			case Formatter.PROVISIONING_MGMT_MODIFY_SERVICE:	
			case Formatter.PROVISIONING_MGMT_DELETE_SERVICE:	
			case Formatter.PROVISIONING_MGMT_VIEWALL_SERVICE:		
				return "ManageService";
			case Formatter.USER_MGMT_ADD_ROLE:
			case Formatter.USER_MGMT_DELETE_ROLE:
			case Formatter.USER_MGMT_ASSIGN_PRIVILEGE:
			case Formatter.USER_MGMT_UNASSIGN_PRIVILEGE:
			case Formatter.USER_MGMT_ADD_PRIVILEGE:
			case Formatter.USER_MGMT_DELETE_PRIVILEGE:
				return "ManageUserRole";
				
			case Formatter.STATUS_UPDATE_AP:
			case Formatter.FETCH_AP_STATUS:
				return "APStatus";
        	default:         
        		return privilegeStr;
       	}
               
        	
    }*/
    
    private boolean isTokenExpire(String date)
    {
        if(date.isEmpty() || date.trim().equals(""))
        {
            return false;
        }
        else
        {
            SimpleDateFormat sdf =  new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT); // Jan-20-2015 1:30:55 PM
            Date d=null;
            Date d1=null;
            
            String today=   sdf.format(new Date());
            try 
            {              
                d = sdf.parse(date);
                d1 = sdf.parse(today);
                if(d1.compareTo(d) <0)// not expired
                {
                    return false;
                }
                else if(d.compareTo(d1)==0)// both date are same
                {
                    if(d.getTime() < d1.getTime())// not expired
                        return false;
                    
                    else if(d.getTime() == d1.getTime())//expired
                        return true;
                    
                    else//expired
                        return true;                    
                }
                else //expired
                {
                    return true;
                }
            } 
            catch (ParseException e) 
            {
                e.printStackTrace();                    
                return false;
            }
        }
    }
        
}

