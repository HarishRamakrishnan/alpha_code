package com.wio.core.security.model;

import java.nio.ByteBuffer;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * Bean for the user login request.
 */
@DynamoDBTable(tableName = DynamoDBObject.USER)
public class LoginUser1 {
	
	private String uId;
	private String userName;
	private String userRole;
	private Boolean authenticatedUser = false;
	private Boolean authorisedUser = false;
	private String service;
	private ByteBuffer password;
	private ByteBuffer salt;
	private String securityToken;
	
	@DynamoDBAttribute(attributeName = "UID")
    public String getUid() {
        return uId;
    }

    public void setUid(String uId) {
        this.uId = uId;
    }
	
    @DynamoDBHashKey(attributeName = "USER_NAME")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@DynamoDBAttribute(attributeName = "USER_ROLE")
	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
	@DynamoDBAttribute(attributeName = "SERVICE")
    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
    
    @DynamoDBAttribute(attributeName = "PASSWORD")
    public ByteBuffer getPassword() {
        return password;
    }

    public void setPassword(ByteBuffer password) {
        this.password = password;
    }
    
    @DynamoDBAttribute(attributeName = "PASSWORD_SALT")
    public ByteBuffer getSalt() {
        return salt;
    }

    public void setSalt(ByteBuffer salt) {
        this.salt = salt;
    }
    
    @DynamoDBIgnore
    public byte[] getPasswordBytes() {
        return password.array();
    }

    @DynamoDBIgnore
    public byte[] getSaltBytes() {
        return salt.array();
    }
    
    @DynamoDBIgnore
	public Boolean getAuthenticatedUser() {
		return authenticatedUser;
	}

   
	public void setAuthenticatedUser(boolean value) {
		this.authenticatedUser = value;
	}
   
    @DynamoDBIgnore
	public Boolean getAuthorizedUser() {
		return authorisedUser;
	}

   
	public void setAuthorizedUser(boolean value) {
		this.authorisedUser = value;
	}
	
	@DynamoDBIgnore
	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

}
