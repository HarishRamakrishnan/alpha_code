package com.wio.core.config.requests;

import java.util.List;

import com.wio.common.config.model.MQTTTopic;

public class DeviceTopicsResponse 
{
	private String deviceID;
	private List<String> subscribeTopics;
	private List<MQTTTopic> publishTopics;

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public List<String> getSubscribeTopics() {
		return subscribeTopics;
	}

	public void setSubscribeTopics(List<String> subscribeTopics) {
		this.subscribeTopics = subscribeTopics;
	}

	public List<MQTTTopic> getPublishTopics() {
		return publishTopics;
	}

	public void setPublishTopics(List<MQTTTopic> publishTopics) {
		this.publishTopics = publishTopics;
	}

	

	
	
	
}
