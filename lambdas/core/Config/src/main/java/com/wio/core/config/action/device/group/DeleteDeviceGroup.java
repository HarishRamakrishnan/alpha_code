package com.wio.core.config.action.device.group;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;

public class DeleteDeviceGroup extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO dao  = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
        dao = GenericDAOFactory.getGenericDAO();
        
        DeviceGroup input = getGson().fromJson(getBodyFromRequest(request), DeviceGroup.class);
        
        DeviceGroupValidator.validateOnDelete(input);
        
        try 
        {
        	User loggedUser = getUserInfoFromRequest(request);
        	
        	DeviceGroup dg = dao.getGenericObject(DeviceGroup.class, input.getDgID());
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(dg.getServiceName()))
        			throw new BadRequestException("Not authorized to delete the Device Group of the Service: '"+dg.getServiceName()+"' !");
        	}
            
            
    	    if(dg != null)
    	    {
    	    	if(DataValidator.isParentDG(dg.getDgID()))
    	    	{
    	    		throw new DAOException("Cannot Delete DG as it is having chaild DGs ! Please remove chaild DGs and Try again !");
    	    	}
    	    	
    	    	
    	    	/*
    	    	if(DataValidator.isDGDependent(dg.getDgID()))
    	        {	        	
    	        	//logger.log("Cannot delete DeviceGroup. 1 or more Devices are mapped under this DG, Exception at "+this.getClass().getName());
    	        	throw new BadRequestException("Cannot delete DeviceGroup. 1 or more Devices are mapped under this DG. "+ExceptionMessages.EX_REL_DEPENDENCY);
    	        }*/
    	    }
    	    else
    	    {
    	    	throw new BadRequestException("Invalid DeviceGroup, does not exist !");
    	    }   
        	
        	dao.deleteGenericObject(dg);
        
        } catch ( DAOException e) 
        {
            logger.log("Error while deleting device group\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(input, DeviceGroup.class);        
    }
}
