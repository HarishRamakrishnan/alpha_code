package com.wio.core.config.action.device.map;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.config.requests.DeviceServiceMapRequest;


public class AssignDevicesToService extends AbstractAction
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

	@Override
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        DeviceServiceMapRequest input = getGson().fromJson(getBodyFromRequest(request), DeviceServiceMapRequest.class);
        
  		List<Device> devices = new ArrayList<Device>();
  		
  	    try 
        {
  	    	if(RequestValidator.isEmptyField(input.getServiceName()))
  	    		throw new BadRequestException("Service cannot be null or empty !");
  	    	
  	    	if(!DataValidator.isServiceExist(input.getServiceName()))
  	    		throw new BadRequestException("Service does not exist, invalid service !");
  	    	
  	    	
  	    	if(!input.getDeviceID().isEmpty())
  	    	{
  	    		input.getDeviceID().forEach(device->
  	  	    	{
	  	  	    	try 
	  	  	    	{
	  	  	    		Device dev = genDAO.getGenericObject(Device.class, device);
	  	  	    		
						if(null != dev)
						{	
							if(dev.getServiceName().equals("NA"))
							{
								dev.setServiceName(input.getServiceName());
								dev.setStatus("NotAssignedToHP");
							}
							else
								throw new BadRequestException("Device: "+dev.getDeviceID()+" is already assigned to a Service: "+dev.getServiceName());
						}
						else
							throw new BadRequestException("Device "+device+" does not exist, invalid Device !");
						
						devices.add(dev);
					} 
	  	  	    	catch (DAOException | BadRequestException e) 
	  	  	    	{
						e.printStackTrace();
					}
  	  	    	});
  	    		
  	    		genDAO.saveGenericObjects(devices);
  	    	}
  	    	else
    		{
    			throw new BadRequestException("At least one device should be assign to Service...");
    		}
  	    	
        } 
  	    catch (final DAOException e) 
  	    {
            logger.log("Error while assigning Devices to Service... \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
  	    return getGson().toJson(devices, List.class);
    }   
}

