package com.wio.core.config.requests;

import java.util.List;

/**
 * Bean for the DG registration request.
 */
public class DeviceDGMapRequest {
	
	
	private List<String> deviceID;
	private String dgID;
	
	
	public String getDgID() {
		return dgID;
	}
	public void setDgID(String dgID) {
		this.dgID = dgID;
	}
	public List<String> getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(List<String> deviceID) {
		this.deviceID = deviceID;
	}
	
}
