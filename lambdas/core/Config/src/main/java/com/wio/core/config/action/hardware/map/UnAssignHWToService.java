package com.wio.core.config.action.hardware.map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HWServiceMap;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.validation.DataValidator;

public class UnAssignHWToService extends AbstractAction
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
   
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        HWServiceMap input = getGson().fromJson(getBodyFromRequest(request), HWServiceMap.class);
        
        try 
        {
  	    	if(DataValidator.isHardwareExist(input.getHwID()))
  	    	{
  	    		if(DataValidator.isServiceExist(input.getServiceName()))
  	    		{
  	    			if(null == genDAO.getGenericObject(HWServiceMap.class, input.getHwID(), input.getServiceName()))
  	    				throw new BadRequestException("Hardware does not assigned to the service !");
  	    			else
  	    				genDAO.deleteGenericObject(input); 	    			
  	    		}
  	    		else
  	    			throw new BadRequestException("Service does not exist, invalid service !");
  	    	}
  	    	else
  	    		throw new BadRequestException("Hardware does not exist, invalid hardware !");
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while un assigning Hardware from Service...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }        

  	    return getGson().toJson(input, HWServiceMap.class);
    }

}
