package com.wio.core.config.validation;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;

public class MQTTTopicTest 
{
	private static final String topicSeperator = "/";
	private static final String wildCardSingleLevel = "+";
	private static final String wildCardMultiLevel = "#";
	
	public static String createTopicName(String service, String dgName, String parentDG) throws DAOException
	{
		if(parentDG.equals("NA"))
		{
			return service+topicSeperator+dgName;
		}
		else
		{			
			GenericDAO dao = GenericDAOFactory.getGenericDAO();
			
			List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
			
		    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, service, ComparisonOperator.EQ));
		    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_NAME, parentDG, ComparisonOperator.EQ));	    
			
		    String parentMQTTTopic = dao.getGenericObjects(DeviceGroup.class, filterConditions).get(0).getMqttTopic();
			
			return parentMQTTTopic+topicSeperator+dgName;
		}
	}
}
