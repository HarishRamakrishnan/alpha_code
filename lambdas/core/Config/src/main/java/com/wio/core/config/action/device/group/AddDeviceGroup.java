package com.wio.core.config.action.device.group;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;
import com.wio.common.util.UniqueIDGenerator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.config.action.device.group.topic.DeviceGroupTopic;

/**
 * Action used to register a new Device Group.
 * <p/>
 * POST to /dg/
 */
public class AddDeviceGroup extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO dao  = null;

    /**
     * Handler implementation for the add user action. It expects a UserRequest object in input and returns
     * a serialized UserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
    {
        logger = lambdaContext.getLogger();
        dao = GenericDAOFactory.getGenericDAO();
        
        DeviceGroup input = getGson().fromJson(getBodyFromRequest(request), DeviceGroup.class);       

        User loggedUser = getUserInfoFromRequest(request);
        
    	DeviceGroup parentDG = DeviceGroupValidator.validate(input, loggedUser);    	
    	
    	DeviceGroup newDG = new DeviceGroup();
    	
    	newDG.setDgID("DG"+UniqueIDGenerator.getID());
    	newDG.setDgName(input.getDgName());
    	newDG.setDgType(input.getDgType());
        
        if(RequestValidator.isEmptyField(input.getParentDG()))
        	newDG.setParentDG("NA");
    	else	
    		newDG.setParentDG(input.getParentDG());
        
        newDG.setServiceName(loggedUser.getServiceName());
        newDG.setNetworkID(input.getNetworkID());
        
        newDG.setMqttTopic(DeviceGroupTopic.prepareTopic(newDG, parentDG));    	
    	   
        newDG.setCreatedBy(getUserInfoFromRequest(request).getUserID());
        newDG.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
        newDG.setLastUpdateTime("NA");
                
        try 
        {
            dao.saveGenericObject(newDG);
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while creating DeviceGroup \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 

        DeviceGroup output = new DeviceGroup();
        output.setDgID(newDG.getDgID());
        output.setDgName(newDG.getDgName());
        
        return getGson().toJson(output, DeviceGroup.class);
    }
}
