package com.wio.core.config.action.hardware.profile;

import java.util.Arrays;
import java.util.List;

import com.wio.common.Enums.Constants;
import com.wio.common.config.model.HPRadioSettings;
import com.wio.common.config.model.HWRadio;
import com.wio.common.config.model.Hardware;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.PortSettings;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class ValidateHardwareProfile 
{
	public static void validateOnAdd(HardwareProfile input) throws BadRequestException, DAOException
	{
		//Empty field validation
		GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
		
		if(RequestValidator.isEmptyField(input.getHwID()))
        	throw new BadRequestException("Hardware ID "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getHpName()))
        	throw new BadRequestException("Hardware Profile Name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getServiceName()))
        	throw new BadRequestException("Service Name "+ExceptionMessages.EX_EMPTY_FIELD);
       
        if(input.getRadioSettings()==null)
        	throw new BadRequestException("Radio setting should not be null/empty");
        //Dependency validation
        
       /* if(!DataValidator.isHardwareExist(input.getHwID()))
        	throw new BadRequestException("Hardware is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);*/
        
        if(!DataValidator.isServiceExist(input.getServiceName()))
        	throw new BadRequestException("Service is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        
        
       if(!DataValidator.isHWServiceMapped(input.getHwID(), input.getServiceName()))
        	throw new BadRequestException("Given Hardware and service are not Mapped");
      
       
       Hardware hw = genDAO.getGenericObject(Hardware.class, input.getHwID());
       
       if(hw==null)
    	   throw new BadRequestException("hardware "+input.getHwID()+" does not exist");    
	
    	if(hw.getDbdcSupported().equals(Constants.SUPPORTED))
        {
	    	if(input.getRadioSettings().size() != 2)
				throw new BadRequestException("2 radio settings/only required as hardware supports DBDC (dual radio) !");
		}
        else if (input.getRadioSettings().size() !=1)
        	throw new BadRequestException("1 radio setting/only required as hardware does not supports DBDC (dual radio) !");
	      
    	
        boolean validDBDCMode = false;
        boolean validRadioBand = false;
        boolean validRadioMode = false;
        boolean validBandwidth = false;
        boolean validChannel=false;


        for(HPRadioSettings radio : input.getRadioSettings())
        {	
        	//mandatory Field of radioSettings
        	if(RequestValidator.isEmptyField(radio.getRadioName()))
    			throw new BadRequestException("Radio name "+ExceptionMessages.EX_EMPTY_FIELD);
        	if(RequestValidator.isEmptyField(radio.getRadioStatus()))
        		throw new BadRequestException("Radio Status "+ExceptionMessages.EX_EMPTY_FIELD);
        	if(RequestValidator.isEmptyField(radio.getDbdcStatus()))
        		throw new BadRequestException("DBDC Status "+ExceptionMessages.EX_EMPTY_FIELD);
        	
        	//Validation of Radio Status
        	if(!radio.getRadioStatus().equals(Constants.STATUS_OFF) && !radio.getRadioStatus().equals(Constants.STATUS_ON))
        		throw new BadRequestException("Radio Status should be ON/OFF ");
        	
        	System.out.println("validating "+radio.getRadioName());
        	if(hw.getDbdcSupported().equals(Constants.SUPPORTED))
        	{
        		if(!(radio.getDbdcStatus().equalsIgnoreCase(Constants.ENABLE) || radio.getDbdcStatus().equalsIgnoreCase(Constants.DISABLE)))
        		{
        			throw new BadRequestException("Invalid DBDC Status... Status should be enable or disable !");
        		}
        	}
        	else
        	{
        		if(!(RequestValidator.isEmptyField(radio.getDbdcStatus()) || RequestValidator.isEmptyField(radio.getDbdcMode())))
        		{
        			throw new BadRequestException("Invalid parameter DBDCStatus/DBDCMode, does not supported by Hardware: "+hw.getHwName());
        		}
        	}
        	if(radio.getRadioStatus().equals(Constants.STATUS_ON) || radio.getRadioStatus().equals(Constants.STATUS_OFF) && !RequestValidator.isEmptyField(radio.getRadioBand()))
        	{
        		for(HWRadio hwradio : hw.getHwRadio())
        		{
        			System.out.println("validating Hardware Radios.....:::"+hwradio.getRadioName());
        			for(String dbdcMode : getColonSeperatedValueList(hwradio.getDbdcModeSupported()))
        			{
        				if(dbdcMode.trim().equalsIgnoreCase(radio.getDbdcMode().trim()))
        				{
        					validDBDCMode = true; 
        					break;
        				}   
        				else
        					validDBDCMode = false;
        			}
	
        			if(validDBDCMode)
        			{
        				for(String radioBand: getColonSeperatedValueList(hwradio.getRadioBandSupported()))
        				{
        					System.out.println("HW radio band: "+radioBand.trim());
        					System.out.println("ap radio bandL: "+radio.getRadioBand().trim());
			
        					if(radioBand.trim().equalsIgnoreCase(radio.getRadioBand().trim()))
        					{
        						validRadioBand = true;
        						break;
        					}
        					else
        						validRadioBand = false;
        				}
        			}
	 
	
        			if(validDBDCMode && validRadioBand)
        			{        			
        				for(String radioMode : getColonSeperatedValueList(hwradio.getRadioModeSupported()))
        				{
        					System.out.println("HW radio mode: "+radioMode);
        					System.out.println("ap radio mode: "+radio.getRadioMode());
			
        					if(radioMode.trim().equalsIgnoreCase(radio.getRadioMode().trim()))
        					{
        						validRadioMode = true;
        						break;
        					}
        					else
        						validRadioMode = false;
        				}        			
        			}
	
        			
        			if(validDBDCMode && validRadioBand && validRadioMode)
        			{
        				for(String bandwidth : getColonSeperatedValueList(hwradio.getBandwidthSupported()))
        				{
        					System.out.println("HW bandwidth: "+bandwidth);
        					System.out.println("ap bandwidth: "+radio.getBandwidth());
			
        					if(bandwidth.trim().equalsIgnoreCase(radio.getBandwidth().trim()))
        					{
        						validBandwidth = true;
        						break;
        					}
        					else
        						validBandwidth = false;
        				}
        			}        		
	
        			if(validRadioBand && !validRadioMode)
        				throw new BadRequestException("Invalid Radio Mode: "+radio.getRadioMode());
	
        			if(validRadioBand && !validBandwidth)
        				throw new BadRequestException("Invalid Bandwidth: "+radio.getBandwidth());
	
        			if(validRadioBand && validRadioMode && validBandwidth)
        				break;
        		}        	

        		if(!validDBDCMode)
        			throw new BadRequestException("Invalid DBDC mode: "+radio.getDbdcMode());
            
        		if(!validRadioBand)
        			throw new BadRequestException("Invalid Radio Band: "+radio.getRadioBand());
            
        		if(!validRadioMode)
        			throw new BadRequestException("Invalid Radio Mode: "+radio.getRadioMode());
            
        		if(!validBandwidth)
        			throw new BadRequestException("Invalid Bandwidth: "+radio.getBandwidth());
        	}
        	
        	if(radio.getRadioStatus().equals(Constants.STATUS_OFF) && RequestValidator.isEmptyField(radio.getRadioBand()))
            {
        		if(!RequestValidator.isEmptyField(radio.getRadioMode())||RequestValidator.isEmptyField(radio.getBandwidth())||RequestValidator.isEmptyField(radio.getChannel()))
        			throw new BadRequestException("Either give radio band or remove radioMode/Bandwidth/channel");
            }
        	
            ;
             
        	if(radio.getRadioBand().equalsIgnoreCase("2.4 GHz ISM"))
        	{
        		if(radio.getBandwidth().equals("20 MHz"))
        		{
        			System.out.println("ppppppppppppp" + radio.getRadioMode().equals("802.11 b "));
        			System.out.println("ppppppppppppp" + radio.getRadioMode().equals("802.11 g "));
        			if(!radio.getRadioMode().equals("802.11 b") && !radio.getRadioMode().equals("802.11 g"))
        				throw new BadRequestException("invalid combination of radio band/mode/bandwidth");
        		}
        		if(radio.getBandwidth().equals("40 MHz"))
        		{
        			System.out.println("kkkkkkkkkkkkkkkkkkkk");
        			if(!radio.getRadioMode().equals("802.11 n") && !radio.getRadioMode().equals("802.11 bgn-mixed"))
        				throw new BadRequestException("invalid combination of radio band/mode/bandwidth");
        		}
        		 
          		if(!RequestValidator.isEmptyField(radio.getChannel()))
        		{	
              		for(String channel : getColonSeperatedValueList(Constants.CHANNEL1))
	        		{
	        			System.out.println("constyant channel: "+channel);
						System.out.println("ap channel: "+radio.getChannel());
		
	        			if(channel.trim().equals(radio.getChannel().trim())){
	        				validChannel=true;
	        				break;}
	        			else
	        				validChannel=false;
	        				
	        		}
              		if(!validChannel)
            			throw new BadRequestException("invalid Channel.. ");
            		
        		}
        		
    
        	}
        	
        	if(radio.getRadioBand().equalsIgnoreCase("5 GHzU-NII-2A") || radio.getRadioBand().equalsIgnoreCase("5 GHzU-NII-2B")||radio.getRadioBand().equalsIgnoreCase("5 GHzU-NII-3"))
        	{
        		if(radio.getRadioMode().equals("802.11 a"))
        		{
        			if(!radio.getBandwidth().equals("40 MHz"))
        				throw new BadRequestException("invalid combination of radio band/mode/bandwidth");
        		}
        		if(radio.getRadioMode().equals("802.11 ac"))
        		{
        			if(!radio.getBandwidth().equals("160 MHz") && !radio.getBandwidth().equals("80 MHz") && !radio.getBandwidth().equals("80+80 MHz"))
        				throw new BadRequestException("invalid combination of radio band/mode/bandwidth");
        		}
        		if(!RequestValidator.isEmptyField(radio.getChannel()))
        		{
        			
        		for(String channel : getColonSeperatedValueList(Constants.CHANNEL))
        		{
        			System.out.println("constyant channel: "+channel);
					System.out.println("ap channel: "+radio.getChannel());
	
        			if(channel.trim().equals(radio.getChannel().trim())){
        				validChannel=true;
        				break;}
        			else
        				validChannel=false;
        				
        		}
        		if(!validChannel)
        			throw new BadRequestException("invalid Channel.. ");
        		}
        	}
        	
        	radio.setBabel("Disabled");
        }
        int lanPorts=0;
        int wanPorts=0;
        for(PortSettings lan:input.getPortSettings())
        {
        	if(lan.getPortType()==null)
        		throw new BadRequestException("Port Type should not be empty");
        	else if(!lan.getPortType().equals(Constants.PORT_TYPE_LAN) && !lan.getPortType().equals(Constants.PORT_TYPE_WAN))
        		throw new BadRequestException("Invalid Port Type...Port Type Should be LAN/WAN");
        	else if(Constants.PORT_TYPE_LAN.equals(lan.getPortType()))
        		lanPorts=lanPorts+1;
        	else
        		wanPorts=wanPorts+1;
        	
        	lan.setBabel("Disabled");
        }
        System.out.println("lanPort: "+lanPorts+ " wanPort: "+wanPorts);
        if(lanPorts!=hw.getLanPortSupported()-1 && wanPorts!=1)
        	throw new BadRequestException("Invalid Number Of Lan/Wan Port Type...Port Type Wan should be one and Lan should be"+(hw.getLanPortSupported()-1));
       
        
       /* if(!(input.getPortSettings().size() == hw.getLanPortSupported()-1))
        	throw new BadRequestException("Number lan ports provided is not supported by Hardware: "+hw.getHwName());  */
        
	}
    
    public static List<String> getColonSeperatedValueList(String fullString)
	{
		String [] separatedValues = fullString.split(":");				
		return Arrays.asList(separatedValues);
	}
}
