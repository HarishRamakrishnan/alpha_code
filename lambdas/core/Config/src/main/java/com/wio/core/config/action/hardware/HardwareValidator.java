package com.wio.core.config.action.hardware;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wio.common.Enums.Constants;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.config.model.HWRadio;
import com.wio.common.config.model.Hardware;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.RequestValidator;

public class HardwareValidator 
{
	public static void validateOnAdd(Hardware input) throws BadRequestException
	{	
		 if(RequestValidator.isEmptyField(input.getHwName()))
	     {
	     	throw new BadRequestException("Hardware Name "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getManufacture()))
	     {     	
	     	throw new BadRequestException("Hardware Manufacture "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getChipsetVendor()))
	     {
	       	throw new BadRequestException("Hardware Chipset vendor "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getChipsetType()))
	     {
	     	throw new BadRequestException("Hardware Chipset type "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getCpuCore()))
	     {
	     	throw new BadRequestException("Hardware Cpu core "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getCpuType()))
	     {
	     	throw new BadRequestException("Hardware Cpu type "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getCpuFrequency()))
	     {
	     	throw new BadRequestException("Hardware Cpu frequency "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getDramController()))
	     {
	     	throw new BadRequestException("Hardware Dram Controller "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getTotalMemory()))
	     {
	     	throw new BadRequestException("Hardware total memory "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getFreeMemory()))
	     {
	     	throw new BadRequestException("Hardware Free memory "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     /*if(InputValidator.isEmptyField(input.getLanConnector()))
	     {
	     	throw new BadRequestException("Hardware LAN connector "+ExceptionMessages.EX_EMPTY_FIELD);
	     }*/
	     
	     if(RequestValidator.isEmptyField(input.getLanInterfaceSupported()))
	     {
	     	throw new BadRequestException("Hardware Lan interface supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getLanPortSupported()))
	     {
	    	 throw new BadRequestException("Hardware Lan Port Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getNatOffloading()))
	     {
	     	throw new BadRequestException("Hardware Nat Off loading "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(!(input.getNatOffloading().equals(Constants.SUPPORTED) || input.getNatOffloading().equals(Constants.NOTSUPPORTED))){
	    	 throw new BadRequestException("Hardware Nat Off loading should be either "+Constants.SUPPORTED +" or "+Constants.NOTSUPPORTED+".");
	     }
	     
	     if(RequestValidator.isEmptyField(input.getPowerAdapter()))
	     {
	     	throw new BadRequestException("Hardware Power Adapter "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getUsbPortSupported()))
	     {
	     	throw new BadRequestException("Hardware USB Port Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getUsbSpeedSupported()))
	     {
	     	throw new BadRequestException("Hardware USB Speed Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getUsbTypeSupported()))
	     {
	     	throw new BadRequestException("Hardware USB Type Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getVisualIndicatorSupported()))
	     {
	     	throw new BadRequestException("Hardware Visual Indicator Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(input.getHwRadio().size()>2 || input.getHwRadio().size()<=1){
	     	throw new BadRequestException("Only 2 Radio settings can be configured for a Hardware.");
	     }
	     
	     List<HWRadio> hwRadios = input.getHwRadio();
	  	
	 	 if(hwRadios.size()>0){
	 		if(hwRadios.get(0).getRadioBandSupported().equals(hwRadios.get(1).getRadioBandSupported())){
	 		  throw new BadRequestException("Already Radio settings with radio band "+ hwRadios.get(0).getRadioBandSupported() +" has been configured.");
	 		}
	 	  }
     
    }
	
	public static void validateOnAddHWRadio(HWRadio input) throws BadRequestException
	{		 
		 if(RequestValidator.isEmptyField(input.getRadioName()))
	     {     	
	     	throw new BadRequestException("Radio Name "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getRadioBandSupported()))
	     {
	       	throw new BadRequestException("Radio Band Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getRadioModeSupported()))
	     {
	     	throw new BadRequestException("Radio Mode Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getBandwidthSupported()))
	     {
	     	throw new BadRequestException("Bandwidth Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(RequestValidator.isEmptyField(input.getMimoMode()))
	     {
	     	throw new BadRequestException("Mimo Mode "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(!(input.getMimoMode().equals(Constants.MU_MIMO+" : "+Constants.SU_MIMO) || input.getMimoMode().equals(Constants.SU_MIMO+" : "+Constants.MU_MIMO)||
	    		 input.getMimoMode().equals(Constants.SU_MIMO) || input.getMimoMode().equals(Constants.MU_MIMO))){
	    	 throw new BadRequestException("HP Radio MIMO should be either "+Constants.MU_MIMO +" or "+Constants.SU_MIMO+" or both with (SU-MIMO : MU-MIMO).");
	     }
	     
	     if(RequestValidator.isEmptyField(input.getDfsMode()))
	     {
	     	throw new BadRequestException("DFS Mode "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(!(input.getDfsMode().equals(Constants.SUPPORTED) || input.getDfsMode().equals(Constants.NOTSUPPORTED))){
	    	 throw new BadRequestException("HP Radio DFS should be either "+Constants.SUPPORTED +" or "+Constants.NOTSUPPORTED+".");
	     }
	     
	     if(RequestValidator.isEmptyField(input.getDbdcModeSupported()))
	     {
	     	throw new BadRequestException("DBDC Mode Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	        
	     if(RequestValidator.isEmptyField(input.getDbdcSupported()))
	     {
	     	throw new BadRequestException("DBDC Supported "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	     
	     if(!(input.getDbdcSupported().equals(Constants.SUPPORTED) || input.getDbdcSupported().equals(Constants.NOTSUPPORTED))){
	    	 throw new BadRequestException("HP Radio DBDC should be either "+Constants.SUPPORTED +" or "+Constants.NOTSUPPORTED+".");
	     }
	     
	     if(RequestValidator.isEmptyField(input.getSpatialStream()))
	     {
	     	throw new BadRequestException("Spatial Stream "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
	
	}
	
	public static void validateOnModify(Hardware input) throws DAOException, BadRequestException
	{
		GenericDAO genDAO = GenericDAOFactory.getGenericDAO();
	
		List<FilterCondition> hardwareFilter =new ArrayList<FilterCondition>();
  	   	hardwareFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_NAME, input.getHwName(), ComparisonOperator.EQ));
	      
		Hardware hp = genDAO.getGenericObjects(Hardware.class, hardwareFilter).get(0);

		if ((input.getNatOffloading() != null) && (!(input.getNatOffloading().equals(Constants.SUPPORTED) || input.getNatOffloading().equals(Constants.NOTSUPPORTED)))) {
			throw new BadRequestException("Hardware Nat Off loading should be either " + Constants.SUPPORTED + " or "
					+ Constants.NOTSUPPORTED + ".");
		}

		if (input.getHwRadio() != null && input.getHwRadio().size() > 2) 
			throw new BadRequestException("Only 2 Radio settings can be configured for a Hardware.");
		
       if(input.getHwRadio()!=null)
       {
    	   for (HWRadio hpradio : input.getHwRadio()) 
    	   {
				System.out.println("validating Hardware Radios.....:::" + hpradio.getRadioName());
				if (input.getDbdcSupported() != null && hp.getDbdcSupported().equals(Constants.SUPPORTED)) 
					input.setDbdcSupported(Constants.SUPPORTED);			
    	   }
       }
	
	}
	
	public static void validateOnModifyHWRadio(HWRadio input) throws BadRequestException
	{		
		if(RequestValidator.isEmptyField(input.getRadioName()))
	     {     	
	     	throw new BadRequestException("Radio Name "+ExceptionMessages.EX_EMPTY_FIELD);
	     }
				
		if ((input.getMimoMode() != null)
				&& (!(input.getMimoMode().equals(Constants.MU_MIMO + " : " + Constants.SU_MIMO)
						|| input.getMimoMode().equals(Constants.SU_MIMO + " : " + Constants.MU_MIMO)
						|| input.getMimoMode().equals(Constants.SU_MIMO)
						|| input.getMimoMode().equals(Constants.MU_MIMO)))) {
			throw new BadRequestException("HW Radio MIMO should be either " + Constants.MU_MIMO + " or "
					+ Constants.SU_MIMO + " or both with (SU-MIMO : MU-MIMO).");
		}

		if ((input.getDbdcModeSupported() != null) && (!(input.getDbdcSupported().equals(Constants.SUPPORTED)
				|| input.getDbdcSupported().equals(Constants.NOTSUPPORTED)))) {
			throw new BadRequestException(
					"HW Radio DBDC should be either " + Constants.SUPPORTED + " or " + Constants.NOTSUPPORTED + ".");
		}

		if ((input.getDfsMode() != null) && (!(input.getDfsMode().equals(Constants.SUPPORTED)
				|| input.getDfsMode().equals(Constants.NOTSUPPORTED)))) {
			throw new BadRequestException(
					"HW Radio DFS should be either " + Constants.SUPPORTED + " or " + Constants.NOTSUPPORTED + ".");
		}
	}
}
