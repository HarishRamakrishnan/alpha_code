package com.wio.core.config.action.wifi;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.WiFi;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;

public class ModifyWiFi extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws AuthorizationException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, AuthorizationException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        WiFi input = getGson().fromJson(getBodyFromRequest(request), WiFi.class);
        
        WiFi wifi = null;
        try 
        {
        	WifiValidator.validateOnModify(input);
        	
        	wifi = genDAO.getGenericObject(WiFi.class, input.getWifiID());
        	
        	if(null == wifi)
                throw new DAOException("Wifi "+input.getWifiID()+" does not exist!"); 
        	
        	User loggedUser = getUserInfoFromRequest(request);

        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(wifi.getServiceName()))
        			throw new AuthorizationException("Not authorized to modify the Wifi of other service!");
        	} 
            
        	updateModifiedWifiNetwork(wifi, input);
        	genDAO.updateGenericObject(wifi);  
        	
        }        	
        catch ( DAOException e) 
        {
            logger.log("Error while modifying wifi \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }  
        return getGson().toJson(wifi, WiFi.class);        
    }

    private void updateModifiedWifiNetwork(WiFi ws, WiFi input)
    {    	   	
      	if(input.getSsid() != null && input.getSsid().trim().length() >0)
    		ws.setSsid(input.getSsid());
    	
    	if(input.getHiddenSSID() != null)
    		ws.setHiddenSSID(input.getHiddenSSID());
    	
    	
    	if(input.getEnableWireless() != null)
    		ws.setEnableWireless(input.getEnableWireless());
    	
    	if(input.getCountry() != null && input.getCountry().trim().length() > 0)
    		ws.setCountry(input.getCountry());
        	
    	if(input.getAuthenticationType() != null && input.getAuthenticationType().trim().length() > 0)
    		ws.setAuthenticationType(input.getAuthenticationType());
    		
    	if(input.getEncryptionType() != null && input.getEncryptionType().trim().length() > 0)
    		ws.setEncryptionType(input.getEncryptionType());
    	
    	if(input.getPassPhrase() != null && input.getPassPhrase().trim().length() > 0)
    		ws.setPassPhrase(input.getPassPhrase());
    	
    	if(input.getNetworkUser()!= null && input.getNetworkUser().trim().length() > 0)
    		ws.setNetworkUser(input.getNetworkUser());
    	
    
    	if(input.getDescription()!= null && input.getDescription().trim().length() > 0)
    		ws.setDescription(input.getDescription());
    	
    	ws.setLastUpdateTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
    	
    }
}
  

