package com.wio.core.config.handler;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.IRequestAction;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.core.config.helper.ConfigurationMgmtActionFactory;

/**
 * This class contains the main event handler for the Lambda function.
 */
public class ConfigRequestRouter 
{
    /**
     * The main Lambda function handler. Receives the request as an input stream, parses the json and looks for the
     * "action" property to decide where to route the request. The "body" property of the incoming request is passed
     * to the UserAction implementation as a request body.
     *
     * @param request  The InputStream for the incoming event. This should contain an "action" and "body" properties. The
     *                 action property should contain the namespaced name of the class that should handle the invocation.
     *                 The class should implement the UserAction interface. The body property should contain the full
     *                 request body for the action class.
     * @param response An OutputStream where the response returned by the action class is written
     * @param context  The Lambda Context object
     * @throws BadRequestException    This Exception is thrown whenever parameters are missing from the request or the action
     *                                class can't be found
     * @throws InternalErrorException This Exception is thrown when an internal error occurs, for example when the database
     *                                is not accessible
     * @throws DAOException 
     * @throws AuthorizationException 
     */
    public static String handler(JsonObject inputObj, OutputStream response, Context context) throws BadRequestException, InternalErrorException, DAOException, AuthorizationException 
    {
        LambdaLogger logger = context.getLogger();

        //JsonParser parser = new JsonParser();
      
        if (inputObj == null || inputObj.get("body") == null ) {
            logger.log("Invald input, could not find request body");
            throw new BadRequestException("Invald input, could not find request body");
        }
        
        
        String actionClass = inputObj.get("action").getAsString();
      
        if (actionClass == null) {
            logger.log("Action class is null");
            throw new BadRequestException("Action class is null");
        }
        
        IRequestAction action;

        action = IRequestAction.class.cast(ConfigurationMgmtActionFactory.getUserActionInstance(actionClass));

        if (action == null) {
            logger.log("Action class is null");
            throw new BadRequestException("Invalid user action ");
        }

        //JsonObject body = null;
        if (inputObj.get("body") == null) {
        	logger.log("Body tag is null");
            throw new BadRequestException("Body tag is null");
        }
        
        String output = action.handle(inputObj, context);

		try 
		{
			IOUtils.write(output, response);
		} 
		catch (final IOException e) 
		{
			logger.log("Error while writing response\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}
		
		return output;
    }
}
