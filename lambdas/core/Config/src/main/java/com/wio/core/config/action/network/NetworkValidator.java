package com.wio.core.config.action.network;

import com.wio.common.config.model.Network;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class NetworkValidator {
	
	public static void validateOnAdd(Network input) throws BadRequestException, InternalErrorException, DAOException	
	{
		if(RequestValidator.isEmptyField(input.getNetworkName()))
        	throw new BadRequestException("Network Name "+ExceptionMessages.EX_EMPTY_FIELD);
	 
        if(RequestValidator.isEmptyField(input.getServiceName()))
        	throw new BadRequestException("service Name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getNetworkType()))
        	throw new BadRequestException("Network Type "+ExceptionMessages.EX_EMPTY_FIELD);
        
        
        if(!DataValidator.isServiceExist(input.getServiceName()))
        	throw new BadRequestException("Invalid service, does not exist");
	}
}
