package com.wio.core.config.requests;

import java.util.List;

public class DeviceServiceMapRequest 
{
	private List<String> deviceID;
	private String serviceName;
	
	
	public List<String> getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(List<String> deviceID) {
		this.deviceID = deviceID;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	

}
