package com.wio.core.config.action.device;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class ViewDevices extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Device input = getGson().fromJson(getBodyFromRequest(request), Device.class);

        List<Device> devices = null;
        
        List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();
        
        if(!RequestValidator.isEmptyField(input.getDeviceID()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_ID, input.getDeviceID(),ComparisonOperator.EQ));
                 
        if(!RequestValidator.isEmptyField(input.getDeviceName()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_NAME, input.getDeviceName(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getHpID()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, input.getHpID(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getNetworkAdmin()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_USER, input.getNetworkAdmin(),ComparisonOperator.EQ));
        
        try
        {
            User loggedUser = getUserInfoFromRequest(request); 
            
            if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
            {
            	if(!RequestValidator.isEmptyField(input.getServiceName()))
            	{
            		if(loggedUser.getServiceName().equals(input.getServiceName()))
                		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(), ComparisonOperator.EQ));
            		else
            			throw new BadRequestException("Not Authorized to view the devices of service '"+input.getServiceName()+"' !");
            	}
            	else
            		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
            }
            else if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER))
            {
            	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_USER, loggedUser.getUserID(),ComparisonOperator.EQ));
            }
            else
            {
            	if(!RequestValidator.isEmptyField(input.getServiceName()))
            		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(),ComparisonOperator.EQ));
            }
            
            logger.log(""+queryFilters);
            devices = genDAO.getGenericObjects(Device.class, queryFilters);
        	
        	if(devices == null || devices.isEmpty())
        		throw new DAOException("Device does not Exist");   
        } 
        catch (final Exception e) {
            logger.log("Error while viewing Device...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(devices, List.class);
    }
}
