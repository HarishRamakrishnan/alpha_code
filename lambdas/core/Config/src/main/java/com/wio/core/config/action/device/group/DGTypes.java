package com.wio.core.config.action.device.group;

import java.util.ArrayList;
import java.util.List;

public class DGTypes 
{
	public static List<String> getTypes()
	{
		List<String> dgTypes = new ArrayList<>();
		
		dgTypes.add("Region");
		dgTypes.add("BuildingComplex");
		dgTypes.add("Building");
		dgTypes.add("Home");
		dgTypes.add("Office");
		
		return dgTypes;
	}

}