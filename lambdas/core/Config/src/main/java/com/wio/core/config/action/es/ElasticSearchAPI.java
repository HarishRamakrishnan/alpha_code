package com.wio.core.config.action.es;
/*package com.wavesio.server.configuration.action.es;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.elasticsearch.AWSElasticsearch;
import com.amazonaws.services.elasticsearch.AWSElasticsearchClientBuilder;
import com.amazonaws.services.elasticsearch.model.CreateElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.CreateElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.DomainInfo;
import com.amazonaws.services.elasticsearch.model.EBSOptions;
import com.amazonaws.services.elasticsearch.model.ElasticsearchClusterConfig;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesRequest;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesResult;
import com.wavesio.server.common.staticinfo.SystemConfigMessages;

public class ElasticSearchAPI
{
	public static void createDomain(String domainName)
	{		
		AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
				.withCredentials(new ProfileCredentialsProvider()).withRegion(SystemConfigMessages.SYSTEM_AWS_REGION).build();

		EBSOptions eBSOptions = new EBSOptions();
		eBSOptions.setEBSEnabled(true);
		eBSOptions.setVolumeType("gp2");
		eBSOptions.setVolumeSize(10);
		
		String accessPolicies = "{\"Version\": \"2012-10-17\","
				+ "\"Statement\": [{\"Effect\": \"Allow\","
				+ "\"Principal\": {\"AWS\": [\"*\"]},"
				+ "\"Action\": [\"es:*\"],"
				+ "\"Resource\": \"arn:aws:es:us-east-1:528719264515:domain/test/*\"}]}";
		
		ElasticsearchClusterConfig clusterConfig = new ElasticsearchClusterConfig();
		clusterConfig.setInstanceType("t2.small.elasticsearch");
		clusterConfig.setInstanceCount(1);
		clusterConfig.setDedicatedMasterEnabled(false);
		clusterConfig.setZoneAwarenessEnabled(false);
		
		
		CreateElasticsearchDomainRequest request = new CreateElasticsearchDomainRequest();
		request.setDomainName(domainName);
		request.setElasticsearchVersion("5.1");
		request.setEBSOptions(eBSOptions);
		request.setAccessPolicies(accessPolicies);
		request.setElasticsearchClusterConfig(clusterConfig);
		
		CreateElasticsearchDomainResult result = awsElasticsearch.createElasticsearchDomain(request);

		System.out.println(result.getDomainStatus());
		System.out.println(result.getSdkHttpMetadata());
		System.out.println(result.getSdkResponseMetadata());	
	}

	public String getESEndpoint(String domainName) 
	{
		AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
				.withCredentials(new ProfileCredentialsProvider()).withRegion(SystemConfigMessages.SYSTEM_AWS_REGION).build();
		
		ListDomainNamesResult listDomainNamesResult = awsElasticsearch.listDomainNames(new ListDomainNamesRequest());
		String endPoint = "";
		
		for (DomainInfo domain : listDomainNamesResult.getDomainNames()) 
		{			
			if(domain.equals(domainName))
			{
				DescribeElasticsearchDomainResult esDomainResult = awsElasticsearch.describeElasticsearchDomain(
			            new DescribeElasticsearchDomainRequest().withDomainName(domain.getDomainName()));	
			    endPoint = esDomainResult.getDomainStatus().getEndpoint();
			    break;
			}
		}		
		System.out.println(endPoint);
	
		return endPoint;
	}
}
*/