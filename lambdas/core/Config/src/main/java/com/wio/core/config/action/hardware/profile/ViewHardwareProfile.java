package com.wio.core.config.action.hardware.profile;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class ViewHardwareProfile extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
                
        HardwareProfile input = getGson().fromJson(getBodyFromRequest(request), HardwareProfile.class);
    
        List<HardwareProfile> hps = null;
                
        try 
        {
        	User loggedUser = getUserInfoFromRequest(request);        	
        	
        	List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();
	                	
        	if(!RequestValidator.isEmptyField(input.getHpID()))
        		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, input.getHpID(),ComparisonOperator.EQ)); 
        	
        	if(!RequestValidator.isEmptyField(input.getHpName()))
        		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_NAME, input.getHpName(),ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getHwID()))
        		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, input.getHwID(),ComparisonOperator.EQ));
        	        	
        	if(!RequestValidator.isEmptyField(input.getModel()))
        		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_MODEL, input.getModel(),ComparisonOperator.EQ));
        	
        	
        	if(!RequestValidator.isEmptyField(input.getServiceName()))
        	{
        		System.out.println("Sevice filter");
        		if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        		{
        			System.out.println("Sevice user detected");
        			if(!input.getServiceName().equals(loggedUser.getServiceName()))
        				throw new BadRequestException("Not Authorized to view other service HWProfiles!"); 
        			else
        				queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
        			
        		}
        		else
        		{
        			System.out.println("Not a service user ");
        			queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(),ComparisonOperator.EQ));
        		}
        	}
        	else
        	{
        		if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        			queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
        	}
        	
	        
	        hps = genDAO.getGenericObjects(HardwareProfile.class, queryFilters);

	        if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
	        	if(hps.isEmpty())
	        		throw new InternalErrorException("HW Profile does not exist or Not mapped to Service "+loggedUser.getServiceName());
	        
	        if(hps==null || hps.isEmpty())
	          	 throw new InternalErrorException("HW Profile does not exist !");
	        
        }
        catch (final DAOException e) 
        {
            logger.log("Error while retrieving Hardware profile\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
                
        return getGson().toJson(hps, List.class);
    }
}
