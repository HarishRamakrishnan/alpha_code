package com.wio.core.config.backup;
/*package com.wavesio.server.configuration.backup;

import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.model.configuration.WANSettings;
import com.wavesio.server.common.staticinfo.ExceptionMessages;
import com.wavesio.server.common.validation.DataValidator;
import com.wavesio.server.common.validation.RequestValidator;
import com.wavesio.shared.common.Enums.Constants;

public class ValidateWANSettings {
	


	public static void validate(WANSettings input) throws BadRequestException, DAOException{
		
		    if(RequestValidator.isEmptyField(input.getWanName()))
	        {
	         	throw new BadRequestException("WAN Settings Name "+ExceptionMessages.EX_EMPTY_FIELD);
	        }
	        
	        if(RequestValidator.isEmptyField(input.getDeviceName()))
	        {
	           	throw new BadRequestException("Device Name "+ExceptionMessages.EX_EMPTY_FIELD);
	        }
	        

	        if(!DataValidator.isDeviceExist(input.getDeviceName()))
	        {
	          	throw new BadRequestException("Device is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
	        }
	        
	        if(RequestValidator.isEmptyField(input.getWanType()))
	        {
	           	throw new BadRequestException("WAN type "+ExceptionMessages.EX_EMPTY_FIELD);
	        }
	        
	        if(RequestValidator.isEmptyField(input.getIpVersion()))
	        {
	           	throw new BadRequestException("IP version "+ExceptionMessages.EX_EMPTY_FIELD);
	        }
	        
	        if(!input.getWanType().equalsIgnoreCase(Constants.PPPOE) && !input.getWanType().equalsIgnoreCase(Constants.PPPOA) &&
	        		!input.getWanType().equalsIgnoreCase(Constants.STATIC) && !input.getWanType().equalsIgnoreCase(Constants.DYNAMIC)){
	        	throw new BadRequestException("WAN type should be either Dynamic / Static / PPPoE / PPPoA "+ExceptionMessages.EX_INVALID_INPUT);
	        }
	        
	        
	        if(!input.getIpVersion().equalsIgnoreCase(Constants.IPV4) && !input.getIpVersion().equalsIgnoreCase(Constants.IPV6)){
	        	throw new BadRequestException("IP version should be either IPv4 or IPv6 "+ExceptionMessages.EX_INVALID_INPUT);
	        }
	        
	    	if(input.getWanType().equalsIgnoreCase(Constants.PPPOE) || input.getWanType().equalsIgnoreCase(Constants.PPPOA)){
		    	
	    	    if(RequestValidator.isEmptyField(input.getUsername()))
		        {
		         	throw new BadRequestException("User Name "+ExceptionMessages.EX_EMPTY_FIELD);
		        }
	    	    
	    	    if(!RequestValidator.isValidAlphaNumaricFormat(input.getUsername()))
		        {
		         	throw new BadRequestException("User Name is not valid "+ExceptionMessages.EX_INVALID_INPUT);
		        }
	    	    
		        if(RequestValidator.isEmptyField(input.getPassword()))
		        {
		           	throw new BadRequestException("Password "+ExceptionMessages.EX_EMPTY_FIELD);
		        }	
	    		
	    	}
	    	
	    	if(input.getWanType().equalsIgnoreCase(Constants.STATIC)){
	        	
	        	if(RequestValidator.isEmptyField(input.getIpAddress())){
		    		throw new BadRequestException("IP Address "+ExceptionMessages.EX_EMPTY_FIELD);
		    	}
		    	
	            if(RequestValidator.isEmptyField(input.getDnsPrimaryAddress())){
	            	throw new BadRequestException("DNS Primary Address "+ExceptionMessages.EX_EMPTY_FIELD);	
		    	}
	            
	            if(RequestValidator.isEmptyField(input.getDnsSecondaryAddress())){
	            	throw new BadRequestException("DNS Secondary Address "+ExceptionMessages.EX_EMPTY_FIELD);
		    	}
	            
	            if(RequestValidator.isEmptyField(input.getDefaultGateway())){
	            	throw new BadRequestException("Default gateway "+ExceptionMessages.EX_EMPTY_FIELD);
		    	}
	            
	            if(RequestValidator.isEmptyField(input.getSubnetMask())){
	            	throw new BadRequestException("Subnet Mask  "+ExceptionMessages.EX_EMPTY_FIELD);
		    	}
	        	
	        	
	    	if(input.getIpVersion().equalsIgnoreCase(Constants.IPV4)){
	    		
	    	if(!RequestValidator.validateIPAddress(input.getIpAddress())){
	    		throw new BadRequestException("IP Address is not valid "+ExceptionMessages.EX_INVALID_INPUT);
	    	}
	    	
            if(!RequestValidator.validateIPAddress(input.getDnsPrimaryAddress())){
            	throw new BadRequestException("DNS Primary Address is not valid "+ExceptionMessages.EX_INVALID_INPUT);	
	    	}
            
            if(!RequestValidator.validateIPAddress(input.getDnsSecondaryAddress())){
            	throw new BadRequestException("DNS Secondary Address is not valid "+ExceptionMessages.EX_INVALID_INPUT);
	    	}
            
            if(!RequestValidator.validateIPAddress(input.getDefaultGateway())){
            	throw new BadRequestException("Default gateway is not valid "+ExceptionMessages.EX_INVALID_INPUT);
	    	}
            
            if(!RequestValidator.validateIPAddress(input.getSubnetMask())){
            	throw new BadRequestException("Subnet Mask is not valid "+ExceptionMessages.EX_INVALID_INPUT);
	    	}
            
                        
	    	}else{
	    		// Need to validate with IPV6 version format
	    		if(!RequestValidator.validateIPV6Address(input.getIpAddress())){
		    		throw new BadRequestException("IP Address is not valid "+ExceptionMessages.EX_INVALID_INPUT);
		    	}
		    	
	            if(!RequestValidator.validateIPV6Address(input.getDnsPrimaryAddress())){
	            	throw new BadRequestException("DNS Primary Address is not valid "+ExceptionMessages.EX_INVALID_INPUT);	
		    	}
	            
	            if(!RequestValidator.validateIPV6Address(input.getDnsSecondaryAddress())){
	            	throw new BadRequestException("DNS Secondary Address is not valid "+ExceptionMessages.EX_INVALID_INPUT);
		    	}
	            
	            if(!RequestValidator.validateIPV6Address(input.getDefaultGateway())){
	            	throw new BadRequestException("Default gateway is not valid "+ExceptionMessages.EX_INVALID_INPUT);
		    	}
	            
	            if(!RequestValidator.validateIPV6Address(input.getSubnetMask())){
	            	throw new BadRequestException("Subnet Mask is not valid "+ExceptionMessages.EX_INVALID_INPUT);
		    	}
	            
	    	 }
	        }
	    	
	    
	    	//need to validate IPv6 address prefix
	    	
	    	// Need to validate mandate ip all ip address field if DHCP server disable

	    		
	}
	
	
	
}
*/