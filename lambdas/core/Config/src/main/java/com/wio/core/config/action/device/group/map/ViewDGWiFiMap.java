package com.wio.core.config.action.device.group.map;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.DGWifiMap;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.validation.RequestValidator;


public class ViewDGWiFiMap extends AbstractAction
{	
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

	@Override
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{		
        logger = lambdaContext.getLogger();     
        genDAO = GenericDAOFactory.getGenericDAO();   
        
        DGWifiMap input = getGson().fromJson(getBodyFromRequest(request), DGWifiMap.class); 
        
        List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();
        
        if(!RequestValidator.isEmptyField(input.getDgID()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_ID, input.getDgID(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getWifiID()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.WIFI_ID, input.getWifiID(),ComparisonOperator.EQ));
                
	    List<DGWifiMap> dgwifi = new ArrayList<>();
	    
  	    try 
        {   
  	    	dgwifi = genDAO.getGenericObjects(DGWifiMap.class, queryFilters);
        } 
  	    catch ( DAOException e) 
  	    {
            logger.log("Error while viewing Unassigned DGs \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
  		
        if(dgwifi.isEmpty())
          	 throw new InternalErrorException("No DG to Wifi map found");	
             
        return getGson().toJson(dgwifi, List.class);
    }
}

