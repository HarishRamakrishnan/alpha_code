package com.wio.core.config.action.service;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Service;
import com.wio.common.config.request.ServiceRequest;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.util.PasswordHelper;
import com.wio.common.validation.RequestValidator;

public class ModifyService extends AbstractAction {
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;    

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        ServiceRequest input = getGson().fromJson(getBodyFromRequest(request), ServiceRequest.class);
                
        if(RequestValidator.isEmptyField(input.getServiceName()))
        	throw new BadRequestException("Service name "+ExceptionMessages.EX_EMPTY_FIELD);
       
        Service service = null;
        try {        	
        	service = genDAO.getGenericObject(Service.class, input.getServiceName());
        
            if (service == null) 
                throw new BadRequestException("Service Name "+input.getServiceName()+" does not exist");
        
            updateModifiedServiceDetails(service, input);  
             
            genDAO.updateGenericObject(service);
        } 
        catch ( DAOException e) {
            logger.log("Error while modifying Service \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }       

        return getGson().toJson(service, Service.class);    
    }
    
    private void updateModifiedServiceDetails(Service service, ServiceRequest input) throws BadRequestException
    {	
    	if(!RequestValidator.isEmptyField(input.getDescription()))
    		service.setDescription(input.getDescription());
    	
    	if(!RequestValidator.isEmptyField(input.getAddress()))
    		service.setAddress(input.getAddress());
    	
    	if(!RequestValidator.isEmptyField(input.getCity()))
    		service.setCity(input.getCity());
    	
    	if(!RequestValidator.isEmptyField(input.getPincode()))
    	  service.setPincode(input.getPincode());
    	
    	if(!RequestValidator.isEmptyField(input.getEmail()))
        {
        	if(!RequestValidator.validateEmail(input.getEmail()))
                throw new BadRequestException(ExceptionMessages.EX_INVALID_EMAIL);
        	else
        		 service.setEmail(input.getEmail());
        }
    	    	
    	if(!RequestValidator.isEmptyField(input.getAcsURL()))
      	  service.setAcsURL(input.getAcsURL());
    	
    	if(!RequestValidator.isEmptyField(input.getAcsUsername()))
      	  service.setAcsUsername(input.getAcsUsername());
    	
    	if(!RequestValidator.isEmptyField(input.getCpeUsername()))
        	  service.setCpeUsername(input.getCpeUsername());
    	
    	   	
		try {
			if (!RequestValidator.isEmptyField(input.getAcsPassword().toString())
					&& !PasswordHelper.getInstance().authenticate(input.getAcsPassword().toString(),
							service.getAcsPassword().array(), service.getAcsPwdSalt().array())) {
				Map<String, byte[]> saltPWDMap = PasswordHelper.getInstance()
						.getEncPassword(input.getAcsPassword().toString());
				if (!saltPWDMap.isEmpty()) {
					service.setAcsPassword(ByteBuffer.wrap(saltPWDMap.get("ENC_PWD")));
					service.setAcsPwdSalt(ByteBuffer.wrap(saltPWDMap.get("SALT")));
				}
			} 
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.log("Algorithim not provided to generate ACS Encrypted Password. Cause - " + e.getMessage());
		}

		try {
			if (!RequestValidator.isEmptyField(input.getCpePassword().toString())
					&& !PasswordHelper.getInstance().authenticate(input.getCpePassword().toString(),
							service.getCpePassword().array(), service.getCpePwdSalt().array())) {
				Map<String, byte[]> saltPWDMap = PasswordHelper.getInstance()
						.getEncPassword(input.getCpePassword().toString());
				if (!saltPWDMap.isEmpty()) {
					service.setCpePassword(ByteBuffer.wrap(saltPWDMap.get("ENC_PWD")));
					service.setCpePwdSalt(ByteBuffer.wrap(saltPWDMap.get("SALT")));
				}
			} 
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.log("Algorithim not provided to generate CPE Encrypted Password. Cause - " + e.getMessage());
		}
    	
    }
}
