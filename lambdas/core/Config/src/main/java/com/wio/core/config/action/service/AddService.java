package com.wio.core.config.action.service;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Service;
import com.wio.common.config.request.ServiceRequest;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.util.PasswordHelper;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new Service.
 * <p/>
 * POST to /service/
 */
public class AddService extends AbstractAction {
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add user action. It expects a UserRequest object in input and returns
     * a serialized UserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        ServiceRequest input = getGson().fromJson(getBodyFromRequest(request), ServiceRequest.class);

        validate(input);
        
        try {
        	Service service = genDAO.getGenericObject(Service.class, input.getServiceName());
        	
            if (null != service)
                throw new BadRequestException("Service already exist !");

            service = new Service();
            Map<String, byte[]> saltPWDMap = PasswordHelper.getInstance().getEncPassword(input.getAcsPassword().toString());
            if(!saltPWDMap.isEmpty()) {
            	service.setAcsPassword(ByteBuffer.wrap(saltPWDMap.get("ENC_PWD")));
            	service.setAcsPwdSalt(ByteBuffer.wrap (saltPWDMap.get("SALT")));
            }
            
            saltPWDMap = PasswordHelper.getInstance().getEncPassword(input.getCpePassword().toString());
            if(!saltPWDMap.isEmpty()) {
            	service.setCpePassword(ByteBuffer.wrap(saltPWDMap.get("ENC_PWD")));
            	service.setCpePwdSalt(ByteBuffer.wrap(saltPWDMap.get("SALT")));
            }
            
            service.setServiceName(input.getServiceName());
            service.setAcsUsername(input.getAcsUsername());
            service.setCpeUsername(input.getCpeUsername());
            service.setAcsURL(input.getAcsURL());
            service.setAddress(input.getAddress());
            service.setPincode(input.getPincode());
            service.setCity(input.getCity());
            service.setCountry(input.getCountry());
            service.setCpeGatewayURL(input.getCpeGatewayURL());
            service.setEmail(input.getEmail());
            
            service.setCreatedBy(getUserInfoFromRequest(request).getUserID());
            service.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
            service.setLastUpdateTime("NA");
            
            genDAO.saveGenericObject(service);
            
            //ElasticSearchAPI.createDomain(input.getServiceName());
        } 
        catch (final DAOException e) {
            logger.log("Error while adding service \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        Service newService = new Service();
        newService.setServiceName(input.getServiceName());

        return getGson().toJson(newService, Service.class);
    }
   

	public static void validate(ServiceRequest input) throws BadRequestException {
    	if(RequestValidator.isEmptyField(input.getServiceName()))
        	throw new BadRequestException("Service name "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(!RequestValidator.isEmptyField(input.getEmail())) {
        	if(!RequestValidator.validateEmail(input.getEmail()))
                throw new BadRequestException(ExceptionMessages.EX_INVALID_EMAIL);
        }
        else 
        	throw new BadRequestException("Email ID "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getAddress()))
        	throw new BadRequestException("Address  "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getCity()))
        	throw new BadRequestException("City "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getPincode()))
        	throw new BadRequestException("Pincode "+ExceptionMessages.EX_EMPTY_FIELD);
        
        if(RequestValidator.isEmptyField(input.getCountry()))
        	throw new BadRequestException("Country "+ExceptionMessages.EX_EMPTY_FIELD);
       	    	
    }
}
