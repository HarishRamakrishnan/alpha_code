package com.wio.core.config.requests;

public class DeviceTopicsRequest 
{
	private String deviceID;

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
}
