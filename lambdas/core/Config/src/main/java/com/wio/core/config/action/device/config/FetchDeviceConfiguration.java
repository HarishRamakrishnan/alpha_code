package com.wio.core.config.action.device.config;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.DGWifiMap;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.WiFi;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.DeviceFirmware;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.firmware.Firmware;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.generic.dao.helper.DynamoDBHelper;
import com.wio.common.mqtt.MQTTConnection;
import com.wio.common.mqtt.MQTTPublisher;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class FetchDeviceConfiguration extends AbstractAction {
	private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

	/**
	 * Handler implementation for the add ap action. It expects a AddAPRequest
	 * object in input and returns a serialized APResponse object
	 *
	 * @param request
	 *            Receives a JsonObject containing the body content
	 * @param lambdaContext
	 *            The Lambda context passed by the AWS Lambda environment
	 * @return Returns the new user identifier and a set of temporary AWS
	 *         credentials as a AddAPResponse object
	 * @throws BadRequestException
	 * @throws InternalErrorException
	 */
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException {
		logger = lambdaContext.getLogger();
		genDAO = GenericDAOFactory.getGenericDAO();

		DeviceConfiguration input = getGson().fromJson(getBodyFromRequest(request), DeviceConfiguration.class);

		DeviceConfiguration deviceConfiguration = null;

		if (RequestValidator.isEmptyField(input.getDeviceId()))
			throw new BadRequestException("Device ID cannot be empty !");

		try {
			Device device = genDAO.getGenericObject(Device.class, input.getDeviceId());

			if (null == device)
				throw new DAOException("Device does not Exist");

			deviceConfiguration = prepareConfiguration(device, lambdaContext);

			if (null == deviceConfiguration)
				throw new DAOException("Unable to fetch the configuration, returing null !");

			String confg = getGson().toJson(deviceConfiguration, DeviceConfiguration.class);

			// Publishing device configuration to default topic
			MQTTConnection conn = new MQTTConnection();
			MqttClient con = conn.getConnection(System.getenv("MQTT_CLIENT_ID"), 0, false, false);
			String publishStatus = MQTTPublisher.publishMessage(con, confg, device.getDefaultTopic(),
					Integer.parseInt(System.getenv("MQTT_TOPIC_DEFAULT_QOS")), true);

			if (publishStatus.equals("SUCCESS"))
				logger.log("Configuration published to device topic: " + device.getDefaultTopic());
			else
				logger.log("Unable to publish configuration to device topic, MQTT Broker issue !");
		} catch (final Exception e) {
			logger.log("Error while fetching Device Configiration...\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}

		return getGson().toJson(deviceConfiguration, DeviceConfiguration.class);
	}

	public DeviceConfiguration prepareConfiguration(Device device, Context lambdaContext) throws DAOException {
		logger = lambdaContext.getLogger();
		genDAO = GenericDAOFactory.getGenericDAO();

		logger.log("Fetching hardware profile !");
		// Fetch hardware profile
		if (RequestValidator.isEmptyField(device.getHpID()))
			throw new DAOException("No Hardware Profile created for this device: " + device.getDeviceID());

		HardwareProfile hp = genDAO.getGenericObject(HardwareProfile.class, device.getHpID());

		if (null == hp)
			throw new DAOException("Device hardware profile does not exist or not avilable !");

		logger.log("Fetching DeviceGroup details!");
		// Fetch Wifi settings
		if (RequestValidator.isEmptyField(device.getDgID()))
			throw new DAOException("Device is not under any DeviceGroup / Provision!");

		DeviceGroup dg = genDAO.getGenericObject(DeviceGroup.class, device.getDgID());

		if (null == dg)
			throw new DAOException("DeviceGroup does not exist or not avilable !");

		List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
		filterConditions
				.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_ID, dg.getDgID(), ComparisonOperator.EQ));

		List<DGWifiMap> dgwifi = genDAO.getGenericObjects(DGWifiMap.class, filterConditions);

		List<WiFiSettings> wifiSettings = new ArrayList<>();

		if (dgwifi.isEmpty())
			throw new DAOException("No WiFi settings asigned to this Device !");

		logger.log("Fetching Wifi Settings !");
		dgwifi.forEach(mapping -> {
			try {
				WiFi wifi = genDAO.getGenericObject(WiFi.class, mapping.getWifiID());

				if (null == wifi)
					throw new DAOException("No wifi/wireless settings assigned to this Device !");

				WiFiSettings wifiSetting = new WiFiSettings();

				wifiSetting.setWifiID(wifi.getWifiID());
				wifiSetting.setWifiName(wifi.getWifiName());
				wifiSetting.setServiceName(wifi.getServiceName());
				wifiSetting.setSsid(wifi.getSsid());
				wifiSetting.setHiddenSSID(wifi.getHiddenSSID());
				wifiSetting.setCountry(wifi.getCountry());
				wifiSetting.setNetworkUser(wifi.getNetworkUser());
				wifiSetting.setAuthenticationType(wifi.getAuthenticationType());
				wifiSetting.setEncryptionType(wifi.getEncryptionType());
				wifiSetting.setPassPhrase(wifi.getPassPhrase());
				wifiSetting.setEnableWireless(wifi.getEnableWireless());

				if (wifi.getNetworkID().equals("NA") || wifi.getNetworkID() == null)
					throw new DAOException("No network asigned to WiFi !");

				logger.log("Fetching Network Settings !");

				Network network = genDAO.getGenericObject(Network.class, wifi.getNetworkID());

				if (null == network)
					throw new DAOException("Network " + wifi.getNetworkID() + " does not exist !");

				logger.log("Preparing Network settings !");

				NetworkSettings networkSetting = new NetworkSettings();

				networkSetting.setNetworkID(network.getNetworkID());
				networkSetting.setServiceName(network.getServiceName());
				networkSetting.setNetworkName(network.getNetworkName());
				networkSetting.setNetworkType(network.getNetworkType());
				networkSetting.setZtEnable(network.getZtEnable());
				networkSetting.setDhcpServer(network.getDhcpServer());
				networkSetting.setIpPoolStart(network.getIpPoolStart());
				networkSetting.setIpPoolEnd(network.getIpPoolEnd());
				networkSetting.setLeaseTime(network.getLeaseTime());

				logger.log("Adding MAC Filter List");
				networkSetting.setMacFilterList(network.getMacFilterList());

				logger.log("Adding Network settings to WifiSettings !");
				wifiSetting.setNetworkSettings(networkSetting);

				wifiSettings.add(wifiSetting);
			} catch (DAOException e) {
				e.printStackTrace();
			}
		});

		logger.log("Fetching Firmware info !");

		// Fetching firmware info
		if (device.getFirmware().equals("NA") || null == device.getFirmware())
			throw new DAOException("No firmware asigned to this device !");

		Firmware fw = genDAO.getGenericObject(Firmware.class, device.getFirmware());

		if (null == fw)
			throw new DAOException("Firmware does not exist or not avilable !");

		DeviceFirmware firmware = new DeviceFirmware();

		firmware.setFwID(fw.getFwID());
		firmware.setFilename(fw.getFilename());
		firmware.setVersion(fw.getVersion());
		firmware.setVersionType(fw.getVersionType());
		firmware.setChecksum(fw.getChecksum());
		firmware.setChecksumType(fw.getChecksumType());
		firmware.setEdgeURL(fw.getEdgeURL());

		logger.log("Preparing device configuration !");

		// Prepare device configuration
		DeviceConfiguration configuration = new DeviceConfiguration();

		configuration.setDeviceId(device.getDeviceID());
		configuration.setDeviceName(device.getDeviceName());
		configuration.setServiceName(device.getServiceName());
		configuration.setModel(device.getModel());
		configuration.setMac(device.getMac());
		configuration.setNetworkAdmin(device.getNetworkAdmin());
		configuration.setDefaultTopic(device.getDefaultTopic());

		configuration.setRadioSettings(device.getRadioSettings());

		configuration.setLanSettings(DynamoDBHelper.getInstance().getLanSetting(device.getLanSettings()));
		configuration.setWanSetting(device.getWanSettings());

		configuration.setWifiSettings(wifiSettings);

		configuration.setFirmware(firmware);

		return configuration;
	}
}
