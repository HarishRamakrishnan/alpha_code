package com.wio.core.config.action.hardware;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HWServiceMap;
import com.wio.common.config.model.Hardware;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class ViewHardware extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Hardware input = getGson().fromJson(getBodyFromRequest(request), Hardware.class);

        List<Hardware> hws = null;
        	         
        try
        {
        	User loggedUser = getUserInfoFromRequest(request);    
        	
        	List<FilterCondition> filterHW=new ArrayList<FilterCondition>();
	        
        	if(!RequestValidator.isEmptyField(input.getHwID()))
    			filterHW.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, input.getHwID(),ComparisonOperator.EQ));
        	
        	if(!RequestValidator.isEmptyField(input.getHwName()))
    			filterHW.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_NAME, input.getHwName(),ComparisonOperator.EQ));
    		
    		if(!RequestValidator.isEmptyField(input.getHwType()))
    			filterHW.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_TYPE, input.getHwType(),ComparisonOperator.EQ));
    		
    		if(!RequestValidator.isEmptyField(input.getManufacture()))
    			filterHW.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_MANUFACTURE, input.getManufacture(),ComparisonOperator.EQ));
        	    		
    		
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
    		{
        		System.out.println("Service user detected!");
        		
        		List<FilterCondition> filter=new ArrayList<FilterCondition>();
        		
        		filter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
        		
        		List<HWServiceMap> hwServiceMap = genDAO.getGenericObjects(HWServiceMap.class, filter);
        		
        		if(null == hwServiceMap || hwServiceMap.isEmpty())
        			throw new BadRequestException("No Hardware mapped to the Service: '"+loggedUser.getServiceName()+"' !");        		
        		
        		if(!RequestValidator.isEmptyField(input.getHwID()))
        		{    				
    				boolean matched = false;
    				for(HWServiceMap map : hwServiceMap)
    				{
    					if(map.getHwID().equals(input.getHwID()))
    					{
    						matched = true;
    						break;
    					}
    				}
    				
    				
    				System.out.println("match checking");
        			if(matched)
        			{   
        				System.out.println("matched"); 
        				System.out.println("service: "+loggedUser.getServiceName());
        				System.out.println("hw name: "+input.getHwID());
        				
        				filterHW.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, input.getHwID(),ComparisonOperator.EQ));
        				System.out.println("getting hw");
        				hws = genDAO.getGenericObjects(Hardware.class, filterHW);
        				System.out.println("hw fetched");
        			}
        			else
        				throw new BadRequestException("Hardware '"+input.getHwName()+"', not mapped under your service '"+loggedUser.getServiceName()+"' ");
        		}
        		else
        		{	
        			hws = new ArrayList<Hardware>();
        			
        			for(HWServiceMap hsmap : hwServiceMap)
        			{        				
        				filterHW.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HW_ID, hsmap.getHwID(),ComparisonOperator.EQ));
        				
        				hws = genDAO.getGenericObjects(Hardware.class, filterHW);        				
        			}
        		}
    		}
        	else
        		hws = genDAO.getGenericObjects(Hardware.class, filterHW);
        	
        	if(hws==null || hws.isEmpty())
	          	 throw new InternalErrorException("No hardware found !");
        	
        }
        catch (final Exception e) 
        {
            logger.log("Error while retrieving Hardware !\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(hws, List.class);
    }
}
