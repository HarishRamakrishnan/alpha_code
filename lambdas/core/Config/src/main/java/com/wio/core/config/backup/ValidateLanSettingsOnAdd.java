package com.wio.core.config.backup;
/*package com.wavesio.server.configuration.backup;

import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.model.configuration.DHCPServers;
import com.wavesio.server.common.model.configuration.LANSettings;
import com.wavesio.server.common.model.configuration.MacFilterList;
import com.wavesio.server.common.staticinfo.ExceptionMessages;
import com.wavesio.server.common.validation.DataValidator;
import com.wavesio.server.common.validation.RequestValidator;
import com.wavesio.shared.common.Enums.Constants;

public class ValidateLanSettingsOnAdd 
{
	public static void validate(LANSettings input) throws BadRequestException, DAOException
	{
		//Mandatory filed validations
        if(RequestValidator.isEmptyField(input.getLanName()))
        {
        	//logger.log("LAN name cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("LAN name "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        if(InputValidator.isEmptyField(input.getDeviceName()))
        {
        	//logger.log("LAN name cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Device name "+ExceptionMessages.EX_EMPTY_FIELD);
        }
        
        //Field format validations
        
        if(input.getVlanId() != null)
    	{
    		if(!RequestValidator.isValidNumber(input.getVlanId()) && input.getVlanId().toString().length() > 4)
    		{
    			//logger.log("Invalid VLAN ID format, Exception at "+this.getClass().getName());
            	throw new BadRequestException("Invalid VLAN ID format "+ExceptionMessages.EX_INVALID_INPUT);
    		}
    		if(input.getVlanId().intValue() > 4094)
    		{
    			//logger.log("VLAN ID shoud be 0 to 4094, Exception at "+this.getClass().getName());
            	throw new BadRequestException("VLAN ID shoud be 0 to 4094 "+ExceptionMessages.EX_INVALID_INPUT);
    		}
    	}
    	else
    		input.setVlanId(0);
                
        if(!RequestValidator.isValidAlphaNumaricFormat(input.getLanName()))
        {
        	//logger.log("Invalid Lan Name, Only alphanumaric characters allowed! Exception at "+this.getClass().getName());
        	throw new BadRequestException("Invalid Lan Name, Only alphanumaric characters allowed! "+ExceptionMessages.EX_INVALID_INPUT);
        }
        
        if(!RequestValidator.validateIPAddress(input.getIpAdrress()))
        {
        	//logger.log("Invalid IP Address, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Invalid IP Address "+ExceptionMessages.EX_INVALID_IP);
        }
        
        if(!RequestValidator.validateIPAddress(input.getSubnetMask()))
        {
        	//logger.log("Invalid SubNetMask IP, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Invalid SubNetMask IP! "+ExceptionMessages.EX_INVALID_INPUT);
        }
        
        if(!RequestValidator.validateIPAddress(input.getDefaultGateway()))
        {
        	//logger.log("Invalid GateWay IP, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Invalid Gateway IP! "+ExceptionMessages.EX_INVALID_INPUT);
        }
        
        if(!RequestValidator.validateIPAddress(input.getDnsPrimaryServer()))
        {
        	//logger.log("Invalid Server IP, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Invalid DNS Server IP! "+ExceptionMessages.EX_INVALID_INPUT);
        }
        

        
        //Dependency validations
        if(!DataValidator.isDeviceExist(input.getDeviceName()))
        {
        	//logger.log("AP Settings is invalid or does not exist, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Device is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        }
       
        if(input.getWifiName()!=null && !DataValidator.isWirelessNetworkExist(input.getWifiName()))
        {
        	//logger.log("Wlan is invalid or does not exist, Exception at "+this.getClass().getName());
        	throw new BadRequestException("WiFi Name is invalid or does not exist "+ExceptionMessages.EX_INVALID_INPUT);
        }
        
        
        //Child objects validation
        if(input.getDhcpServers()!=null){ 
         if(!input.getDhcpServers().isEmpty())
         {
        	 
	        for(DHCPServers dhcp : input.getDhcpServers())
	        {   
	        	
	        	System.out.println("dhcpServer disable"+dhcp.getDhcpServer().equals(Constants.DISABLE));
	        	System.out.println("dhcpServer enable"+dhcp.getDhcpServer().equals(Constants.ENABLE));
	        	if(!(dhcp.getDhcpServer().equals(Constants.DISABLE) || dhcp.getDhcpServer().equals(Constants.ENABLE))){
	        		
	        			
	        			throw new BadRequestException("dhcpServer can only be Enable/Disable");
	        	}
	        	if(dhcp.getDhcpServer().equals(Constants.ENABLE))
	        	{
	        		if(RequestValidator.isEmptyField(dhcp.getIpPoolStart()))
	        			throw new BadRequestException("IP Pool Start IP... "+ExceptionMessages.EX_EMPTY_FIELD);
	        		if(RequestValidator.isEmptyField(dhcp.getIpPoolEnd()))
	        			throw new BadRequestException("IP Pool End IP... "+ExceptionMessages.EX_EMPTY_FIELD);
	        		if(RequestValidator.isEmptyField(dhcp.getLeaseTime()))
	        			throw new BadRequestException("Lease Time... "+ExceptionMessages.EX_EMPTY_FIELD);
	        	}
	        	if(!RequestValidator.isEmptyField(dhcp.getIpPoolStart())&&!RequestValidator.validateIPAddress(dhcp.getIpPoolStart()))
	        	{
	        		//logger.log("Invalid IP Pool Start IP, Exception at "+this.getClass().getName());
	            	throw new BadRequestException("Invalid IP Pool Start IP... "+ExceptionMessages.EX_INVALID_IP);
	        	}
	        	
	        	if(!RequestValidator.isEmptyField(dhcp.getIpPoolEnd())&&!RequestValidator.validateIPAddress(dhcp.getIpPoolEnd()))
	        	{
	        		//logger.log("Invalid IP Pool End IP, Exception at "+this.getClass().getName());
	            	throw new BadRequestException("Invalid IP Pool End IP... "+ExceptionMessages.EX_INVALID_IP);
	        	}
	        	
	        	if(!RequestValidator.isEmptyField(dhcp.getLeaseTime())){
	        	  if(dhcp.getLeaseTime().intValue() <= 0 && dhcp.getLeaseTime().intValue() >= 8640)
	        	  {
	        		//logger.log("Invalid Lease Time, it should be between 0 to 8640 seconds, Exception at "+this.getClass().getName());
	            	throw new BadRequestException("Invalid Lease Time, it should be from within 0 to 8640 seconds! "+ExceptionMessages.EX_INVALID_INPUT);
	        	  }
	        	}
	        }
         }

        }
        
        if(input.getMacFilterList()!=null){
         if(!input.getMacFilterList().isEmpty())
         {
	        for(MacFilterList mac : input.getMacFilterList())
	        {
	        	if(!RequestValidator.isValidMACFormat(mac.getMacAddress()))
	        	{
	        		//logger.log("Invalid MAC Address, Exception at "+this.getClass().getName());
	            	throw new BadRequestException("Invalid MAC Address... "+ExceptionMessages.EX_INVALID_INPUT);
	        	}
	        }
         }
        }
        
	}

}
*/