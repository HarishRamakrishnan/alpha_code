package com.wio.core.config.action.hardware.map;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.HWServiceMap;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.config.requests.HWServiceMapRequest;


public class AssignHWToService extends AbstractAction
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

	@Override
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        HWServiceMapRequest input = getGson().fromJson(getBodyFromRequest(request), HWServiceMapRequest.class);
        
  		List<HWServiceMap> mapping = new ArrayList<HWServiceMap>();
  		
  	    try 
        {
  	    	if(RequestValidator.isEmptyField(input.getServiceName()))
  	    		throw new BadRequestException("Service cannot be null or empty !");
  	    	
  	    	if(!DataValidator.isServiceExist(input.getServiceName()))
  	    		throw new BadRequestException("Service does not exist, invalid service !");
  	    	
  	    	
  	    	if(!input.getHwID().isEmpty())
  	    	{
  	    		input.getHwID().forEach(hw->
  	  	    	{
	  	  	    	try 
	  	  	    	{
						if(DataValidator.isHardwareExist(hw))
						{
							if(null != genDAO.getGenericObject(HWServiceMap.class, hw, input.getServiceName()))
								throw new BadRequestException("Hardware "+hw+" is already assigned to the service !");
							else
							{
								HWServiceMap map = new HWServiceMap();
								map.setHwID(hw);
								map.setServiceName(input.getServiceName());
								
								if(!RequestValidator.isEmptyField(input.getDescription()))
									map.setDescription(input.getDescription());
								
								mapping.add(map);
							}
						}
						else
							throw new BadRequestException("Hardware does not exist, invalid hardware !");
					} 
	  	  	    	catch (DAOException | BadRequestException e) 
	  	  	    	{
						e.printStackTrace();
					}
  	  	    	});
  	    		
  	    		genDAO.saveGenericObjects(mapping);
  	    	}
  	    	else
    		{
    			throw new BadRequestException("At least one hardware should be assign to Service...");
    		}
  	    	
        } 
  	    catch (final DAOException e) 
  	    {
            logger.log("Error while assigning Hardware to Service... \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
  	    return getGson().toJson(mapping, List.class);
    }   
}

