package com.wio.core.config.action.wifi;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.WiFi;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class DeleteWiFi extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        WiFi input = getGson().fromJson(getBodyFromRequest(request), WiFi.class);
        
        if(RequestValidator.isEmptyField(input.getWifiID()))
			throw new BadRequestException("getWifiID field"+ExceptionMessages.EX_EMPTY_FIELD);
        
        WiFi wifi = null;
        		
        try 
        {
        	User loggedUser = getUserInfoFromRequest(request);
        	
        	wifi = genDAO.getGenericObject(WiFi.class, input.getWifiID());
        	
        	if(null == wifi)
        		throw new DAOException("WiFi setttings does not exist !");
        		
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(!loggedUser.getServiceName().equals(wifi.getServiceName()))
        			throw new BadRequestException("Not authorized to delete the Wifi of the Service: '"+input.getServiceName()+"' !");
        	}
        	
        	if(DataValidator.isWiFiDependent(wifi.getWifiID()))
        		throw new DAOException("WiFi dependant to other objects !");
        	        	
        	genDAO.deleteGenericObject(wifi);
        } 
        catch ( DAOException e) 
        {
            logger.log("Error while deleting Wifi\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
       
        WiFi output = new WiFi();
        
        output.setWifiID(input.getWifiID());
        
        return getGson().toJson(output, WiFi.class);       
    }
}
