package com.wio.core.config.action.device.group.map;


import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.config.requests.DeviceDGMapRequest;

public class AssignDeviceToGroup extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        DeviceDGMapRequest input = getGson().fromJson(getBodyFromRequest(request), DeviceDGMapRequest.class);
               
        try 
        {
        	List<Device> devices = new  ArrayList<Device>();
        	
        	if(RequestValidator.isEmptyField(input.getDgID()))
    		{
    			throw new BadRequestException("DeviceGroup "+ExceptionMessages.EX_EMPTY_FIELD);
    		}
    		
    		if(!DataValidator.isDGExist(input.getDgID()))
    		{
    			throw new BadRequestException("Device group does not exist !");
    		}		
    		
    		if(!input.getDeviceID().isEmpty())
    		{
    			for(String deviceID : input.getDeviceID())
    		    {
    				Device device = new Device();
    				
    		    	if(!DataValidator.isDeviceExist(deviceID))
    		    	{
    		    		throw new BadRequestException("Device "+ deviceID +" does not exist !");
    		    	}
    		    	
    		    	device = genDAO.getGenericObject(Device.class, deviceID);
    		    	
    		    	if(device.getDgID().equals(input.getDgID()))
    		    	{
    		    		throw new BadRequestException("Device "+deviceID+" is already assigned to DG: "+input.getDgID());
    		    	}
    		    	else if(!device.getDgID().equals("NA"))
    		    	{
    		    		throw new BadRequestException("Device "+deviceID+" is already assigned to a DG: "+device.getDgID());
    		    	}
    		    	
    		    	device.setDgID(input.getDgID());
    		    	device.setStatus("READY_FOR_PROVISION");
    		    	device.setDefaultTopic(genDAO.getGenericObject(DeviceGroup.class, input.getDgID()).getMqttTopic() +"/"+device.getDeviceID());
    		    	devices.add(device);
    		    }
    		}
    		else
    		{
    			throw new BadRequestException("At least one device should be assign, Device "+ExceptionMessages.EX_EMPTY_FIELD);
    		}
    		
        	genDAO.saveGenericObjects(devices);        	
        } 
        catch ( DAOException e) 
        {
            logger.log("Error while linking Device to DG \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(input, DeviceDGMapRequest.class);
    }
}
