package com.wio.core.config.action.hardware;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Hardware;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

public class DeleteHardware extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Hardware input = getGson().fromJson(getBodyFromRequest(request), Hardware.class);
 
        if(RequestValidator.isEmptyField(input.getHwID()))
        {
        	logger.log("Hardware ID cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("Hardware ID  "+ExceptionMessages.EX_EMPTY_FIELD);
        }
       
        Hardware hw = null;
        try 
        {
        	hw = genDAO.getGenericObject(Hardware.class, input.getHwID());
        	
        	if(null == hw)
        	{
        		throw new BadRequestException("Invalid Hardware ID, Hardware does not exist !");
        	}
        	
        	if(DataValidator.isHWDependent(hw.getHwID()))
        	{
        		throw new BadRequestException("Cannot delete Hardware, dependent with other object !");
        	}
        
            genDAO.deleteGenericObject(hw);
        } 
        catch ( DAOException e) 
        {
            logger.log("Error while deleting Hardware..\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        Hardware output = new Hardware();
        
        output.setHwID(input.getHwID());

        return getGson().toJson(output, Hardware.class);    
    }
}
