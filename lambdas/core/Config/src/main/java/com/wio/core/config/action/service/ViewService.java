package com.wio.core.config.action.service;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Service;
import com.wio.common.config.request.ServiceRequest;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to get all Device Group.
 * <p/>
 * POST to /service/all
 */
public class ViewService extends AbstractAction 
{
    private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        ServiceRequest input = getGson().fromJson(getBodyFromRequest(request), ServiceRequest.class);
        
        //TODO Need to check whether the user has privilege to view the service details.
        
        List<FilterCondition> queryFilters = new ArrayList<FilterCondition>();

        if(!RequestValidator.isEmptyField(input.getServiceName()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getEmail()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.EMAIL, input.getEmail(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getCity()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_CITY, input.getCity(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getCountry()))
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_COUNTRY, input.getCountry(),ComparisonOperator.EQ));
        
        List<Service> services = null;
        
        try 
        {
        	services = genDAO.getGenericObjects(Service.class, queryFilters);
        	
        	if(null == services)
        		throw new BadRequestException("No service found !");        		
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while viewing service \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 
        
        return getGson().toJson(services, List.class);
    }
}
