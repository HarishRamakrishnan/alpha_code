package com.wio.core.config.action.device.group;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.DeviceGroup;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to get all Device Group.
 * <p/>
 * POST to /dg/all
 */
public class ViewDeviceGroup extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO dao  = null;

    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
        logger = lambdaContext.getLogger();
        dao = GenericDAOFactory.getGenericDAO();
        
        DeviceGroup input = getGson().fromJson(getBodyFromRequest(request), DeviceGroup.class);

        List<DeviceGroup> dgs =  new ArrayList<DeviceGroup>();
        
        
        List<FilterCondition> queryFilters = new ArrayList<FilterCondition>();
    	
        if(!RequestValidator.isEmptyField(input.getDgID()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_ID, input.getDgID(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getDgName()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_NAME, input.getServiceName(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getDgType()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DG_TYPE, input.getDgType(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getParentDG()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PARENT_DG, input.getParentDG(),ComparisonOperator.EQ));
        
        if(!RequestValidator.isEmptyField(input.getNetworkID()))
    		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.NETWORK_ID, input.getNetworkID(),ComparisonOperator.EQ));
       
        
        try 
        {        	
        	User loggedUser = getUserInfoFromRequest(request);
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
            {
            	if(!RequestValidator.isEmptyField(input.getServiceName()))
            	{
            		if(loggedUser.getServiceName().equals(input.getServiceName()))
                		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
            		else
            			throw new BadRequestException("Not Authorized to view the Device Groups of service '"+input.getServiceName()+"' !");
            	}
            	else
            		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, loggedUser.getServiceName(),ComparisonOperator.EQ));
            	
            }
            else
            {
            	if(!RequestValidator.isEmptyField(input.getServiceName()))
            		queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(),ComparisonOperator.EQ));
                
            }
        	
        	dgs = dao.getGenericObjects(DeviceGroup.class, queryFilters);
        	
	        if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
	        {
	        	if(dgs.isEmpty())
	        		throw new InternalErrorException("Device Groups does not exist or Not mapped to Service "+loggedUser.getServiceName());
	        }
	        
        	if(dgs == null || dgs.size() == 0)
        		throw new DAOException("Device Groups does not Exist");
        } 
        catch (final Exception e) 
        {
            logger.log("Error while viewing Device Groups...\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(dgs, List.class);
    }
}


