package com.wio.core.config.action.device;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.LanSettings;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.helper.DynamoDBHelper;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class ModifyDevices extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
	
    
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        Device input = getGson().fromJson(getBodyFromRequest(request), Device.class);
        
        try {
        	DeviceValidator.validateOnModify(input);
        	
        	User loggedUser = getUserInfoFromRequest(request);  
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER)) {
        		if(!loggedUser.getServiceName().equals(input.getServiceName()))
        			throw new BadRequestException("Not authorized to modify the Device of other service!");
        	}
        	
        	/*List<FilterCondition> deviceFilters=new ArrayList<FilterCondition>();
             
         	deviceFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.SERVICE_NAME, input.getServiceName(), ComparisonOperator.EQ)); 
         	deviceFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_NAME, input.getDeviceName(), ComparisonOperator.EQ));
        	 */         	
         	Device device = DeviceValidator.getDeviceList().get(0);
         	
        	if (device == null ) 
                throw new BadRequestException("Device "+input.getDeviceName()+" does not exist");        
                  
            patchModifiedDeviceSettings(device, input);
            
            genDAO.updateGenericObject(device); 
            } 
        catch (final Exception e) 
        {
            logger.log("Error while adding AP Settings\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        Device output = new Device();
        
        output.setDeviceName(input.getDeviceName());
        
        return getGson().toJson(output, Device.class);
    }
    
    private DeviceConfiguration populateDeviceConfiguration(Device device, Device input) {
		DeviceConfiguration conf = new DeviceConfiguration();
		if(!device.getDefaultTopic().equals(input.getDefaultTopic()))
			conf.setDefaultTopic(device.getDefaultTopic());
		//For future use.
		//if(device.getDgID().equals(input.getDgID()))
		
		if(!device.getServiceName().equals(input.getServiceName()))
			conf.setServiceName(input.getServiceName());
		
		
		List<LanSettings> latestLanSettings = DynamoDBHelper.getInstance().getListItem(device.getLanSettings(),
				input.getLanSettings());
		if(latestLanSettings != null && !latestLanSettings.isEmpty())
			conf.setLanSettings(latestLanSettings);
	
		List<DeviceRadioSettings> latestRadioSettings = DynamoDBHelper.getInstance()
				.getListItem(device.getRadioSettings(), input.getRadioSettings());
		if(latestRadioSettings != null && !latestRadioSettings.isEmpty())
			conf.setRadioSettings(latestRadioSettings);
	
		if(!RequestValidator.isNullObject(device.getWanSettings()) && !RequestValidator.isNullObject(input.getWanSettings())) {
			
			if(!device.getWanSettings().equals(input.getWanSettings())) {
				conf.setWanSetting(input.getWanSettings());
			}
		}
		return conf;
	}
    
	private void patchModifiedDeviceSettings(Device device, Device input)
    {
    	
    	//if(input.getDeviceName() != null && input.getDeviceName().trim().length() > 0 )
    		//apSettings.setDeviceName(input.getDeviceName());
    	
    	if(input.getHpID() != null && input.getHpID().trim().length() >0)
    		device.setHpID(input.getHpID());
    	
    		//apSettings.setConnectedClients(input.getConnectedClients());
    	
    	/*if(input.getFirmware() != null && input.getFirmware().trim().length() > 0)
    		apSettings.setFirmware(input.getFirmware());*/
    	
    	/*if(input.getHncp() != null && input.getHncp().trim().length() > 0)
    		device.setHncp(input.getHncp());
    	
    	if(input.getDhcpServer() != null && input.getDhcpServer().trim().length() > 0 )
    		device.setDhcpServer(input.getDhcpServer());
    	*/
    	device.setLastUpdateTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
    		
    	//if(input.getMac() != null && input.getMac().trim().length() > 0)
    		//apSettings.setMac(input.getMac());
    		
   /* 	if(input.getMesh()!= null && input.getMesh().trim().length() > 0)
    		device.setMesh(input.getMesh());
    		*/
    	//if(input.getUuid()!= null && input.getUuid().trim().length() > 0)
    		//apSettings.setUuid(input.getUuid());
    		
    }
}
