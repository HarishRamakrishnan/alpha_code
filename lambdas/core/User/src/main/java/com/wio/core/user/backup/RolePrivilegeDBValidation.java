package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.generics.dao.FilterCondition;
import com.wavesio.server.common.generics.dao.GenericDAO;
import com.wavesio.server.common.generics.dao.GenericDAOFactory;
import com.wavesio.server.common.generics.dao.QueryHelper;
import com.wavesio.server.common.model.user.RolePrivilegeMap;
import com.wavesio.server.common.model.user.UserPrivilege;
import com.wavesio.server.common.model.user.Role;
import com.wavesio.server.common.staticinfo.SystemConfigMessages;
import com.wavesio.shared.common.Enums.DynamoDBObjectKeys;

public class RolePrivilegeDBValidation {

	private static AmazonDynamoDBClient ddbClient = new AmazonDynamoDBClient();	
	private static GenericDAO dao = GenericDAOFactory.getGenericDAO();
	
	static
	{
		ddbClient.setRegion(Region.getRegion(Regions.fromName(SystemConfigMessages.SYSTEM_AWS_REGION)));
    	ddbClient.setSignerRegionOverride(SystemConfigMessages.SYSTEM_AWS_REGION);
	}	
	
	public static boolean isPrivilegeExist(String privilege) throws DAOException
	{
		List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();    
	    filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PRIVILEGE, privilege, ComparisonOperator.EQ));        
		
	    if(!dao.getGenericObjects(UserPrivilege.class, filterConditions).isEmpty())
	    	return true;
	    
        return false;	
	}
	
	public static boolean isRoleExist(String role) throws DAOException
	{
		List<FilterCondition> roleFilter = new ArrayList<FilterCondition>();    
		roleFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, role, ComparisonOperator.EQ));        
		
		if(!dao.getGenericObjects(Role.class, roleFilter).isEmpty())
	    	return true;
	    
        return false;	
	}
	
	public static boolean isRoleAndPrivilegeMapped(String role, String privilege) throws DAOException
	{
		List<FilterCondition> mapFilter = new ArrayList<FilterCondition>();    
		mapFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.USER_ROLE, role, ComparisonOperator.EQ));        
		mapFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PRIVILEGE, privilege, ComparisonOperator.EQ));        
		
		if(!dao.getGenericObjects(RolePrivilegeMap.class, mapFilter).isEmpty())
	    	return true;
	    
        return false;
        
	}
	
	protected static DynamoDBMapper getMapper() {
        return new DynamoDBMapper(ddbClient);
    }
}
*/