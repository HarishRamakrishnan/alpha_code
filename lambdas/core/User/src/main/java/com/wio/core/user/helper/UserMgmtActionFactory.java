package com.wio.core.user.helper;

import com.wio.common.action.IRequestAction;
import com.wio.common.helper.ActionMapper;
import com.wio.core.user.action.rights.PrivilegedActionAdd;
import com.wio.core.user.action.rights.PrivilegedActionDelete;
import com.wio.core.user.action.rights.PrivilegedActionView;
import com.wio.core.user.action.rights.RoleAdd;
import com.wio.core.user.action.rights.RoleDelete;
import com.wio.core.user.action.rights.RolePrivilegeAssign;
import com.wio.core.user.action.rights.RolePrivilegeUnAssign;
import com.wio.core.user.action.rights.RolePrivilegeView;
import com.wio.core.user.action.rights.RoleView;
import com.wio.core.user.action.user.AddUserAction;
import com.wio.core.user.action.user.DeleteUserAction;
import com.wio.core.user.action.user.ModifyUserAction;
import com.wio.core.user.action.user.ViewUsersAction;

public class UserMgmtActionFactory {

	
	/**
     * Default constructor
     */
    private UserMgmtActionFactory() {}
    
    public static IRequestAction getUserActionInstance(String action){
    	
    	switch(action)
    	{
    		case ActionMapper.USER_MGMT_ADD_USER:
    			return new AddUserAction();
    		case ActionMapper.USER_MGMT_MODIFY_USER:
    			return new ModifyUserAction();
    		case ActionMapper.USER_MGMT_DELETE_USER:
    			return new DeleteUserAction();
    		case ActionMapper.USER_MGMT_VIEW_USER:
    			return new ViewUsersAction();
    			
    		case ActionMapper.USER_MGMT_ADD_ROLE:
    			return new RoleAdd();
    		case ActionMapper.USER_MGMT_DELETE_ROLE:
    			return new RoleDelete();
    		case ActionMapper.USER_MGMT_VIEW_ROLE:
    			return new RoleView();
    			
    		case ActionMapper.USER_MGMT_ADD_PRIVILEGED_ACTION:
    			return new PrivilegedActionAdd();
    		case ActionMapper.USER_MGMT_DELETE_PRIVILEGED_ACTION:
    			return new PrivilegedActionDelete();
    		case ActionMapper.USER_MGMT_VIEW_PRIVILEGED_ACTION:
    			return new PrivilegedActionView();
    			
    		case ActionMapper.USER_MGMT_ADD_ROLE_PRIVILEGE:
    			return new RolePrivilegeAssign();
    		case ActionMapper.USER_MGMT_DELETE_ROLE_PRIVILEGE:
    			return new RolePrivilegeUnAssign();
    		case ActionMapper.USER_MGMT_VIEW_ROLE_PRIVILEGE:
    			return new RolePrivilegeView();
    			
    			
    			
    		/*case ActionMapper.USER_MGMT_ASSIGN_PRIVILEGE:
    			return new AssignPrivilegeAction();
    		case ActionMapper.USER_MGMT_UNASSIGN_PRIVILEGE:
    			return new UnAssignPrivilegeAction();
    		case ActionMapper.USER_MGMT_VIEW_ROLE_PRIVILEGE:
    			return new ViewPrivilegeMap();
    			
    		case ActionMapper.USER_MGMT_ADD_PRIVILEGE:
    			return new AddPrivilegeAction();
    		case ActionMapper.USER_MGMT_DELETE_PRIVILEGE:
    			return new DeletePrivilegeAction();
    		case ActionMapper.USER_MGMT_VIEW_PRIVILEGE:
    			return new ViewPrivilege();
    			    		
    		case ActionMapper.USER_MGMT_VIEW_ACTION:
    			return new ViewUserAction();*/
    			
    		/*case ActionMapper.USER_MGMT_MAP_ACTION_PRIVILEGE:
    			return new MapActionPrivilege();    			
    		case ActionMapper.USER_MGMT_UNMAP_ACTION_PRIVILEGE:
    			return new UnMapActionPrivilege();
    		case ActionMapper.USER_MGMT_VIEW_ACTION_PRIVILEGE:
    			return new ViewActionPrivilege();*/
    			
    		default:
    			return null;	
    			
    	}
    	
    }


    
}
