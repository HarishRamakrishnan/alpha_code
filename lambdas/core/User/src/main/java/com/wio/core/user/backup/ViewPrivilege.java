package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.google.gson.JsonObject;
import com.wavesio.server.common.action.AbstractAction;
import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.exception.InternalErrorException;
import com.wavesio.server.common.generics.dao.GenericDAO;
import com.wavesio.server.common.generics.dao.GenericDAOFactory;
import com.wavesio.server.common.model.user.UserPrivilege;

public class ViewPrivilege extends AbstractAction
{
	//private LambdaLogger logger = null;
	private GenericDAO genDAO = null;
	
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	{
        //logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        //JsonObject body = getBodyFromRequest(request);
	
		List<UserPrivilege> privilegeDetails = new ArrayList<UserPrivilege>(); 
		
		try
		{
			privilegeDetails = genDAO.getGenericObjects(UserPrivilege.class, null);
			
			if(privilegeDetails == null)
				throw new InternalErrorException("Privilege is not available");				
		}
		catch (DAOException e) 
    	{
			e.printStackTrace();
		}
		return getGson().toJson(privilegeDetails, List.class);
			
	 }
		
}
*/