package com.wio.core.user.backup;
/*package com.wavesio.server.user.backup;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wavesio.server.common.action.AbstractAction;
import com.wavesio.server.common.exception.BadRequestException;
import com.wavesio.server.common.exception.DAOException;
import com.wavesio.server.common.exception.InternalErrorException;
import com.wavesio.server.common.generics.dao.GenericDAO;
import com.wavesio.server.common.generics.dao.GenericDAOFactory;
import com.wavesio.server.common.model.user.ActionPrivilegeMap;


*//**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 *//*
public class MapActionPrivilege extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    *//**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     *//*
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();        
        
        JsonObject body = getBodyFromRequest(request);
		
        ActionPrivilegeMap input = getGson().fromJson(body, ActionPrivilegeMap.class);
             
        try 
        {
        	ValidateMapping.validateMapping(input, logger);  
        	genDAO.saveGenericObject(input);
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while mapping Action-Privilege \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(input, ActionPrivilegeMap.class);
    }
}
*/