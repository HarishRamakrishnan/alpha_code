package com.wio.core.user.action.user;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.RequestValidator;

public class DeleteUserAction extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the registration action. It expcts a RegisterUserRequest object in input and returns
     * a serialized RegisterUserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws AuthorizationException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, AuthorizationException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        JsonObject body = getBodyFromRequest(request);
        
        User input = getGson().fromJson(body, User.class);
        
        if(RequestValidator.isEmptyField(input.getUserID()))
        {
        	logger.log("username cannot be empty, Exception at "+this.getClass().getName());
        	throw new BadRequestException("username "+ExceptionMessages.EX_EMPTY_FIELD);
        }
    
        User user = null;
        try 
        {
        	user = genDAO.getGenericObject(User.class, input.getUserID());
        	
        	if(null == user)
        		throw new DAOException("User does not exist !");
        	
        	User loggedUser=getUserInfoFromRequest(request);
        	
        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
        	{
        		if(loggedUser.getServiceName().equals(user.getServiceName()))
        		{
        			if(user.getUserRole().equals(UserRoleType.USER_ROLE_NETWORK_USER))
        				genDAO.deleteGenericObject(user);
        			else
        				throw new AuthorizationException("Unauthorized to delete user under role "+user.getUserRole());
        		}
        		else
        			throw new AuthorizationException("Unauthorized to delete user from other service!");        			
        	}
        	else
        	{
	        	if(UserRoleType.USER_ROLE_WAVES_ADMIN.equals(user.getUserRole()))
	        	{
		        	logger.log("Administrator user can not be deleted.");
		        	throw new InternalErrorException("Administrator user can not be deleted.");        		
	        	}
	        		        	
	        	genDAO.deleteGenericObject(user);
        	}
          
        } 
        catch ( DAOException e) {
            logger.log("Error while deleting user\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        OutputMessage output = new OutputMessage();
        output.setStatus(200);
        output.setMessage("User: "+user.getUserID()+" Deleted from WavesIO !");        
        
        return getGson().toJson(output, OutputMessage.class);        
    }
}
