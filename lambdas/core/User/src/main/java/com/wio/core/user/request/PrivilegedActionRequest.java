package com.wio.core.user.request;

import java.util.List;

import com.wio.common.user.model.PrivilegedAction;

public class PrivilegedActionRequest 
{
	private String securityToken;
	private List<PrivilegedAction> privilegedActions;
	
	
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	public List<PrivilegedAction> getPrivilegedActions() {
		return privilegedActions;
	}
	public void setPrivilegedActions(List<PrivilegedAction> privilegedActions) {
		this.privilegedActions = privilegedActions;
	}
	
}
