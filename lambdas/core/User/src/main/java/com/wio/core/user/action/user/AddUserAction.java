package com.wio.core.user.action.user;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.UserRoleType;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.AuthorizationException;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.helper.ActionMapper;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;
import com.wio.core.user.helper.PasswordHelper;
import com.wio.core.user.request.UserObject;
import com.wio.core.user.request.UserRequest;


/**
 * Action used to register a new user.
 * <p/>
 * POST to /users/
 */
public class AddUserAction extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the add user action. It expects a UserRequest object in input and returns
     * a serialized UserResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a RegisterUserResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, AuthorizationException, DAOException 
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();
        
        JsonObject body = getBodyFromRequest(request);
        User loggedUser = getUserInfoFromRequest(request); 
        
        UserRequest usersList = getGson().fromJson(body, UserRequest.class);      
                
        List<User> newUsers = new ArrayList<User>();
        
        try 
        {
        	if(usersList.getUsers().isEmpty() || usersList.getUsers() == null)
        	{
        		throw new BadRequestException("At least one user details should be added !");
        	}
        	
	        for(UserObject user: usersList.getUsers())
	        {
	        	UserValidator.validateOnAdd(user);
	        	
	        	if(UserRoleType.USER_ROLE_GLOBAL_USER.equals(user.getUserRole()))
	        		user.setServiceName("global");
	        	
	        	if(loggedUser.getUserRole().equals(UserRoleType.USER_ROLE_SERVICE_USER))
	        	{
	        		if(RequestValidator.isEmptyField(user.getServiceName()))
	        			user.setServiceName(loggedUser.getServiceName());
	        		else
	        		{
	        			if(!user.getServiceName().equals(loggedUser.getServiceName()))
	        				throw new BadRequestException("Not authoried to create user under service: "+user.getServiceName());
	        		}
	        	}
	        	
	        	
	        	String userID = ActionMapper.createUserId(user.getServiceName(), user.getUsername());
	        	
	        	if(DataValidator.isUserExist(userID))
	        		throw new DAOException("User : "+userID+" already exists in waves.io !");
	        	
	        	User newUser = new User();
	        	
	        	newUser.setUserID(userID);
	        	newUser.setUsername(user.getUsername());
	        	
	        	//password encryption	        	
	        	byte[] salt = PasswordHelper.generateSalt();
	            byte[] encryptedPassword = PasswordHelper.getEncryptedPassword(user.getPassword().toString(), salt);
	            
	            newUser.setPassword(ByteBuffer.wrap(encryptedPassword));
	            newUser.setSalt(ByteBuffer.wrap(salt));
	            
	            if (newUser.getPassword() == null) 
	            {
	                logger.log("Password null after encryption");
	                throw new InternalErrorException(ExceptionMessages.EX_PWD_SAVE);
	            }
	            	 
	            newUser.setServiceName(user.getServiceName());	            	           
	            newUser.setFirstName(user.getFirstName());
	            newUser.setLastName(user.getLastName());
	            newUser.setPhoneNumber(user.getPhoneNumber());
	            newUser.setEmail(user.getEmail());	            
	            newUser.setUserRole(user.getUserRole());	            
	            
	            newUser.setAccountStatus("Active");
	            
	            if(user.isNeedPasswordChange())
	            	newUser.setNeedPasswordChange(true);
	            else
	            	newUser.setNeedPasswordChange(false);
	            	
	            newUser.setMaxLoginAttempts(3);
	            newUser.setFailedLoginAttempts(0);
	            	            
	            newUser.setCreatedBy(loggedUser.getUserID());
	            newUser.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
	            newUser.setLastUpdatedTime("NA");
	            
	            newUsers.add(newUser);
	        }            
	        
	        genDAO.saveGenericObjects(newUsers);
        } 
        catch (final NoSuchAlgorithmException e) 
        {
            logger.log("No algrithm found for password encryption\n" + e.getMessage());
            throw new InternalErrorException(ExceptionMessages.EX_PWD_SALT);
        } 
        catch (final InvalidKeySpecException e) 
        {
            logger.log("No KeySpec found for password encryption\n" + e.getMessage());
            throw new InternalErrorException(ExceptionMessages.EX_PWD_ENCRYPT);
        }
        catch (final DAOException e) 
        {
            logger.log("Error while saving new user\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        } 

        List<User> output = new ArrayList<User>();
        
        for(User newUser : newUsers)
        {
        	User user = new User();
        	
        	user.setUserID(newUser.getUserID());
        	
        	output.add(user);
        }
        
        return getGson().toJson(output, List.class);
    }
}
