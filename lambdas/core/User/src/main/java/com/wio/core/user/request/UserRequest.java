package com.wio.core.user.request;

import java.util.List;


/**
 * Bean for the user registration request.
 */
public class UserRequest 
{
	private String securityToken;
	private List<UserObject> users;
	
	
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	
	public List<UserObject> getUsers() {
		return users;
	}
	public void setUsers(List<UserObject> users) {
		this.users = users;
	}
	
	
}
