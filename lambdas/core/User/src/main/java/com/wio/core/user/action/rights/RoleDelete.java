/**
 * 
 */
package com.wio.core.user.action.rights;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.staticinfo.ExceptionMessages;
import com.wio.common.user.model.Role;
import com.wio.common.validation.DataValidator;
import com.wio.common.validation.RequestValidator;

/**
 * @author mtambour
 *
 */
public class RoleDelete extends AbstractAction 
{
	private LambdaLogger logger = null;
	private GenericDAO genDAO = null;

	/**
	 * Handler implementation for the delete userRole action. It expects a request
	 * object in input and returns a serialized response object
	 *
	 * @param request
	 *            Receives a JsonObject containing the body content
	 * @param lambdaContext
	 *            The Lambda context passed by the AWS Lambda environment
	 * @return Returns the userRole and a set of temporary AWS credentials as a
	 *         RegisterUserResponse object
	 * @throws BadRequestException
	 * @throws InternalErrorException
	 * @throws DAOException 
	 */
	public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException, DAOException 
	{
		logger = lambdaContext.getLogger();
		genDAO = GenericDAOFactory.getGenericDAO();
		
		JsonObject body = getBodyFromRequest(request);

		Role input = getGson().fromJson(body, Role.class);		
		
		try 
		{
			validate(input);
			genDAO.deleteGenericObject(input);
		} 
		catch (final DAOException e) 
		{
			logger.log("Error while deleting userRole\n" + e.getMessage());
			throw new InternalErrorException(e.getMessage());
		}

		return getGson().toJson(input, Role.class);
	}
	
	public void validate(Role input) throws BadRequestException, DAOException
	{
		if(RequestValidator.isEmptyField(input.getUserRole()))
        {
        	throw new BadRequestException("Role "+ExceptionMessages.EX_EMPTY_FIELD);
        }
		
		if(!DataValidator.isRoleExist(input.getUserRole()))
			throw new DAOException("Role does not exist, invalid role !");

		if(DataValidator.isRoleDependent(input.getUserRole()))
			throw new DAOException("Cannot delete the role, dependent with other objects !");
	}
}