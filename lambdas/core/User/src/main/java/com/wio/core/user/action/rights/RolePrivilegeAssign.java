package com.wio.core.user.action.rights;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.user.model.RolePrivilege;
import com.wio.common.validation.RequestValidator;
import com.wio.core.user.request.RolePrivilegeRequest;


/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class RolePrivilegeAssign extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();        
        
        JsonObject body = getBodyFromRequest(request);
		
        RolePrivilegeRequest input = getGson().fromJson(body, RolePrivilegeRequest.class);
             
        try 
        {
        	validate(input);        	
        	genDAO.saveGenericObjects(input.getRolePrivileges());
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while adding Role-Privilege \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(input.getRolePrivileges(), List.class);
    }
    
    public void validate(RolePrivilegeRequest input) throws DAOException, BadRequestException
    {	
    	List<RolePrivilege> rolePrivileges = genDAO.getGenericObjects(RolePrivilege.class, null);
    	
    	for(RolePrivilege rp : input.getRolePrivileges())
    	{   
        	if(RequestValidator.isEmptyField(rp.getPrivilege()))
        		throw new BadRequestException("Privilege cannot be empty !");
        	
        	if(RequestValidator.isEmptyField(rp.getRole()))
        		throw new BadRequestException("Role cannot be empty !");
        		
        	if(rolePrivileges.contains(rp))        		
        		throw new DAOException("Role-Privilege "+rp.getPrivilege()+" already mapped !");
    	}
    }
}
