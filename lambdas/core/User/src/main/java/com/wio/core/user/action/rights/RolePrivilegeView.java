package com.wio.core.user.action.rights;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.user.model.RolePrivilege;
import com.wio.common.validation.RequestValidator;


/**
 * Action used to register a new AP.
 * <p/>
 * POST to /ap/
 */
public class RolePrivilegeView extends AbstractAction 
{
    private LambdaLogger logger = null;
    private GenericDAO genDAO = null;
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new user identifier and a set of temporary AWS credentials as a AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     * @throws DAOException 
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException
    {
        logger = lambdaContext.getLogger();
        genDAO = GenericDAOFactory.getGenericDAO();        
        
        JsonObject body = getBodyFromRequest(request);
		
        RolePrivilege input = getGson().fromJson(body, RolePrivilege.class);
        
        List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
        
    	if(!RequestValidator.isEmptyField(input.getPrivilege()))
    		filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.PRIVILEGE, input.getPrivilege(), ComparisonOperator.EQ));
    	
    	if(!RequestValidator.isEmptyField(input.getRole()))
    		filterConditions.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.ROLE, input.getRole(), ComparisonOperator.EQ));
    	
    	List<RolePrivilege> rp = null;
        try 
        {
        	rp = genDAO.getGenericObjects(RolePrivilege.class, filterConditions);
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while adding Role-Privilege \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
        
        return getGson().toJson(rp, List.class);
    }
}
