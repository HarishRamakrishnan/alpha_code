package com.wio.core.firmware.helper;

import com.wio.common.action.IRequestAction;
import com.wio.common.helper.ActionMapper;
import com.wio.core.firmware.action.FirmwareUploadAction;
import com.wio.core.firmware.action.ViewFirmwareAction;

public class FirmwareUpgradeActionFactory {

	
	/**
     * Default constructor
     */
    private FirmwareUpgradeActionFactory() {}
    
    public static IRequestAction getUserActionInstance(String action){
    	
    	switch(action)
    	{
    		case ActionMapper.FIRMWARE_UPLOAD:
    			return new FirmwareUploadAction();
    			
    		case ActionMapper.FIRMWARE_VIEW:
    			return new ViewFirmwareAction();
    			
    		default:
    			return null;	
    			
    	}
    	
    }


    
}
