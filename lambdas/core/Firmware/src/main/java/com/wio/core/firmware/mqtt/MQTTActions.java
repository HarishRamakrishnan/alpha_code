package com.wio.core.firmware.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.exception.DAOException;
import com.wio.common.firmware.Firmware;
import com.wio.common.mqtt.MQTTConnection;
import com.wio.common.mqtt.MQTTPublisher;

/**
 * The table in DynamoDB should be created with an Hash Key called deviceId.
 */
public class MQTTActions 
{	
	public static String notifyDevice(String tranID, Firmware latestfirmware, Context lambdaContext) throws DAOException
	{
		LambdaLogger logger = lambdaContext.getLogger();	 
		String status = "FAILURE";
		
		try
		{
			MQTTConnection conn = new MQTTConnection();
            MqttClient connection = conn.getConnection(System.getenv("MQTT_CLIENT_ID"), 0, false, false);
			
            status = MQTTPublisher.publishMessage(connection, prepareMessage(latestfirmware, tranID), 
            		latestfirmware.getHpTopic(), Integer.parseInt(System.getenv("MQTT_TOPIC_DEFAULT_QOS")), false);
            
            if(status.equals("FAILURE"))
				logger.log("Unalbe to send firmware notification to devices under HP: "+latestfirmware.getHpID()+", MQTT Broker issue !");
						
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}				
		return status;
	}
	
	public static String prepareMessage(Firmware latestfirmware, String tranID)
	{
		JsonObject messageContent = new JsonObject();
		
		messageContent.addProperty("transid", tranID);
		messageContent.addProperty("fwurl", latestfirmware.getEdgeURL());
		
		return messageContent.toString();
	}
		
}
