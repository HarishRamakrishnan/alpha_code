package com.wio.core.firmware.action;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.firmware.Firmware;
import com.wio.common.firmware.FirmwareUpdates;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.user.model.User;
import com.wio.common.util.UniqueIDGenerator;
import com.wio.core.firmware.mqtt.MQTTActions;
import com.wio.core.firmware.request.firmware.FirmwareUploadS3Response;
import com.wio.core.firmware.s3.WavesioS3Bucket;

public class FirmwareUploadAction extends AbstractAction
{	
	  private LambdaLogger logger = null;
	  private GenericDAO genDAO = null;
	  private static final String CF_EDGE_URL = "https://"+System.getenv("CF_EDGE_URL")+"/";

	    /**
	     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
	     * a serialized APResponse object
	     *
	     * @param request       Receives a JsonObject containing the body content
	     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
	     * @return Returns the new AddAPResponse object
	     * @throws BadRequestException
	     * @throws InternalErrorException
	     */
	    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
	    {
	    	System.out.println("In firmware upload action");
	    	logger = lambdaContext.getLogger();	        
	    	genDAO = GenericDAOFactory.getGenericDAO();
	    	
	    	OutputMessage output = new OutputMessage();
	    	
	        Firmware input = getGson().fromJson(getBodyFromRequest(request), Firmware.class);	        
	        ValidateFirmware.validate(input);
	        
	        try 
	        {
	        	User loggedUser = getUserInfoFromRequest(request); 
	        	
	        	//check HP is exist or no
	        	HardwareProfile hwp = new HardwareProfile();
	        	hwp.setHpID(input.getHpID());
	        	
	        	HardwareProfile hp = genDAO.getGenericObject(HardwareProfile.class, hwp.getHpID());
	        	
	        	if(null == hp)
	        	{
	        		output.setStatus(HttpURLConnection.HTTP_BAD_REQUEST);
	        		output.setErrorMessage("Invalid Hardware Profile");
	            	
	            	return getGson().toJson(output, OutputMessage.class);  
	        	}	        	
	        	
	        	//Check Firmware exist or not !
	        	String filename = FilenameUtils.getName(new URL(input.getSource()).getPath());
	        	
	        	List<FilterCondition> filters=new ArrayList<FilterCondition>();
	        	
		        filters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_FILE_NAME, filename , ComparisonOperator.EQ));
		        filters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_VERSION, input.getVersion(),ComparisonOperator.EQ));
		        filters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, input.getHpID(),ComparisonOperator.EQ));
		        filters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.FW_VERSION_TYPE, input.getVersionType(),ComparisonOperator.EQ));
	        	
		        List<Firmware> firmwareRespose = genDAO.getGenericObjects(Firmware.class, filters);
		        
	        	if(!firmwareRespose.isEmpty())
	        	{
	        		output.setStatus(HttpURLConnection.HTTP_BAD_REQUEST);
	        		output.setErrorMessage("Firmware "+filename+" already exist in WIO for the same HP !");
	            	
	            	return getGson().toJson(output, OutputMessage.class);  
	        	}
	        	
	        	//upload the firmware to S3 bucket
	        	FirmwareUploadS3Response s3Response = WavesioS3Bucket.uploadFirmware(input);
	        		
	        	logger.log("Firmware uploded to S3...");
	        	
	        	input.setFwID("FW"+UniqueIDGenerator.getID());
	        	input.setFilename(s3Response.getFileName());
	        	input.setHpTopic(hp.getHpTopic());
	        	input.setS3BucketName(s3Response.getBucketName());
	        	input.setS3KeyName(s3Response.getKeyName());
	        	input.setS3URL(s3Response.getS3URL());
	        	input.setEdgeURL(CF_EDGE_URL+s3Response.getFileName());
	        	input.setUploadDate(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
	        	input.setStatus("UPLOADED");
	        	
	        	input.setUploadedBy(loggedUser.getUserRole());
	            
	        	
	        	//store the firmware information to DyanamoDB	  
	        	logger.log("Storing firmware info to DynamoDB...");
	        	genDAO.saveGenericObject(input); 	
	        	
	        	
	        	//Store firmware update transaction per device to DynamoDB	        	
	        	List<FirmwareUpdates> fwTransactions = new ArrayList<>();
	        	
	        	List<FilterCondition> devFilters=new ArrayList<FilterCondition>();	        	
	        	devFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.HP_ID, hwp.getHpID() , ComparisonOperator.EQ));
		        
	        	List<Device> devices = genDAO.getGenericObjects(Device.class, devFilters);
	        	
	        	if(devices.isEmpty())
	        		logger.log("No devices found !");
	        	else
	        		logger.log(devices.size()+" devices found !");
	        	
	        	String tranId = UniqueIDGenerator.getID();
	        	
	        	devices.forEach(device ->
	        	{
	        		logger.log("Iterating for "+device.getDeviceID());
		        	FirmwareUpdates fwTransaction = new FirmwareUpdates();
		        	
		        	fwTransaction.setTransactionID(tranId);
	        		fwTransaction.setDeviceID(device.getDeviceID());
	        		fwTransaction.setFwID(input.getFwID());
	        		fwTransaction.setHpID(input.getHpID());
	        		fwTransaction.setStatus("FW_UPLOADED");
	        		fwTransaction.setLastUpdateTime("NA");
	        		fwTransaction.setCreationTime(new SimpleDateFormat(SystemConfigMessages.SYSTEM_DATE_FORMAT).format(new Date()));
		        	
		        	fwTransactions.add(fwTransaction);
	        	});
	        	
	        	logger.log("saving all transactions...");
	        	genDAO.saveGenericObjects(fwTransactions);
	        	
	        	
	        	//Firmware upgrade notification to Device
	        	logger.log("Sending notification to MQTT subscribers...");
	        	MQTTActions.notifyDevice(tranId, input, lambdaContext);	           
	        } 
	        catch (final Exception e) 
	        {
	            logger.log("Error while saving firmware details.....\n" + e.getMessage());
	            throw new InternalErrorException(e.getMessage());
	        } 

	        Firmware res = new Firmware();
	        
	        res.setFilename(input.getFilename());
	        res.setVersion(input.getVersion());
	        res.setEdgeURL(input.getEdgeURL());

	        return getGson().toJson(res, Firmware.class);
	    }
}

