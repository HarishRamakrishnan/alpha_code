package com.wio.core.system.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.AUTO_TEST_RESULTS)
public class TestResults 
{
	private String testID;
	private String suiteID;
	private String lambda;
	private String module;
	private String action;
	private String scenario;
	private String description;
	private TRInput input; 
	private TROutput output;
	private String result;
	private int serial;
	private String timestamp;
	
	
	public TestResults() {

    }


	@DynamoDBHashKey(attributeName = "TEST_ID")
	public String getTestID() {
		return testID;
	}
	public void setTestID(String testID) {
		this.testID = testID;
	}

	@DynamoDBAttribute(attributeName = "SUITE_ID")
	public String getSuiteID() {
		return suiteID;
	}
	public void setSuiteID(String suiteID) {
		this.suiteID = suiteID;
	}

	@DynamoDBAttribute(attributeName = "LAMBDA")
	public String getLambda() {
		return lambda;
	}
	public void setLambda(String lambda) {
		this.lambda = lambda;
	}

	@DynamoDBAttribute(attributeName = "MODULE")
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}

	@DynamoDBAttribute(attributeName = "ACTION")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	@DynamoDBAttribute(attributeName = "SCENARIO")
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@DynamoDBAttribute(attributeName = "INPUT")
	public TRInput getInput() {
		return input;
	}
	public void setInput(TRInput input) {
		this.input = input;
	}

	@DynamoDBAttribute(attributeName = "OUTPUT")
	public TROutput getOutput() {
		return output;
	}
	public void setOutput(TROutput output) {
		this.output = output;
	}

	@DynamoDBAttribute(attributeName = "RESULT")
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

	@DynamoDBAttribute(attributeName = "SERIAL")
	public int getSerial() {
		return serial;
	}
	public void setSerial(int serial) {
		this.serial = serial;
	}

	@DynamoDBAttribute(attributeName = "TIME_STAMP")
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
