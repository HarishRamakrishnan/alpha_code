package com.wio.core.system.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.AUTO_TEST_RUN)
public class TestRun 
{
	private String runID;
	private String endTime;
	private String startTime;
	
	
	public TestRun() {

    }
	
	
	@DynamoDBHashKey(attributeName = "RUN_ID")
	public String getRunID() {
		return runID;
	}
	public void setRunID(String runID) {
		this.runID = runID;
	}
	
	
	@DynamoDBAttribute(attributeName = "END_TIME")
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
	@DynamoDBAttribute(attributeName = "START_TIME")
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

}
