package com.wio.core.system.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;


@DynamoDBTable(tableName = DynamoDBObject.AUTO_TEST_SUITE)
public class TestSuite 
{
	private String suiteID;
	private String runID;
	private String verdict;
	private String startTime;
	private String endTime;
	
	
	public TestSuite() {

    }
	
	@DynamoDBHashKey(attributeName = "SUITE_ID")
	public String getSuiteID() {
		return suiteID;
	}
	public void setSuiteID(String suiteID) {
		this.suiteID = suiteID;
	}
	
	
	@DynamoDBAttribute(attributeName = "RUN_ID")
	public String getRunID() {
		return runID;
	}
	public void setRunID(String runID) {
		this.runID = runID;
	}
	
	
	@DynamoDBAttribute(attributeName = "START_TIME")
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	
	@DynamoDBAttribute(attributeName = "END_TIME")
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
		
	@DynamoDBAttribute(attributeName = "VERDICT")
	public String getVerdict() {
		return verdict;
	}
	public void setVerdict(String verdict) {
		this.verdict = verdict;
	}
	
	

}
