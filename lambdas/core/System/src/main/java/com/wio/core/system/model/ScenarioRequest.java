package com.wio.core.system.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.google.gson.JsonObject;

@DynamoDBDocument
public class ScenarioRequest 
{
	private String scenario;
	private String uri;
	private String method;
	private String timeout;
	private JsonObject input;
	private JsonObject output;
	
	@DynamoDBHashKey(attributeName = "SCENARIO")	
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	
	@DynamoDBAttribute(attributeName = "URI")
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	@DynamoDBAttribute(attributeName = "METHOD")
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
	@DynamoDBAttribute(attributeName = "TIMEOUT")
	public String getTimeout() {
		return timeout;
	}
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}
	
	@DynamoDBAttribute(attributeName = "INPUT")
	public JsonObject getInput() {
		return input;
	}
	public void setInput(JsonObject input) {
		this.input = input;
	}
	
	@DynamoDBAttribute(attributeName = "OUTPUT")
	public JsonObject getOutput() {
		return output;
	}
	public void setOutput(JsonObject output) {
		this.output = output;
	}

}
