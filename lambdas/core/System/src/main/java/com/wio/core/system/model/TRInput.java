package com.wio.core.system.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

@DynamoDBDocument
public class TRInput 
{
	private String uri;
	private String request;
	private String timeout;
	private String method;	
	
	
	public TRInput() {

    }


	@DynamoDBHashKey(attributeName = "URL")
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	@DynamoDBAttribute(attributeName = "REQUEST")
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}

	@DynamoDBAttribute(attributeName = "TIMEOUT")
	public String getTimeout() {
		return timeout;
	}
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	@DynamoDBAttribute(attributeName = "METHOD")
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
	

}
