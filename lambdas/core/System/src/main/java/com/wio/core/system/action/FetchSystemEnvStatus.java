package com.wio.core.system.action;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.JsonObject;
import com.wio.common.Enums.DynamoDBObjectKeys;
import com.wio.common.action.AbstractAction;
import com.wio.common.exception.BadRequestException;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InternalErrorException;
import com.wio.common.exception.OutputMessage;
import com.wio.common.generic.dao.FilterCondition;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.generic.dao.QueryHelper;
import com.wio.core.system.model.TestResults;
import com.wio.core.system.model.TestRun;
import com.wio.core.system.model.TestSuite;

/**
 * Action used to zero provisioning a new AP.
 * <p/>
 * POST to /ap/
 */
public class FetchSystemEnvStatus extends AbstractAction 
{
    /**
     * Handler implementation for the add ap action. It expects a AddAPRequest object in input and returns
     * a serialized APResponse object
     *
     * @param request       Receives a JsonObject containing the body content
     * @param lambdaContext The Lambda context passed by the AWS Lambda environment
     * @return Returns the new AddAPResponse object
     * @throws BadRequestException
     * @throws InternalErrorException
     */
    public String handle(JsonObject request, Context lambdaContext) throws BadRequestException, InternalErrorException 
    {
    	LambdaLogger logger = lambdaContext.getLogger();
        GenericDAO dao = GenericDAOFactory.getGenericDAO();  
    	
        List<TestRun> run = new ArrayList<>();
        List<TestSuite> suites = new ArrayList<>();
    	List<TestResults> testResults = new ArrayList<>();
	    
        try 
        {
        	if(!System.getenv("REGION").equals("eu-west-1"))
        	{
        		OutputMessage response = new OutputMessage();
            	
            	response.setStatus(200);
            	response.setErrorMessage("Please try in Test region (Irland: eu-west-1) !");
            	
            	return getGson().toJson(response, OutputMessage.class);
        	}
        	
        	//Fetching TEST_RUN data
        	List<FilterCondition> runFilter=new ArrayList<FilterCondition>();        	
        	runFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.TEST_LATEST, "TRUE", ComparisonOperator.EQ));        	
        	
        	run = dao.getGenericObjects(TestRun.class, runFilter);
        	
        	if(!run.isEmpty())
        	{
        		if(run.size() > 1)
        		{
        			OutputMessage response = new OutputMessage();
                	
                	response.setStatus(200);
                	response.setErrorMessage("More than 1 Test Run Data cannot be proccessed !");
                	
                	return getGson().toJson(response, OutputMessage.class);
        		}
        	}
        	else
        	{
        		OutputMessage response = new OutputMessage();
            	
            	response.setStatus(200);
            	response.setErrorMessage("No Test Run Data Found !");
            	
            	return getGson().toJson(response, OutputMessage.class);
        	}
        		
        	
        	//Fetching TEST_SUITE data
        	List<FilterCondition> suiteFilter=new ArrayList<FilterCondition>();        	
        	suiteFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.TEST_RUN_ID, run.get(0).getRunID(), ComparisonOperator.EQ));        	
        	
        	suites = dao.getGenericObjects(TestSuite.class, suiteFilter);
        	
        	if(!suites.isEmpty())
        	{
        		List<TestResults> results = new ArrayList<>();
        		suites.forEach(suite ->
        		{        			
        			List<FilterCondition> resultFilter=new ArrayList<FilterCondition>();        	
        			resultFilter.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.TEST_SUITE_ID, suite.getSuiteID(), ComparisonOperator.EQ));        	
                	
        			try 
        			{
        				results.addAll(dao.getGenericObjects(TestResults.class, resultFilter));
					} 
        			catch (DAOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                	
        		});
        		
        		testResults.addAll(results);
        	}
        	else
        	{
        		OutputMessage response = new OutputMessage();
            	
            	response.setStatus(200);
            	response.setErrorMessage("No Test Suites found for Run ID: "+run.get(0).getRunID());
            	
            	return getGson().toJson(response, OutputMessage.class);
        	}
        	
        	
        	 
        	/*List<String> startTime = new ArrayList<>();
        	
        	run.forEach(item->{
        		startTime.add(item.getStartTime());
        	});      	
        	
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss a");
            Collections.sort(startTime, (s1, s2) -> LocalDateTime.parse(s1, formatter).compareTo(LocalDateTime.parse(s2, formatter)));
        	
            startTime.get(startTime.size()-1);*/
        	
        	
        /*	
        	List<FilterCondition> queryFilters=new ArrayList<FilterCondition>();
        	
        	queryFilters.add(QueryHelper.CreateQueryFilter(DynamoDBObjectKeys.DEVICE_ID, input.getDeviceID(),ComparisonOperator.EQ));
        	
        	//Fetching Test Results...
        	
        	run = dao.getGenericObjects(TestRun.class, null)*/
        	
        	
        	/*testResults = dao.getGenericObjects(TestResults.class, null);*/
        	
            if(!testResults.isEmpty())
            { 
            	System.out.println("Result:: "+getGson().toJson(testResults, List.class));
                return getGson().toJson(testResults, List.class);            	
            }
            else
            {
            	OutputMessage response = new OutputMessage();
            	
            	response.setStatus(200);
            	response.setErrorMessage("No Test Result Found !");
            	
            	return getGson().toJson(response, OutputMessage.class);
            }
        } 
        catch (final DAOException e) 
        {
            logger.log("Error while fetching test results \n" + e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }         
    }
}
