package com.wio.core.system.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.AUTOMATION_TEST_RESULTS)
public class AutomationTestResults 
{
	private Integer testID;
	private String uuid;
	private String lambda;
	private String module;
	private String action;
	private String scenario;
	private String description;
	private ScenarioRequest request ;
	private String result;
	private TestSuite testSuite;
	private String timeStamp;
	
	@DynamoDBRangeKey(attributeName = "TEST_ID")
	public Integer getTestID() {
		return testID;
	}
	public void setTestID(Integer testID) {
		this.testID = testID;
	}
	
	@DynamoDBHashKey(attributeName = "UUID")
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	@DynamoDBAttribute(attributeName = "LAMBDA")
	public String getLambda() {
		return lambda;
	}
	public void setLambda(String lambda) {
		this.lambda = lambda;
	}
	
	@DynamoDBAttribute(attributeName = "MODULE")
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	
	@DynamoDBAttribute(attributeName = "ACTION")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	@DynamoDBAttribute(attributeName = "SCENARIO")
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	
	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@DynamoDBAttribute(attributeName = "REQUEST")
	public ScenarioRequest getRequest() {
		return request;
	}
	public void setRequest(ScenarioRequest request) {
		this.request = request;
	}
	
	@DynamoDBAttribute(attributeName = "TEST_SUITE")
	public TestSuite getTestSuite() {
		return testSuite;
	}
	public void setTestSuite(TestSuite testSuite) {
		this.testSuite = testSuite;
	}
	
	@DynamoDBAttribute(attributeName = "TIME_STAMP")
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	@DynamoDBAttribute(attributeName = "RESULT")
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
}
