package com.wio.core.provision.request;

public class DeviceTopics 
{
	private String topic;
	private int qos;
	
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public int getQos() {
		return qos;
	}
	public void setQos(int qos) {
		this.qos = qos;
	}
	@Override
	public String toString() {
		return "DeviceTopics [topic=" + topic + ", qos=" + qos + "]";
	}
	
	
}
