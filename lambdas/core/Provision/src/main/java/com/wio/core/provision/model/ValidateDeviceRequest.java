package com.wio.core.provision.model;

public class ValidateDeviceRequest 
{
		private String deviceID;
		private String mac;
		private String model;
		
				
		public String getMac() {
			return mac;
		}
		public void setMac(String mac) {
			this.mac = mac;
		}
		
		public String getDeviceID() {
			return deviceID;
		}
		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}
		
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		
		
	
}
