package com.wio.core.provision.request;

import java.util.List;

import com.wio.common.config.request.PublishingTopics;

public class PublishingTopicsResponse 
{
	private List<PublishingTopics> topics;

	public List<PublishingTopics> getTopics() {
		return topics;
	}

	public void setTopics(List<PublishingTopics> topics) {
		this.topics = topics;
	}
	
	


}
