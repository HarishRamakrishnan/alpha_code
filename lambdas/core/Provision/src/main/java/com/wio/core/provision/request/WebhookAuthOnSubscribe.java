package com.wio.core.provision.request;

import java.util.List;

public class WebhookAuthOnSubscribe 
{
	private String username;
	private String mountpoint;
	private String clientID;
	private List<DeviceTopics> topics;
	
	private String result;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMountpoint() {
		return mountpoint;
	}
	public void setMountpoint(String mountpoint) {
		this.mountpoint = mountpoint;
	}
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public List<DeviceTopics> getTopics() {
		return topics;
	}
	public void setTopics(List<DeviceTopics> topics) {
		this.topics = topics;
	}	
	
	
}
