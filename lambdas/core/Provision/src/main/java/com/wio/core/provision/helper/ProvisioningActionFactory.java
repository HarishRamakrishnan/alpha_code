package com.wio.core.provision.helper;

import com.wio.common.action.IRequestAction;
import com.wio.common.helper.ActionMapper;
import com.wio.core.provision.action.ValidateDeviceAction;
import com.wio.core.provision.action.mqtt.webhook.AuthOnRegister;
import com.wio.core.provision.action.mqtt.webhook.AuthOnSubscribe;

public class ProvisioningActionFactory {

	
	/**
     * Default constructor
     */
    private ProvisioningActionFactory() {}
    
    public static IRequestAction getUserActionInstance(String action){
    	
    	switch(action)
    	{
    		case ActionMapper.INITIAL_PROVISIONING_VALIDATE_DEVICE:
    			return new ValidateDeviceAction();
    		case ActionMapper.INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_REGISTER:
    			return new AuthOnRegister();
    		case ActionMapper.INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_SUBSCRIBE:
    			return new AuthOnSubscribe();
    		default:
    			return null;	    			
    	}
    	
    }


    
}
