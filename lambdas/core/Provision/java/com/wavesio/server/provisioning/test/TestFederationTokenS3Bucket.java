package com.wavesio.server.provisioning.test;

import java.io.IOException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.wavesio.server.common.configuration.SystemConfigMessages;

public class TestFederationTokenS3Bucket {

	public static void main(String[] args) throws IOException
	{
		try
		{
			BasicSessionCredentials cred = new BasicSessionCredentials("ASIAJIZEOQ4SPDVKDGCQ", 
					"D2AYewFzDpO8DUOPuiUpIFQpyBJpb0eltw6c7IHu", 
					"FQoDYXdzEBAaDL+3rObKcDt+d6f1NCK+A1e/lHRoVKCfTAyTXG2avJOnYToVEJsSSyxG4p6RTbCvTrFedzasPnk/VfCgxvbcOdtgEFYM4v8RS+YSSDcpupz7PYBcL5iAwX8AWYpUmh1/FCv+Q8ty38myCk+hhy+x4G02nLKcm13aBaP/IyFQ1uPFS6VpXuZV9uQ4RmEHFGtUq+swkT8OfrfLq0qgTkQRNNY5j33RRYGGdAIFq3ZeqfgW8o9sTUBm1R2XRYaRJfMYFYiJPFqEiEUWXNzXIt84X3Os+TiEvxaOzd0A4U23Z+eXY5cx1ART8btZCMi1h+vFpVEE+4X8rRQfXK7vu6fVcEL5txmcINd/uwfSEX8qzvEDNYxgblmotKXMyq5u6sDnXlezNN6vZEOMY06BNqrKLBsYN9XAzBL1l2tCVHlPSXnDoTg4HKN7XtjLiZgOO5VVqlxA6aD4X6mMRy3A1aWjGwPFCoaFVaZA2pjYOSMlR5JaIRXUqGpIpb3XTi6UFCSH5apLFenLS1iDn6WSXqecmGfoR21QjVK3BkmkiJ5rJ9r3prByrHIdECb2vrEzM20pNV8tKyebyRtWH7mYd3gQmqp8F2RxOINHA7z9vKrWKJidiMMF");
            
			AmazonS3 s3client = new AmazonS3Client(cred);
	        s3client.setRegion(Region.getRegion(Regions.fromName(SystemConfigMessages.SYSTEM_AWS_REGION)));
	        
	        s3client.createBucket(new CreateBucketRequest("samplebucket"));
	        
	        String bucketLocation = s3client.getBucketLocation(new GetBucketLocationRequest("samplebucket"));
            System.out.println("bucket location = " + bucketLocation);
			
		}
		catch (AmazonServiceException ase) 
		{
	            System.out.println("Caught an AmazonServiceException, which means your request made it " +
	                    "to Amazon SQS, but was rejected with an error response for some reason.");
	            System.out.println("Error Message:    " + ase.getMessage());
	            System.out.println("HTTP Status Code: " + ase.getStatusCode());
	            System.out.println("AWS Error Code:   " + ase.getErrorCode());
	            System.out.println("Error Type:       " + ase.getErrorType());
	            System.out.println("Request ID:       " + ase.getRequestId());
	    } 
		catch (AmazonClientException ace) 
		{
	            System.out.println("Caught an AmazonClientException, which means the client encountered " +
	                    "a serious internal problem while trying to communicate with SQS, such as not " +
	                    "being able to access the network.");
	            System.out.println("Error Message: " + ace.getMessage());
	    }
		catch(Exception e)
		{
			throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
		}
	}
}
