package com.wio.cwmp.dao;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wio.common.config.model.ConfiguredValuesMO;
import com.wio.common.config.model.HPRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Service;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.exception.DAOException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.cwmp.auth.CWMPDetails;
import com.wio.cwmp.helper.SetTR69ParamChangesToWIO;
import com.wio.cwmp.helper.WIOtoTR69ParamMapper;
import com.wio.cwmp.model.ParameterValidator;
import com.wio.cwmp.model.TR69ServiceToWIOParameterMapping;
import com.wio.cwmp.util.RequestValidator;
import com.wio.cwmp.util.WIOConstants;


public class TR69ParametersDAO {
	
	private static final Logger log = Logger.getLogger(TR69ParametersDAO.class);
	
	private static final TR69ParametersDAO tr69ParamInstance = new TR69ParametersDAO();
	
	private DDBGenericDAO genDao = DDBGenericDAO.getInstance();
	
	private TR69ParametersDAO(){
	}
	
	public static TR69ParametersDAO getInstance(){
		return tr69ParamInstance;
	}

	/**
	 * Retrieve ServiceToWIOParameterMapping from the mapping table in DB
	 * 
	 * @param serviceName
	 * @return
	 */
	public TR69ServiceToWIOParameterMapping readParamPropertiesMapping(String serviceName) {
		
		try {
			TR69ServiceToWIOParameterMapping serParams = genDao.getGenericObject(TR69ServiceToWIOParameterMapping.class, serviceName);
			
			if (serParams != null) {
				log.debug("Service Name => " + serParams.getServiceName());
				log.debug("Parameters => " + serParams.getParameters());
				log.info("WIO to "+ serParams.getServiceName()+" Params mapping objects => " + serParams.getParams());
				return serParams;
			}
			log.info("No record found for service name = " + serviceName);
			return null;
		} catch (DAOException e) {
			log.info("Exception while fetching the mapping paramters for service name = " + serviceName);
			log.info("Exception message is " + e.getMessage());
			return null;
		}
	}

	/**
	 * This method will only returns CWMP credentials details only when the device is Online.
	 * 
	 * @param deviceID
	 * @return
	 */
	public CWMPDetails scanCWMPDetails(String deviceID) {

		CWMPDetails creds = null;
		
			if (WIOtoTR69ParamMapper.device != null) {
				String serviceName = WIOtoTR69ParamMapper.device.getServiceName();
				String deviceStatus = WIOtoTR69ParamMapper.device.getStatus();
				log.info(" service name:- " + serviceName + " device status:- " + deviceStatus);
				if (ParameterValidator.isValidString(deviceStatus) && ParameterValidator.isValidString(serviceName)) {
					if ("Online".equalsIgnoreCase(deviceStatus)) {
											
						try {
							WIOtoTR69ParamMapper.service = genDao.getGenericObject(Service.class, serviceName);
						} catch (DAOException e) {
							log.error("Exception while querying service table.");
						}
						
						if(WIOtoTR69ParamMapper.service == null){
							
							if(!RequestValidator.isValidString(WIOtoTR69ParamMapper.errorString ) ){
								log.info("Invalid Service name = "+serviceName);
								WIOtoTR69ParamMapper.errorString = "Invalid Service name 111 = "+serviceName;
							}
							return null;
						}
						creds = new CWMPDetails(WIOtoTR69ParamMapper.service.getCpeUsername(),
								WIOtoTR69ParamMapper.service.getCpePassword());
					}else{
						
						if(!RequestValidator.isValidString(WIOtoTR69ParamMapper.errorString ) ){
							log.info("Device status is not online.");
							WIOtoTR69ParamMapper.errorString = "Device status is not online.";
						}
					}
				} else {
					if(!RequestValidator.isValidString(WIOtoTR69ParamMapper.errorString ) ){
						log.info("Invalid device Status or service name ");
						WIOtoTR69ParamMapper.errorString = "Device Status or service name is null or empty.";
					}
				}
			}else{
				
				if(!RequestValidator.isValidString(WIOtoTR69ParamMapper.errorString )){
					log.info("Invalid Device ID.");
					WIOtoTR69ParamMapper.errorString = "Invalid Device ID.";
				}
			}
		return creds;
	}

	public String createConfigUpdTransaction(DeviceConfiguration configChanges) {
		Gson gson = new Gson();
		log.info("The Final Device configuration ==>> "+gson.toJson(configChanges).toString());
		String transactionID = null;
		ConfiguredValuesMO configValues = new ConfiguredValuesMO();
		configValues.setConfiguration(configChanges);
		configValues.setDeviceId(configChanges.getDeviceId());
		configValues.setStatus("Configured");
		
		try {
			genDao.saveGenericObject(configValues);
			transactionID = configValues.getTransactionId();
			log.info("Generated transaction id = "+configValues.getTransactionId());
			SetTR69ParamChangesToWIO.transID = configValues.getTransactionId();
		} catch (DAOException e) {
			log.error("Unable to save the latest config changes from ACS to CONFIG_UPDATES table."+e.toString());
			e.printStackTrace();
		}
		return transactionID;
		
	}

	/**
	 * UPdates a particular DB attribute. 
	 * 
	 * @param tableName
	 * @param primaryKey
	 * @param pkValue
	 * @param attrName
	 * @param attrValue
	 */
	public void updateDBItem(String tableName, String primaryKey, Object pkValue, String attrName, String attrValue) {

		Item latestItem = null;
		try {
			latestItem = genDao.scanItemFromTable(tableName, primaryKey, pkValue);
			latestItem.withString(attrName, attrValue);
			
			genDao.updateDBItem(tableName,latestItem);

		} catch (DAOException e) {
			log.error("Update DB failed for table " +tableName + " for attribute " + attrName);
		}
	}

	/**
	 * 
	 * This method will update Radio or Lan setting changes received from ACS SETPARAM RPC call. 
	 * 
	 * @param tableName
	 * @param primaryKey
	 * @param pkValue
	 * @param settingType
	 * @param configList
	 */
	@SuppressWarnings({ "unchecked" })
	public void updateDBItem(String tableName, String primaryKey, Object pkValue, String settingType,
			List<?> configList) {

		Item latestItem = null;
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		Type listType = new TypeToken<Map<String, Object>>() {}.getType();
		try {
			if (configList == null) {
				return;
			}
	    	
			latestItem = genDao.scanItemFromTable(tableName, primaryKey, pkValue);
			log.debug("Latest item from the Table "+tableName+" DB:- "+latestItem);
			
			List<Map<String, Object>> finalList = new ArrayList<>();
			switch (settingType) {
			
			case WIOConstants.LAN_STRING:
				
				for (LANSettings lanSetting : (List<LANSettings>)configList){
					// json string to Map<String, Object>
					Map<String, Object> jsonToMap = objGson.fromJson(objGson.toJson(lanSetting), listType);
					finalList.add(jsonToMap);
				}
				
				latestItem.withList(WIOConstants.LAN_SETTINGS, finalList);
				break;

			case WIOConstants.RADIO_STRING:
				for (HPRadioSettings radioSetting : (List<HPRadioSettings>)configList){
					
					// json string to Map<String, Object>
					Map<String, Object> jsonToMap = objGson.fromJson(objGson.toJson(radioSetting), listType);
					finalList.add(jsonToMap);
				}
				
				latestItem.withList(WIOConstants.RADIO_SETTINGS, finalList);
				break;
			default:
				break;
			}
			genDao.updateDBItem(tableName, latestItem);
			log.info("Saved Data for "+ settingType+" setting.");
		} catch (DAOException e) {
			log.error("Update DB failed for table " + tableName + " for attribute " + configList);
		}

	}

	public void updateDBItem(String tableName, String primaryKey, Object pkValue, String settingType,
			WANSettings wanSetting) {

		Item latestItem = null;
		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		Type listType = new TypeToken<Map<String, Object>>() {}.getType();
		
		try {
			if (wanSetting == null) {
				return;
			}
			latestItem = genDao.scanItemFromTable(tableName, primaryKey, pkValue);
			log.debug("Latest item from the Table "+tableName+" DB:- "+latestItem);

			switch (settingType) {
			case WIOConstants.WAN_STRING:
				Map<String, Object> jsonToMap = objGson.fromJson(objGson.toJson(wanSetting), listType);
				latestItem.withMap(WIOConstants.WAN_SETTINGS, jsonToMap);
				break;
			
			default:
				break;
			}
			
			genDao.updateDBItem(tableName, latestItem);
			log.info("Saved Data for Wan setting");
		} catch (DAOException e) {
			log.error("Update DB failed for table " + tableName + " for attribute " + wanSetting);
		}
	}

}
