package com.wio.cwmp.util;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.HWRadio;
import com.wio.cwmp.helper.WIOtoTR69ParamMapper;
import com.wio.cwmp.soap.Fault;
import com.wio.cwmp.soap.FaultUtil;


public class ValidateRadioSettings {
	
	private static Logger log = Logger.getLogger(ValidateRadioSettings.class);
	
	public static void validateValues(List<DeviceRadioSettings> input) throws Fault  {

		boolean validDBDCMode = false;
		boolean validRadioBand = false;
		boolean validRadioMode = false;
		boolean validBandwidth = false;
		boolean validChannel = false;
		int index = 0;
	
		for (DeviceRadioSettings radio : input) {
			// mandatory Field of radioSettings
			
			if (radio.getRadioStatus().equals(RequestValidator.Switch.ON)
					|| radio.getRadioStatus().equals(RequestValidator.Switch.OFF)
							&& !RequestValidator.isEmptyField(radio.getRadioBand())) {
				for (HWRadio hwradio : WIOtoTR69ParamMapper.hardware.getHwRadio()) {
					log.info("validating Hardware Radios.....:::" + hwradio.getRadioName());
					for (String dbdcMode : getCommaSeperatedValueList(hwradio.getDbdcModeSupported())) {
						if (dbdcMode.trim().equalsIgnoreCase(radio.getDbdcMode().trim())) {
							validDBDCMode = true;
							break;
						} else
							validDBDCMode = false;
					}

					if (validDBDCMode) {
						for (String radioBand : getCommaSeperatedValueList(hwradio.getRadioBandSupported())) {
							log.info("HW radio band: " + radioBand.trim());
							log.info("ap radio bandL: " + radio.getRadioBand().trim());

							if (radioBand.trim().equalsIgnoreCase(radio.getRadioBand().trim())) {
								validRadioBand = true;
								break;
							} else
								validRadioBand = false;
						}
					}

					if (validDBDCMode && validRadioBand) {
						for (String radioMode : getCommaSeperatedValueList(hwradio.getRadioModeSupported())) {
							log.info("HW radio mode: " + radioMode);
							log.info("ap radio mode: " + radio.getRadioMode());

							if (radioMode.trim().equalsIgnoreCase(radio.getRadioMode().trim())) {
								validRadioMode = true;
								break;
							} else
								validRadioMode = false;
						}
					}

					if (validDBDCMode && validRadioBand && validRadioMode) {
						for (String bandwidth : getCommaSeperatedValueList(hwradio.getBandwidthSupported())) {
							log.info("HW bandwidth: " + bandwidth);
							log.info("AP bandwidth: " + radio.getBandwidth());

							if (bandwidth.trim().equalsIgnoreCase(radio.getBandwidth().trim())) {
								validBandwidth = true;
								break;
							} else
								validBandwidth = false;
						}
					}

					if (validRadioBand && !validRadioMode)
						throw new Fault(FaultUtil.FAULT_9007,
								"Attempt to set a invalid Radio Mode. Value should be YES or NO.",
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_MODE"), null);

					if (validRadioBand && !validBandwidth)
						throw new Fault(FaultUtil.FAULT_9007, "Attempt to set a invalid Bandwidth. Value should be YES or NO.",
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_MODE"), null);

					if (validRadioBand && validRadioMode && validBandwidth)
						break;
				}


				if (!validRadioBand)
					throw new Fault(FaultUtil.FAULT_9007, "Attempt to set a invalid Radio Band. ",
							WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_BAND"), null);

				if (!validRadioMode)
					throw new Fault(FaultUtil.FAULT_9007, "Attempt to set a invalid Radio Mode. Value should be YES or NO.",
							WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_MODE"), null);

				if (!validBandwidth)
					throw new Fault(FaultUtil.FAULT_9007, "Attempt to set a invalid Bandwidth.",
							WIOtoTR69ParamMapper.radioParamMap.get(index).get("BANDWIDTH"), null);
			}

			if (radio.getRadioStatus().equals(RequestValidator.Switch.ON)) {
				if (!RequestValidator.isEmptyField(radio.getRadioMode())
						|| RequestValidator.isEmptyField(radio.getBandwidth())
						|| RequestValidator.isEmptyField(radio.getChannel()))
					throw new Fault(FaultUtil.FAULT_9007, "Either give radio band or remove radioMode/Bandwidth/channel.",
							WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_MODE"), null);
			}

			;

			if (radio.getRadioBand().equalsIgnoreCase("2.4 GHz ISM")) {
				if (radio.getBandwidth().equals("20 MHz")) {
					log.info("For 20MHz radio = 802.11 b/g" + radio.getRadioMode().equals("802.11 b "));
					if (!radio.getRadioMode().equals("802.11 b") && !radio.getRadioMode().equals("802.11 g"))
						throw new Fault(FaultUtil.FAULT_9007, "invalid combination of radio (802.11 b/g) band/mode/bandwidth.",
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_MODE"), null);
				}
				if (radio.getBandwidth().equals("40 MHz")) {
					log.info("For 40 MHz radio .. ");
					if (!radio.getRadioMode().equals("802.11 n") && !radio.getRadioMode().equals("802.11 bgn-mixed"))
						throw new Fault(FaultUtil.FAULT_9007,
								"invalid combination of radio (802.11 bgn-mixed) (" + radio.getBandwidth()
										+ ") band/mode/bandwidth for 2.4GHz ISM radio band.",
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("RADIO_MODE"), null);
				}

					for (String channel : getCommaSeperatedValueList(WIOConstants.CHANNEL1)) {
						log.info("ap channel: " + radio.getChannel());
						log.info("Channel ==> "+ channel);
						if (channel.trim().equals(radio.getChannel().trim())) {
							validChannel = true;
							break;
						} else{
							validChannel = false;
						}

					}
					if (!validChannel)
						throw new Fault(FaultUtil.FAULT_9007, "Attempt to set a invalid Channel.",
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("CHANNEL"), null);
			}

			if (radio.getRadioBand().equalsIgnoreCase("5 GHzU-NII-2A")
					|| radio.getRadioBand().equalsIgnoreCase("5 GHzU-NII-2B")
					|| radio.getRadioBand().equalsIgnoreCase("5 GHzU-NII-3")) {
				if (radio.getRadioMode().equals("802.11 a")) {
					if (!radio.getBandwidth().equals("40 MHz"))
						throw new Fault(FaultUtil.FAULT_9007,
								"invalid combination of radio band/mode/bandwidth for Radio band "
										+ radio.getRadioBand() + " and Radio Mode " + radio.getRadioMode(),
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("BANDWIDTH"), null);
				}
				if (radio.getRadioMode().equals("802.11 ac")) {
					if (!radio.getBandwidth().equals("160 MHz") && !radio.getBandwidth().equals("80 MHz")
							&& !radio.getBandwidth().equals("80+80 MHz"))
						throw new Fault(FaultUtil.FAULT_9007, "invalid combination of radio mode/bandwidth for Radio band "
								+ radio.getRadioBand() + " and Radio Mode " + radio.getRadioMode(),
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("BANDWIDTH"), null);
				}
				if (!RequestValidator.isEmptyField(radio.getChannel())) {

					for (String channel : getCommaSeperatedValueList(WIOConstants.CHANNEL2)) {
						System.out.println("constyant channel: " + channel);
						System.out.println("ap channel: " + radio.getChannel());

						if (channel.trim().equals(radio.getChannel().trim())) {
							validChannel = true;
							break;
						} else
							validChannel = false;

					}
					if (!validChannel)
						throw new Fault(FaultUtil.FAULT_9007, "Attempt to set a invalid Channel. ",
								WIOtoTR69ParamMapper.radioParamMap.get(index).get("CHANNEL"), null);
				}
			}

			//radio.setBabel("Disabled");
			index++;
		}

	}
    
    public static List<String> getCommaSeperatedValueList(String fullString)
	{
		String [] separatedValues = fullString.split(",");				
		return Arrays.asList(separatedValues);
	}
}
