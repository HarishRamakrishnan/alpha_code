package com.wio.cwmp.util;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.Properties;

public class UtilProperties {
	
	public Properties loadProperties()
	{
		Properties prop = new Properties();
		//Properties prop1 = new Properties();
	    
		ClassLoader classLoader = getClass().getClassLoader();
		try {
		    prop.load(classLoader.getResourceAsStream("Exception.properties"));
		    //prop.load(classLoader.getResourceAsStream("SystemConfig.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	
	//Text to MD5 Encryption
	public static String encrypt(String plainText) {  
	    String encryptedText = "";  
	    try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        byte encryptedData[] = md.digest(plainText.getBytes());  
	        StringBuffer hexString = new StringBuffer();  
	        for (int i = 0; i < encryptedData.length; i++) {  
	            String hex = Integer.toHexString(0xFF & encryptedData[i]);  
	            if (hex.length() == 1) {  
	                hexString.append('0');  
	            }  
	            hexString.append(hex);  
	        }  
	        encryptedText = hexString.toString();  

	    } catch (Exception e) {  
	        e.printStackTrace();  
	    }  
	    return encryptedText;  
	}

}
