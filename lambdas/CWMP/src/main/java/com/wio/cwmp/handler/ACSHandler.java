package com.wio.cwmp.handler;

import java.util.Iterator;

import org.apache.log4j.Logger;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.cwmp.RPCMethodMng;
import com.wio.cwmp.TR69ParametersReader;
import com.wio.cwmp.api.EventCode;
import com.wio.cwmp.api.FileUtil;
import com.wio.cwmp.api.ICom;
import com.wio.cwmp.api.IModel;
import com.wio.cwmp.api.RPCMethodMngService;
import com.wio.cwmp.helper.AWSLambdaClientInvoker;
import com.wio.cwmp.helper.Com;
import com.wio.cwmp.helper.SetTR69ParamChangesToWIO;
import com.wio.cwmp.model.EventStruct;
import com.wio.cwmp.model.ParamDataModel;
import com.wio.cwmp.model.ParameterData;
import com.wio.cwmp.model.ParameterValidator;
import com.wio.cwmp.model.TR69ServiceParameter;

public class ACSHandler {
	
	
	static LambdaLogger logger = null;
    private static final String INDENT_OUTPUT = "http://xmlpull.org/v1/doc/features.html#indent-output";
    
    public static final Logger log = Logger.getLogger(ACSHandler.class);
    /** The session id. */
    private String sessionId = "";
    
    private FileUtil fileUtil = new FileUtil();
    /**
     * Gets the session id.
     * @return the session id
     */
    public String getSessionId() {
        return sessionId;
    }

    // private ManagementServerDDTO managementServer = new
    // ManagementServerDDTO();
    
    private ParameterData data = new ParameterData();
    
    private ICom com = null;
    
    public void setCom(ICom com) {
		this.com = com;
		com.setRunning(true);
		data.setCom(com);
	}

    public void unsetCom(ICom com) {
    	this.com = null;
    	com.setRunning(false);
    	data.setCom(null);
    }
    
    private IModel model = null;
    
   
    public void setModel(IModel model) {
		this.model = model;
	}
    
    private RPCMethodMngService rpcMethodMng;
    
    public void setRpcMethodMng(RPCMethodMngService rpcMethodMng) {
		this.rpcMethodMng = rpcMethodMng;
	}
    
    /**
     * Start t r69.
     */
    public void startTR69(String deviceId) {
        log.info("TR69Client is starting");
        model = new ParamDataModel();
        com = new Com();
        rpcMethodMng = RPCMethodMng.getInstance();
        data.setRoot(fileUtil.loadProperties().getProperty("root"));
        //data.setRoot("InternetGatewayDevice.");
        log.info("Root is " + data.getRoot());            
        
        putDefaultParameter(deviceId);
        
     // put the data model structure
        model.setData(data);
        
        log.info("=======================");
        log.info("Model is ");
        log.info("=======================");
        // put data into data model
        Iterator<TR69ServiceParameter> it = data.getParameterIterator();
        while (it.hasNext()) {
        	TR69ServiceParameter p = it.next();
            log.debug("Param tostring ==> "+p.toString());
        }
        
        log.debug("===========================");
        log.debug("Model is after restore ");
        log.debug("===========================");
        log.info("ParameterData to string ====>>"+data.toString());
        com.setParameterData(data);
        com.setRPCMng(rpcMethodMng);
        it = data.getParameterIterator();
        // save data model
        while (it.hasNext()) {
        	TR69ServiceParameter p = it.next();
        }
        
        com.setRunning(true);
        com.startCOM();
        // uncomment this line once input details confirmed.
        //AWSLambdaClientInvoker.invokeConfigLambda(SetTR69ParamChangesToWIO.transID);
        log.info("Going to shutdown CWMP..");
    }

    /**
     * Put default parameter.
     * @see com.francetelecom.admindm.api.ICSV#putDefaultParameter()
     */
    public void putDefaultParameter(String deviceId) {

        data.addEvent(new EventStruct(EventCode.BOOTSTRAP, ""));
        data.addEvent(new EventStruct(EventCode.BOOT, ""));
        //data.addOrUpdateParameter(param, updater);
        TR69ParametersReader reader;
        reader = new TR69ParametersReader(data);
        log.info("-------- Device ID is ----------" +deviceId);
        if(!ParameterValidator.isValidString(deviceId)){
        	log.error("Not a valid deviceId = "+deviceId);
        	return;
        }
        
        
        DeviceConfiguration wavesParam = reader.readWIOParamsFromDB(deviceId);
        if(wavesParam != null){
        	reader.populateParameterData(wavesParam);
        }else{
        	System.exit(0);
        }
        
    }

}
