package com.wio.cwmp.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.Hardware;
import com.wio.common.config.model.HardwareProfile;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.Service;
import com.wio.common.config.model.WANSettings;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.DeviceConfigurationRequest;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.cwmp.model.TR69ServiceParameter;
import com.wio.cwmp.model.TR69ServiceToWIOParameterMapping;
import com.wio.cwmp.soap.Fault;
import com.wio.cwmp.util.WIOConstants;


public class WIOtoTR69ParamMapper {

	public static Map<String, TR69ServiceParameter> serviceParameterMap = new HashMap<>();
	public static Map<Integer, Map<String,String>> radioParamMap = new HashMap<>();
	public static DeviceConfiguration wavesParameters = null;
	public static Hardware hardware = null;
	public static HardwareProfile hardwareProfile = null;
	public static DeviceConfigurationRequest deviceConfig = null;
	public static Device device = null;
	public static Service service = null;
	private static final Logger log = Logger.getLogger(WIOtoTR69ParamMapper.class);
	public static String errorString = null;

	/**
	 * populate maps 
	 * e.g :- 
	 * 	serviceParameterMap ==> Map<ServiceParamName, TR69ServiceParameter>
	 * @param valueList
	 */
	private static Map<String, TR69ServiceParameter>  populateConfigSettingsToMaps(List<TR69ServiceParameter> valueList,
			DeviceConfiguration wavesParameters) {

		WIOtoTR69ParamMapper.wavesParameters = wavesParameters;
		log.info("Constructing the number of service params to be reported to ACS..");
		if (valueList == null) {
			log.error(
					" Ensure mapping table 'TR69_PARAM_PROPERTIES' is polulated in AWS dynamoDB for the service provider "
							+ wavesParameters.getServiceName());
			return serviceParameterMap;
		}
		try {
			for (int i = 0; i < valueList.size(); i++) {
				TR69ServiceParameter value = valueList.get(i);

				switch (value.getSettingType()) {
				case WIOConstants.LAN_STRING:
					populateLANSettings(wavesParameters, value);
					break;
				case WIOConstants.WAN_STRING:
					populateWANSettings(wavesParameters, value);
				break;	
				case WIOConstants.RADIO_STRING:
					populateRadioSettings(wavesParameters, value);
					break;
				case WIOConstants.WIFI_STRING:
					populateWiFiSettings(wavesParameters, value);
					break;
				case WIOConstants.NETWORK_STRING:
					populateNetworkSettings(wavesParameters, value);
					break;
				default:
					switch (value.getWavesParamName()) {
					case "MANUFACTURER":
						value.setServiceParamValue(wavesParameters.getManufacturer());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "MANUFACTURER_OUI":
						value.setServiceParamValue(wavesParameters.getManufacturerOUI());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "PRODUCT_CLASS":
						value.setServiceParamValue(wavesParameters.getProductClass());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "SERIAL_NUMBER":
						value.setServiceParamValue(wavesParameters.getSerialNumber());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "HARDWARE_VERSION":
						value.setServiceParamValue(wavesParameters.getHardwareVersion());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "SOFTWARE_VERSION":
						value.setServiceParamValue(wavesParameters.getSoftwareVersion());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "PROVISIONING_CODE":
						value.setServiceParamValue(wavesParameters.getProvisioningCode());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "SPEC_VERSION":
						value.setServiceParamValue(wavesParameters.getSpecVersion());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "DESCRIPTION":
						value.setServiceParamValue(wavesParameters.getDescription());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "ACS_SERVER_URL":
						value.setServiceParamValue(wavesParameters.getAcsServerUrl());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "ACS_PASSWORD":
						value.setServiceParamValue(wavesParameters.getAcsPassword());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "ACS_USERNAME":
						value.setServiceParamValue(wavesParameters.getAcsUsername());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "CPE_USERNAME":
						value.setServiceParamValue(wavesParameters.getConnectionRequestUsername());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "CPE_PASSWORD":
						value.setServiceParamValue(wavesParameters.getConnectionRequestPassword());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					case "CPE_GATEWAY_URL":
						value.setServiceParamValue(wavesParameters.getConnectionRequestURL());
						serviceParameterMap.put(value.getServiceParamName(), value);
						break;
					default:
						break;
					}
					break;
				}

			}

		} catch (Fault e) {
			e.printStackTrace();
			log.error("Fault was generated.." + e.getFaultstring());
		}
		log.info("No of service parameter's to be reported to ACS = " + serviceParameterMap.size());
		return serviceParameterMap;

	}

	private static void populateNetworkSettings(DeviceConfiguration wavesParameters, TR69ServiceParameter value)
			throws Fault {
		TR69ServiceParameter servParam = null;
		NetworkSettings networkSettings = null;
		int index = wavesParameters.getWifiSetting().size();
		
		for (int i = 1; i <= index; i++) {
			servParam = defaultToSpecificTR69ServParam(value, i);
			networkSettings = wavesParameters.getWifiSetting().get((i-1)).getNetworkSettings();

			switch (value.getWavesParamName()) {

			case "DHCP_SERVER":
				servParam.setServiceParamValue(networkSettings.getDhcpServer());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "IP_POOL_START":
				servParam.setServiceParamValue(networkSettings.getIpPoolStart());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "IP_POOL_END":
				servParam.setServiceParamValue(networkSettings.getIpPoolEnd());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "LEASE_TIME":
				servParam.setServiceParamValue(networkSettings.getLeaseTime());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "MAC_ADDRESS":
				servParam.setServiceParamValue(networkSettings.getMacFilterList().get(0).getMacAddress());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;

			default:
				break;

			}
		}
	}

	private static TR69ServiceParameter defaultToSpecificTR69ServParam(TR69ServiceParameter value, int i) {
		TR69ServiceParameter servParam;
		String serviceParamName;
		servParam = new TR69ServiceParameter();
		serviceParamName = value.getServiceParamName().replaceAll("\\{i\\}", String.valueOf(i));
		servParam.setIndex(i);
		servParam.setServiceParamName(serviceParamName);
		servParam.setRebootRequired(value.getRebootRequired());
		servParam.setSettingType(value.getSettingType());
		servParam.setTableName(value.getTableName());
		servParam.setWavesParamName(value.getWavesParamName());
		servParam.setWritable(value.getWritable());
		return servParam;
	}


	private static void populateWiFiSettings(DeviceConfiguration wavesParameters, TR69ServiceParameter value)
			throws Fault {
		
		TR69ServiceParameter servParam = null;
		WiFiSettings wifiSetting  = null;
		int index = wavesParameters.getWifiSetting().size();
		
		for (int i = 1; i <= index; i++) {
			servParam = defaultToSpecificTR69ServParam(value, i);
			wifiSetting = wavesParameters.getWifiSetting().get((i-1));

			switch (value.getWavesParamName()) {

			case "SSID":
				servParam.setServiceParamValue(wifiSetting.getSsid());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "AUTHENTICATION_TYPE":
				servParam.setServiceParamValue(wifiSetting.getAuthenticationType());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "ENCRYPTION_TYPE":
				servParam.setServiceParamValue(wifiSetting.getEncryptionType());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "PASS_PHRASE":
				servParam.setServiceParamValue(wifiSetting.getPassPhrase());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "ENABLE_WIRELESS":
				servParam.setServiceParamValue(wifiSetting.getEnableWireless());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;

			default:
				break;

			}
		}
		
	}

	private static void populateRadioSettings(DeviceConfiguration wavesParameters, TR69ServiceParameter value)
			throws Fault {

		TR69ServiceParameter servParam = null;
		DeviceRadioSettings radioSetting = null;
		int index = wavesParameters.getRadioSetting().size();

		for (int i = 1; i <= index; i++) {
			servParam = defaultToSpecificTR69ServParam(value, i);

			radioSetting = wavesParameters.getRadioSetting().get((i-1));

			switch (value.getWavesParamName()) {

			case "BANDWIDTH":
				servParam.setServiceParamValue(radioSetting.getBandwidth());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "CHANNEL":
				servParam.setServiceParamValue(radioSetting.getChannel());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "RADIO_NAME":
				servParam.setServiceParamValue(radioSetting.getRadioName());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "RADIO_STATUS":
				servParam.setServiceParamValue(radioSetting.getRadioStatus());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "RADIO_BAND":
				servParam.setServiceParamValue(radioSetting.getRadioBand());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "RADIO_MODE":
				servParam.setServiceParamValue(radioSetting.getRadioMode());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "TRANSMIT_POWER":
				servParam.setServiceParamValue(radioSetting.getTransmitPower());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;

			default:
				break;

			}
		}
	}

	private static void populateWANSettings(DeviceConfiguration wavesParameters, TR69ServiceParameter value)
			throws Fault {
		WANSettings wanSetting = wavesParameters.getWanSetting();
		if(wanSetting.getWanType().equalsIgnoreCase("Static")){
			switch (value.getWavesParamName()) {
			
			case "WAN_TYPE":
				String staticWanType = "";
				value.setServiceParamValue(wanSetting.getWanType());
				staticWanType = value.getServiceParamName().replace("XXX", "WANIPConnection");
				staticWanType = staticWanType.replace("YYY", "AddressingType");
				
				value.setServiceParamName(staticWanType);
				log.info("WAN_TYPE ====>> "+staticWanType);
				serviceParameterMap.put(staticWanType, value);
				break;
				
			case "IFNAME":
				value.setServiceParamValue(wanSetting.getIfName());
				serviceParameterMap.put(value.getServiceParamName(), value);
			case "GATEWAY":
				value.setServiceParamValue(wanSetting.getGateway());
				serviceParameterMap.put(value.getServiceParamName(), value);
				break;
			case "SUBNET_MASK":
				value.setServiceParamValue(wanSetting.getSubnetMask());
				serviceParameterMap.put(value.getServiceParamName(), value);
				break;
			case "IPV6":
				value.setServiceParamValue(wanSetting.getIpv6());
				serviceParameterMap.put(value.getServiceParamName(), value);
				break;
			case "DNS_PRIMARY":
				value.setServiceParamValue(wanSetting.getDnsPrimary() + "," + wanSetting.getDnsSecondary());
				serviceParameterMap.put(value.getServiceParamName(), value);
				break;
			case "IPV4":
				value.setServiceParamValue(wanSetting.getIpv4());
				serviceParameterMap.put(value.getServiceParamName(), value);
				break;
			case "DNS_SECONDARY":
				value.setServiceParamValue(wanSetting.getDnsPrimary() + "," + wanSetting.getDnsSecondary());
				serviceParameterMap.put(value.getServiceParamName(), value);
				break;
				
			case "NAT":
				String staticNAT = "";
				value.setServiceParamValue(wanSetting.getNat());
				
				if(value.getServiceParamName().contains("XXX")){
					staticNAT = value.getServiceParamName().replace("XXX", "WANIPConnection");
					value.setServiceParamName(staticNAT);
				}
				
				serviceParameterMap.put(staticNAT, value);
				break;
			default:
				break;
			}
			
		} else if(wanSetting.getWanType().equalsIgnoreCase("PPPoE") || wanSetting.getWanType().equalsIgnoreCase("PPPoA")){
			switch (value.getWavesParamName()) {
				case "WAN_TYPE":
				String pppWanType = "";
				value.setServiceParamValue(wanSetting.getWanType());
				pppWanType = value.getServiceParamName().replace("XXX", "WANPPPConnection");
				pppWanType = pppWanType.replace("YYY", "TransportType");
				value.setServiceParamName(pppWanType);
				serviceParameterMap.put(pppWanType, value);
				break;
				
				case "USERNAME":
					value.setServiceParamValue(wanSetting.getUsername());
					serviceParameterMap.put(value.getServiceParamName(), value);
					break;
				case "PASSWORD":
					value.setServiceParamValue(wanSetting.getPassword());
					serviceParameterMap.put(value.getServiceParamName(), value);
					break;
				case "NAT":
					String pppNAT = "";
					value.setServiceParamValue(wanSetting.getNat());
					
					if(value.getServiceParamName().contains("XXX")){
						pppNAT = value.getServiceParamName().replace("XXX", "WANPPPConnection");
						value.setServiceParamName(pppNAT);
					}
					
					serviceParameterMap.put(pppNAT, value);
					break;
				default:
					break;

			}
		} else { // wan type = dynamic
			switch (value.getWavesParamName()) {
			
			case "WAN_TYPE":
				String dynamicWanType = "";
				value.setServiceParamValue(wanSetting.getWanType());
				dynamicWanType = value.getServiceParamName().replace("XXX", "WANIPConnection");
				dynamicWanType = dynamicWanType.replace("YYY", "AddressingType");
				value.setServiceParamName(dynamicWanType);
				
				serviceParameterMap.put(dynamicWanType, value);
				break;
				
				case "NAT":
					String dynamicNAT = "";
					value.setServiceParamValue(wanSetting.getNat());
					
					if(value.getServiceParamName().contains("XXX")){
						dynamicNAT = value.getServiceParamName().replace("XXX", "WANIPConnection");
						value.setServiceParamName(dynamicNAT);
					}
					
					serviceParameterMap.put(dynamicNAT, value);
					break;
				default:
					break;
			
			}
		}
	}

	private static void populateLANSettings(DeviceConfiguration wavesParameters, TR69ServiceParameter value)
			throws Fault {
		
		TR69ServiceParameter servParam = null;
		LANSettings lanSetting  = null;
		int index = wavesParameters.getLanSetting().size();

		for (int i = 1; i <= index; i++) {
			servParam = defaultToSpecificTR69ServParam(value, i);
			
			lanSetting = wavesParameters.getLanSetting().get((i-1));

			switch (value.getWavesParamName()) {
			
			case "IFNAME":
				servParam.setServiceParamValue(lanSetting.getIfName());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);

			case "GATEWAY":
				servParam.setServiceParamValue(lanSetting.getGateway());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "SUBNET_MASK":
				servParam.setServiceParamValue(lanSetting.getSubnetMask());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "IPV6": // renamed from STATIC_IPV6 in the TR69_PARAM_MAPPING table
				servParam.setServiceParamValue(lanSetting.getIpV6());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "DNS_PRIMARY":
				servParam.setServiceParamValue(lanSetting.getDnsPrimary() + "," + lanSetting.getDnsSecondary());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "IPV4":// renamed from STATIC_IPV4 in the TR69_PARAM_MAPPING table
				servParam.setServiceParamValue(lanSetting.getIpV4());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;
			case "DNS_SECONDARY":
				servParam.setServiceParamValue(lanSetting.getDnsPrimary() + "," + lanSetting.getDnsSecondary());
				serviceParameterMap.put(servParam.getServiceParamName(), servParam);
				break;

			default:
				break;

			}
		}
		
	}


	/**
	 * Retrieve 'parameters' column from Mapping table (TR69_Param_Properties)
	 * and parse that JSONArray to a Map
	 * 
	 * Map<String, TR69ServiceParameter> ==> Map <ServiceParamName ,TR69ServiceParameter>
	 * 
	 * @param parameters
	 * @return
	 */
	public static Map<String, TR69ServiceParameter> convertTR69ParamPropToMap(
			TR69ServiceToWIOParameterMapping parameters, DeviceConfiguration wavesParameters) {

		Map<String, TR69ServiceParameter> paramData = null;
		paramData = populateConfigSettingsToMaps(parameters.getParams(),
				wavesParameters);

		return paramData;

	}
	
	/**
	 * parse json array to list of Class<T> in specific to TR69ServiceParameter here.
	 * 
	 * @param array
	 * @return
	 * @throws JSONException
	 */
	/*public static <T> List<T> populateJsonArrToList(JSONArray array, Class<T> genericModel) throws JSONException {

		ObjectMapper mapper = new ObjectMapper();
		List<T> valueList = null;

		try {
			// valueList = {TR69ServiceParameter,TR69ServiceParameter,TR69ServiceParameter}

			valueList = mapper.readValue(array.toString(),
					mapper.getTypeFactory().constructCollectionType(List.class, genericModel));
			
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return valueList;
	}

	public static <T> Class<T> convertToWavesParam(JSONObject newObj, Class<T> typeKey) throws Exception {

		ObjectMapper mapper = new ObjectMapper().setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		Class<T> typeKey1 = null;

		try {

			typeKey1 = (Class<T>) mapper.readValue(newObj.toString(), typeKey);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return typeKey1;

	}


	private static LANSettings convertToLanSetting(JSONObject newObj) throws Exception {

		ObjectMapper mapper = new ObjectMapper().setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		LANSettings sett = null;

		try {
			sett = mapper.readValue(newObj.toString(), LANSettings.class);

			log.info("Inside mapper.." + sett.toString());
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sett;

	}

/*	public static Map<String, Object> parseJSONObjectToMap(JSONObject jsonObject) throws JSONException {
		Map<String, Object> mapData = new HashMap<String, Object>();
		Iterator<String> keysItr = jsonObject.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = jsonObject.get(key);

			if (value instanceof JSONArray) {
				value = parseJSONArrayToList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = parseJSONObjectToMap((JSONObject) value);
			}
			mapData.put(key, value);
		}
		
		log.info("parseJSONObjectToMap ===============>> "+mapData);
		
		return mapData;
	}

	public static List<Object> parseJSONArrayToList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = parseJSONArrayToList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = parseJSONObjectToMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}
	
	public static Map<String, List<Object>> parseTR69Mapping(JSONObject jsonObject) throws JSONException {
		Map<String, List<Object>> mapData = new HashMap<String, List<Object>>();
		Iterator<String> keysItr = jsonObject.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			if ("parameters".equals(key)) {
				Object value = jsonObject.get(key);
				if (value instanceof JSONArray) {
					mapData.put(key, parseJSONArrayToList((JSONArray) value));
				}
			}

		}
		return mapData;
	}*/

	public static String convertPOJOToJson(Object typeKey) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		// Convert object to JSON string and pretty print
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(typeKey);
			
			log.debug("JSON String ==>> "+jsonInString);
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
	
	
}
