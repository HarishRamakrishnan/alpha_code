package com.wio.cwmp.helper;

import org.apache.log4j.Logger;

import com.wio.common.aws.api.lambda.LambdaClientAPI;

public class AWSLambdaClientInvoker {

	private static final Logger log = Logger.getLogger(AWSLambdaClientInvoker.class);
	
	public static String invokeConfigLambda(String transID) {
		String source = "ConfigLambda";
		String lambdaFunctionName = "ConfigLambda";
		String deviceJson = "{ \"params\": { \"path\": {}, \"header\": { \"userName\": \"\", \"password\": \"\", \"calledBy\": \"" +source +"\" }, \"querystring\": { \"transID\": \"" +transID + "\" } } }";
		log.info(deviceJson);
		return LambdaClientAPI.invoke(lambdaFunctionName, deviceJson);
	}
	
}
