package com.wio.cwmp.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.wio.cwmp.soap.Fault;

/**
 * 
 * @author mageshb
 *
 */
@DynamoDBDocument
public class TR69ServiceParameter {

	private String wavesParamName;
	private String serviceParamName;
	private Boolean writable;
	private Boolean rebootRequired;
	private String tableName;
	private String settingType;
	private int index;
	private String serviceParamValue;
	
	public TR69ServiceParameter() {

	}
	
	@DynamoDBAttribute(attributeName = "WAVES_PARAM_NAME")
	public String getWavesParamName() {
		return wavesParamName;
	}
	public void setWavesParamName(String wavesParamName) {
		this.wavesParamName = wavesParamName;
	}
	
	@DynamoDBHashKey(attributeName = "SERVICE_PARAM_NAME")
	public String getServiceParamName() {
		return serviceParamName;
	}
	public void setServiceParamName(String serviceParamName) {
		this.serviceParamName = serviceParamName;
	}
	
	@DynamoDBAttribute(attributeName = "WRITABLE")
	public Boolean getWritable() {
		return writable;
	}
	public void setWritable(Boolean writable) {
		this.writable = writable;
	}
	
	@DynamoDBAttribute(attributeName = "REBOOT_REQUIRED")
	public Boolean getRebootRequired() {
		return rebootRequired;
	}
	public void setRebootRequired(Boolean rebootRequired) {
		this.rebootRequired = rebootRequired;
	}

	@DynamoDBAttribute(attributeName = "TABLE_NAME")
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	@DynamoDBAttribute(attributeName = "SETTING_TYPE")
	public String getSettingType() {
		return settingType;
	}
	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	@DynamoDBAttribute(attributeName = "INDEX")
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
	@DynamoDBAttribute(attributeName = "SERVICE_PARAM_VALUE")
	public String getServiceParamValue() {
		return serviceParamValue;
	}
	public void setServiceParamValue(String serviceParamValue) throws Fault {
		this.serviceParamValue = serviceParamValue;
	}


	public boolean getMandatoryNotification() {
		return true;
	}
	// Below 3 methods need to be checked if required.
	/*public int getType() {
		return 5;
	}

	public boolean isImmediateChanges() {
		return true;
	}*/
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serviceParamName == null) ? 0 : serviceParamName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TR69ServiceParameter other = (TR69ServiceParameter) obj;
		if (serviceParamName == null) {
			if (other.serviceParamName != null)
				return false;
		} else if (!serviceParamName.equals(other.serviceParamName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TR69ServiceParameter [wavesParamName=" + wavesParamName + ", serviceParamName="
				+ serviceParamName + ", Writable=" + writable + ", RebootRequired=" + rebootRequired + ", tableName="
				+ tableName + ", settingType=" + settingType + ", index=" + index + ", serviceParamValue="
				+ serviceParamValue + "]";
	}

	


}
