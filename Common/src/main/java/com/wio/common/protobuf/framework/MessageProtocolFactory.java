package com.wio.common.protobuf.framework;

public class MessageProtocolFactory implements IMessageProtocol {
	
	private static MessageProtocolFactory factoryInst = new MessageProtocolFactory();
	
	private MessageProtocolFactory() {
	}
	
	public static MessageProtocolFactory getFactoryInst() {
		
		return factoryInst;
	}

	@Override
	public IMessageSerializer getMessageProtocol(String protocolType) {
		
		IMessageSerializer msgSer = null;
		
		switch(protocolType) {
		
		case "PROTPBUF":
			
			msgSer = new ProtoBufferMessage();
			
			break;
		case "AVRO":
			break;
			
		case "THRIFT":
			break;
		}
		return msgSer;
	}

	
}
