package com.wio.common.protobuf.impl;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.wio.common.protobuf.framework.IMessage;
import com.wio.common.protobuf.generated.ConfigUpdatesProto;
import com.wio.common.protobuf.generated.ConfigUpdatesProto.ConfigUpdates;
import com.wio.common.validation.RequestValidator;
 

public class ConfigProtoData implements IMessage {
	
	
	private ConfigUpdates.Builder configUpd = null;
	private ConfigUpdates.ConfigParam.Builder param = null;
	
	private static IMessage message = new ConfigProtoData();
	
	private ConfigProtoData() {
	}

	public static IMessage getInstance() {
		return message;
	}
	
	@Override
	public void messageConstructor() {
		configUpd = ConfigUpdates.newBuilder();
		param = ConfigUpdates.ConfigParam.newBuilder();
	}

	@Override
	public GeneratedMessageV3 serialize() {
		return configUpd.build();
	}

	@Override
	public ConfigUpdates deSerialize(byte[] input) {
		return getConfigUpdates(input);
	}
	
	public String deSerializeToString(byte[] input) {
		return getConfigUpdates(input).toString();
	}
	
	private ConfigUpdatesProto.ConfigUpdates getConfigUpdates(byte[] input) {
		ConfigUpdatesProto.ConfigUpdates confUpds = null;
		try {
			if(input != null && input.length > 0) {
				confUpds = ConfigUpdatesProto.ConfigUpdates.parseFrom(input);
			}else {
				System.out.println("Accepts only byte array input");
			}
		} catch (InvalidProtocolBufferException e) {
			System.err.println("Exception in parsing..");
			e.printStackTrace();
		}
		return confUpds;
	}

	/**
	 * This method will take all the required fields as input to construct the message.
	 * 
	 * {#message ConfigUpdates}
	 * 
	 */
	public void setConfigUpdateDetails(String action, String time,
			String deviceId, String configStatus, String transactionId) {
		
		if(!RequestValidator.isEmptyField(action)) {
			configUpd = configUpd.setAction(action);
		}
		if(!RequestValidator.isEmptyField(time)) {
			configUpd = configUpd.setTime(time);
		}
		if(!RequestValidator.isEmptyField(deviceId)) {
			configUpd = configUpd.setDeviceid(deviceId);
		}
		if(!RequestValidator.isEmptyField(configStatus)) {
			configUpd = configUpd.setStatus(configStatus);
		}
		if(!RequestValidator.isEmptyField(transactionId)) {
			configUpd = configUpd.setTransid(transactionId);
		}
		System.out.println("All Fields >> "+configUpd.getAllFields());
	}
	
	public void setConfigParamDetails(String paramName, String paramValue) {
		if(!RequestValidator.isEmptyField(paramName)) {
			param = param.setKey(paramName);
		}
		if(!RequestValidator.isEmptyField(paramValue)) {
			param = param.setValue(paramValue);
		}
		configUpd = configUpd.addParam(param);
	}
	

	public ByteString getConfigUpdateByteString() {
		return configUpd.build().toByteString();
	}
	
	@Override
	public String description() {
		return "Google ProtoBuf";
	}

	@Override
	public String schema() {
		return "wioCOnfigUpdates.proto";
	}

}
