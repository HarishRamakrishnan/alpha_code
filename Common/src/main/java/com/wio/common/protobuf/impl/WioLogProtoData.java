package com.wio.common.protobuf.impl;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.wio.common.protobuf.framework.IMessage;
import com.wio.common.protobuf.generated.WioLogProto.WIOLOG;
import com.wio.common.validation.RequestValidator;


public class WioLogProtoData implements IMessage {
	
	private static WioLogProtoData inst = new WioLogProtoData();
	
	private WIOLOG.Builder wioLog = null;
	
	private WioLogProtoData() {
	}
	
	public static WioLogProtoData getInstance() {
		return inst;
	}

	@Override
	public void messageConstructor() {
		wioLog = WIOLOG.newBuilder();
	}

	@Override
	public GeneratedMessageV3 serialize() {
		return wioLog.build();
	}

	@Override
	public WIOLOG deSerialize(byte[] input) {
		
		return getWioLog(input);
	}
	
	public String deSerializeToString(byte[] input) {
		return getWioLog(input).toString();
	}
	
	private WIOLOG getWioLog(byte[] input) {
		
		WIOLOG wioLog = null;
		try {
			if(input != null && input.length > 0) {
				wioLog = WIOLOG.parseFrom(input);
			}else {
				System.out.println("Accepts only byte array input");
			}
		} catch (InvalidProtocolBufferException e) {
			System.err.println("Exception in parsing..");
			e.printStackTrace();
		}
		
		return wioLog;
	}
	
	/**
	 * This method will take all the required fields as input to construct the message.
	 * 
	 * {#message ConfigUpdates}
	 * 
	 */
	public void setConfigUpdateDetails(String action, String time,
			String deviceId, String logFile, String log) {
		
		if(!RequestValidator.isEmptyField(action)) {
			wioLog = wioLog.setAction(action);
		}
		if(!RequestValidator.isEmptyField(time)) {
			wioLog = wioLog.setTime(time);
		}
		if(!RequestValidator.isEmptyField(deviceId)) {
			wioLog = wioLog.setDeviceid(deviceId);
		}
		if(!RequestValidator.isEmptyField(logFile)) {
			wioLog = wioLog.setLogfile(logFile);
		}
		if(!RequestValidator.isEmptyField(log)) {
			wioLog = wioLog.setLog(log);
		}
		//System.out.println("=========== >> "+wioLog.getAllFields());
	}

	
	public ByteString getWioLogByteString() {
		return wioLog.build().toByteString();
	}

	@Override
	public String description() {
		return "Google ProtoBuf";
	}

	@Override
	public String schema() {
		return "wioCOnfigUpdates.proto";
	}


}
