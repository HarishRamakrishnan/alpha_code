package com.wio.common.protobuf.framework;

public interface IMessage {
	
	/**
	 * 1. get instance of protobuf 
	 * 2. construct protobuf messages
	 * 3. serialize messages
	 * 4. deserialize messages
	 * @param <T>
	 * 
	 */
	
	public void messageConstructor();
	
	public Object serialize();
	
	public Object deSerialize(byte[] input);
	
	public String description();
	
	public String schema();
	

}
