package com.wio.common.protobuf.framework;

import com.wio.common.protobuf.impl.ConfigProtoData;
import com.wio.common.protobuf.impl.WIOConfigData;
import com.wio.common.protobuf.impl.WioLogProtoData;


public class ProtoBufferMessage implements IMessageSerializer{

	public static final String CONFIG_UPDATE_PROTO = "CONFIG_UPDATE";
	public static final String WIO_LOG_PROTO = "WIO_LOG";
	public static final String WIO_CONFIG_PROTO = "WIO_CONFIG";
	
	public IMessage getMesgSerializer(String type_proto) {
		
		IMessage message = null;
		
		switch(type_proto) {
		
		case "CONFIG_UPDATE":
			message = ConfigProtoData.getInstance();
			
			break;
		case "WIO_LOG":
			message = WioLogProtoData.getInstance();
			break;
		
		case "WIO_CONFIG":
			message = WIOConfigData.getInstance();
			break;
		
		}
		
		return message;
	}

	

}
