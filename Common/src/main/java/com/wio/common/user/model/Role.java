package com.wio.common.user.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * UserRole object model - this class is annotated to be UserRole with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.USER_ROLE)
public class Role 
{	
    private String userRole;
    private int systemUser;
    private String description;
    
    public Role()
    {
    	
    }
    
    @DynamoDBHashKey(attributeName = "USER_ROLE")
    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
    
    @DynamoDBAttribute(attributeName = "SYS_USER")
    public int getSystemUser() {
		return systemUser;
	}

	public void setSystemUser(int systemUser) {
		this.systemUser = systemUser;
	}

	@DynamoDBAttribute(attributeName = "description")
    public String getDescription() {
        return description;
    }
    
		public void setDescription(String description) {
        this.description = description;
    }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((description == null) ? 0 : description.hashCode());
			result = prime * result + ((userRole == null) ? 0 : userRole.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Role other = (Role) obj;
			if (description == null) {
				if (other.description != null)
					return false;
			} else if (!description.equals(other.description))
				return false;
			if (userRole == null) {
				if (other.userRole != null)
					return false;
			} else if (!userRole.equals(other.userRole))
				return false;
			return true;
		}

		
  
}