package com.wio.common.user.model;

import java.nio.ByteBuffer;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;


/**
 * User object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.USER)
public class User 
{
	private String userID;
    private String username;
    private ByteBuffer password;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    
    private String serviceName;
    private String userRole;
    private String accountStatus;
    
    private boolean needPasswordChange;
    private ByteBuffer salt;    
    private String currentPassword;
    private String newPassword;    
    private int maxLoginAttempts;
    private int failedLoginAttempts; 
    
    private Boolean authenticatedUser = false;
	private Boolean authorisedUser = false;
    
    
    private String securityToken;
    private String tokenStartTime;
    private String tokenEndTime;
    
    private String lastLogin;
    private String currentLogin;
    private String creationTime;
    private String createdBy;
    private String lastUpdatedTime;   

    public User() {

    }
    
    @DynamoDBHashKey(attributeName = "USER_ID")
    public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	
	@DynamoDBAttribute(attributeName = "USER_NAME")
    public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
    	
    @DynamoDBAttribute(attributeName = "PASSWORD")
    public ByteBuffer getPassword() {
        return password;
    }

   	public void setPassword(ByteBuffer password) {
        this.password = password;
    }

    @DynamoDBAttribute(attributeName = "PASSWORD_SALT")
    public ByteBuffer getSalt() {
        return salt;
    }

    public void setSalt(ByteBuffer salt) {
        this.salt = salt;
    }


    @DynamoDBIgnore
    public byte[] getPasswordBytes() {
        return password.array();
    }

    @DynamoDBIgnore
    public byte[] getSaltBytes() {
        return salt.array();
    }

    @DynamoDBAttribute(attributeName = "FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@DynamoDBAttribute(attributeName = "LAST_NAME")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@DynamoDBAttribute(attributeName = "EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@DynamoDBAttribute(attributeName = "PHONE_NUMBER")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@DynamoDBAttribute(attributeName = "LAST_LOGIN")
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}


	@DynamoDBAttribute(attributeName = "MAX_LOGIN_ATTEMPTS")
	public int getMaxLoginAttempts() {
		return maxLoginAttempts;
	}

	public void setMaxLoginAttempts(int maxLoginAttempts) {
		this.maxLoginAttempts = maxLoginAttempts;
	}

	@DynamoDBAttribute(attributeName = "FAILED_LOGIN_ATTEMPTS")
	public int getFailedLoginAttempts() {
		return failedLoginAttempts;
	}

	public void setFailedLoginAttempts(int failedLoginAttempts) {
		this.failedLoginAttempts = failedLoginAttempts;
	}

	@DynamoDBAttribute(attributeName = "NEED_PASSWORD_CHANGE")
	public boolean isNeedPasswordChange() {
		return needPasswordChange;
	}

	public void setNeedPasswordChange(boolean needPasswordChange) {
		this.needPasswordChange = needPasswordChange;
	}
	
	@DynamoDBAttribute(attributeName = "USER_ROLE")
	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	
	@DynamoDBIgnore
	public String getCurrentPassword() {
		return currentPassword;
	}	

	@DynamoDBIgnore
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	@DynamoDBIgnore
	public String getNewPassword() {
		return newPassword;
	}

	@DynamoDBIgnore
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	
	@DynamoDBAttribute(attributeName = "ACCOUNT_STATUS")
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	
	@DynamoDBAttribute(attributeName = "CURRENT_LOGIN")
	public String getCurrentLogin() {
		return currentLogin;
	}
	public void setCurrentLogin(String currentLogin) {
		this.currentLogin = currentLogin;
	}

	
	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	@DynamoDBAttribute(attributeName = "LAST_UPDATED_TIME")
	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	
	@DynamoDBAttribute(attributeName = "SECURITY_TOKEN")
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	
	@DynamoDBAttribute(attributeName = "TOKEN_START_TIME")
	public String getTokenStartTime() {
		return tokenStartTime;
	}
	public void setTokenStartTime(String tokenStartTime) {
		this.tokenStartTime = tokenStartTime;
	}

	
	@DynamoDBAttribute(attributeName = "TOKEN_END_TIME")
	public String getTokenEndTime() {
		return tokenEndTime;
	}
	public void setTokenEndTime(String tokenEndTime) {
		this.tokenEndTime = tokenEndTime;
	}
	
	@DynamoDBIgnore
	public Boolean getAuthenticatedUser() {
		return authenticatedUser;
	}   
	public void setAuthenticatedUser(boolean value) {
		this.authenticatedUser = value;
	}
   
    @DynamoDBIgnore
	public Boolean getAuthorizedUser() {
		return authorisedUser;
	}   
	public void setAuthorizedUser(boolean value) {
		this.authorisedUser = value;
	}
}
