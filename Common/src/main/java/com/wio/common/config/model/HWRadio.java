package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

/**
 * HardwareProfile object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBDocument
public class HWRadio {
	
	private String radioName;
	private String radioBandSupported;
	private String radioModeSupported;
	private String bandwidthSupported;
	private String mimoMode;
	private String dfsMode;
	private String dbdcSupported;
	private String dbdcModeSupported;
	private String spatialStream;
		
	
	public HWRadio() {

    }

	
	
    @DynamoDBHashKey(attributeName = "RADIO_NAME")
    public String getRadioName() {
		return radioName;
	}


	public void setRadioName(String radioName) {
		this.radioName = radioName;
	}


	@DynamoDBAttribute(attributeName = "RADIO_BAND_SUPPORTED")
	public String getRadioBandSupported() {
		return radioBandSupported;
	}


	public void setRadioBandSupported(String radioBandSupported) {
		this.radioBandSupported = radioBandSupported;
	}


	@DynamoDBAttribute(attributeName = "RADIO_MODE_SUPPORTED")
	public String getRadioModeSupported() {
		return radioModeSupported;
	}


	public void setRadioModeSupported(String radioModeSupported) {
		this.radioModeSupported = radioModeSupported;
	}

	@DynamoDBAttribute(attributeName = "BANDWIDTH_SUPPORTED")
	public String getBandwidthSupported() {
		return bandwidthSupported;
	}


	public void setBandwidthSupported(String bandwidthSupported) {
		this.bandwidthSupported = bandwidthSupported;
	}

	@DynamoDBAttribute(attributeName = "MIMO_MODE")
	public String getMimoMode() {
		return mimoMode;
	}


	public void setMimoMode(String mimoMode) {
		this.mimoMode = mimoMode;
	}

	@DynamoDBAttribute(attributeName = "DFS_MODE")
	public String getDfsMode() {
		return dfsMode;
	}


	public void setDfsMode(String dfsMode) {
		this.dfsMode = dfsMode;
	}

	@DynamoDBAttribute(attributeName = "DBDC_SUPPORTED")
	public String getDbdcSupported() {
		return dbdcSupported;
	}
	public void setDbdcSupported(String dbdcSupported) {
		this.dbdcSupported = dbdcSupported;
	}
	

	@DynamoDBAttribute(attributeName = "DBDC_MODE_SUPPORTED")
	public String getDbdcModeSupported() {
		return dbdcModeSupported;
	}
	public void setDbdcModeSupported(String dbdcModeSupported) {
		this.dbdcModeSupported = dbdcModeSupported;
	}


	@DynamoDBAttribute(attributeName = "SPATIAL_STREAM")
	public String getSpatialStream() {
		return spatialStream;
	}

	public void setSpatialStream(String spatialStream) {
		this.spatialStream = spatialStream;
	}

	@Override
	public String toString() {
		return "HWRadio [radioName=" + radioName + ", radioBandSupported=" + radioBandSupported
				+ ", radioModeSupported=" + radioModeSupported + ", bandwidthSupported=" + bandwidthSupported
				+ ", mimoMode=" + mimoMode + ", dfsMode=" + dfsMode + ", dbdcSupported=" + dbdcSupported
				+ ", dbdcModeSupported=" + dbdcModeSupported + ", spatialStream=" + spatialStream + "]";
	}
	
}
