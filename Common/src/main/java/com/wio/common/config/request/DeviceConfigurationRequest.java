package com.wio.common.config.request;

import java.util.List;

import com.wio.common.config.model.HPRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.PortSettings;
import com.wio.common.config.model.WANSettings;

public class DeviceConfigurationRequest 
{
	private String deviceID;
	private String deviceName;
	private String serviceName;
	private String model;	
	private String mac;
	private String networkAdmin;
	private String defaultTopic;
	private Boolean generateKeyForZT;
	
	private List<HPRadioSettings> radioSettings;
	private List<PortSettings> portSettings;
	private List<LANSettings> lanSettings;
	private WANSettings wanSettings;	
	private List<WiFiSettings> wifiSettings;	
	
	private DeviceFirmware firmware;

	
	
	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getNetworkAdmin() {
		return networkAdmin;
	}

	public void setNetworkAdmin(String networkAdmin) {
		this.networkAdmin = networkAdmin;
	}

	public String getDefaultTopic() {
		return defaultTopic;
	}

	public void setDefaultTopic(String defaultTopic) {
		this.defaultTopic = defaultTopic;
	}

	public Boolean getGenerateKeyForZT() {
		return generateKeyForZT;
	}

	public void setGenerateKeyForZT(Boolean generateKeyForZT) {
		this.generateKeyForZT = generateKeyForZT;
	}

	public List<HPRadioSettings> getRadioSettings() {
		return radioSettings;
	}

	public void setRadioSettings(List<HPRadioSettings> radioSettings) {
		this.radioSettings = radioSettings;
	}

	public List<PortSettings> getPortSettings() {
		return portSettings;
	}

	public void setPortSettings(List<PortSettings> portSettings) {
		this.portSettings = portSettings;
	}

	public List<LANSettings> getLanSettings() {
		return lanSettings;
	}

	public void setLanSettings(List<LANSettings> lanSettings) {
		this.lanSettings = lanSettings;
	}

	public WANSettings getWanSettings() {
		return wanSettings;
	}

	public void setWanSettings(WANSettings wanSettings) {
		this.wanSettings = wanSettings;
	}

	public DeviceFirmware getFirmware() {
		return firmware;
	}

	public void setFirmware(DeviceFirmware firmware) {
		this.firmware = firmware;
	}

	public List<WiFiSettings> getWifiSettings() {
		return wifiSettings;
	}

	public void setWifiSettings(List<WiFiSettings> wifiSettings) {
		this.wifiSettings = wifiSettings;
	}

	
}
