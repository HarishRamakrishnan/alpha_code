package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBDocument
public class PortSettings 
{	
	private String lanInterface;
	private int portNo;
	private String portType;
	private String babel;
	
   public PortSettings() 
   {

   }
   

	@DynamoDBHashKey(attributeName = "PORT_NO")
	public int getPortNo() {
		return portNo;
	}	
	public void setPortNo(int portNo) {
		this.portNo = portNo;
	}
	
	
    @DynamoDBAttribute(attributeName = "LAN_INTERFACE")
	public String getLanInterface() {
		return lanInterface;
	}	
	public void setLanInterface(String lanInterface) {
		this.lanInterface = lanInterface;
	}
	

	@DynamoDBAttribute(attributeName = "PORT_TYPE")
	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}
	
	
	@DynamoDBAttribute(attributeName = "BABEL")
	public String getBabel() {
		return babel;
	}

	public void setBabel(String babel) {
		this.babel = babel;
	}

}
