package com.wio.common.config.request;

public class PublishingTopics 
{
	private String config;
	private String log;
	private String auth;
	private String eventsArp;
	private String eventsDhcp;
	private String eventsHostapd3;
	private String rfMetric;
	private String rfMgmt;
	private String confresp;
	
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getAuth() {
		return auth;
	}
	public String getEventsArp() {
		return eventsArp;
	}
	public void setEventsArp(String eventsArp) {
		this.eventsArp = eventsArp;
	}
	public String getEventsDhcp() {
		return eventsDhcp;
	}
	public void setEventsDhcp(String eventsDhcp) {
		this.eventsDhcp = eventsDhcp;
	}
	public String getEventsHostapd3() {
		return eventsHostapd3;
	}
	public void setEventsHostapd3(String eventsHostapd3) {
		this.eventsHostapd3 = eventsHostapd3;
	}
	public String getRfMetric() {
		return rfMetric;
	}
	public void setRfMetric(String rfMetric) {
		this.rfMetric = rfMetric;
	}
	public String getRfMgmt() {
		return rfMgmt;
	}
	public void setRfMgmt(String rfMgmt) {
		this.rfMgmt = rfMgmt;
	}
	public String getConfresp() {
		return confresp;
	}
	public void setConfresp(String confresp) {
		this.confresp = confresp;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	
	@Override
	public String toString() {
		return "PublishingTopics [config=" + config +  ", log=" + log +  ", auth="
				+ auth + ", eventsArp=" + eventsArp + ", eventsDhcp=" + eventsDhcp + ", eventsHostapd3="
				+ eventsHostapd3 + ", rfMetric=" + rfMetric + ", rfMgmt=" + rfMgmt + ", confresp=" + confresp + "]";
	}
	
}
