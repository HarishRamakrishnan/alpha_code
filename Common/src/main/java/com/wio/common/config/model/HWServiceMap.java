package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.HW_SERVICE_MAP)

public class HWServiceMap
{	
	private String hwID;
	private String serviceName;
	private String description;
	
	public HWServiceMap() {
		// TODO Auto-generated constructor stub
	
}
	@DynamoDBHashKey(attributeName = "HW_ID")
	public String getHwID() {
		return hwID;
	}
	public void setHwID(String hwID) {
		this.hwID = hwID;
	}
	

	@DynamoDBRangeKey(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}