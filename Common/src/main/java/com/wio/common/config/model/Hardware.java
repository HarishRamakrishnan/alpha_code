package com.wio.common.config.model;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * HardwareProfile object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.HARDWARE)
public class Hardware 
{
	private String hwID;	
	private String hwName;
	private String manufacture;
	private String chipsetVendor;
	private String chipsetType;
	private String cpuCore;
	private String cpuType;
	private String cpuFrequency;
	private String natOffloading;
	private String dramController;
	private String totalMemory;
	private String freeMemory;
		
	private Integer lanPortSupported;
	private String lanInterfaceSupported;
	private String connector;
	
	private Integer usbPortSupported;
	private String usbTypeSupported;
	private String usbSpeedSupported;	
	
	private String powerAdapter;
	private String visualIndicatorSupported;
	private String dbdcSupported;
	private String hwType;
	
	private List<HWRadio> hwRadio;

	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;
	
	public Hardware() {

    }

	@DynamoDBHashKey(attributeName = "HW_ID")
	public String getHwID() {
		return hwID;
	}

	public void setHwID(String hwID) {
		this.hwID = hwID;
	}
	
	
	@DynamoDBAttribute(attributeName = "HW_NAME")
    public String getHwName() {
		return hwName;
	}

	public void setHwName(String hwName) {
		this.hwName = hwName;
	}

	@DynamoDBAttribute(attributeName = "MANUFACTURE")
	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	@DynamoDBAttribute(attributeName = "CHIPSET_VENDOR")
	public String getChipsetVendor() {
		return chipsetVendor;
	}

	public void setChipsetVendor(String chipsetVendor) {
		this.chipsetVendor = chipsetVendor;
	}

	@DynamoDBAttribute(attributeName = "CHIPSET_TYPE")
	public String getChipsetType() {
		return chipsetType;
	}

	public void setChipsetType(String chipsetType) {
		this.chipsetType = chipsetType;
	}

	@DynamoDBAttribute(attributeName = "CPU_CORE")
	public String getCpuCore() {
		return cpuCore;
	}

	public void setCpuCore(String cpuCore) {
		this.cpuCore = cpuCore;
	}
	
	@DynamoDBAttribute(attributeName = "CPU_TYPE")
	public String getCpuType() {
		return cpuType;
	}

	public void setCpuType(String cpuType) {
		this.cpuType = cpuType;
	}

	@DynamoDBAttribute(attributeName = "CPU_FREQUENCY")
	public String getCpuFrequency() {
		return cpuFrequency;
	}

	public void setCpuFrequency(String cpuFrequency) {
		this.cpuFrequency = cpuFrequency;
	}

	@DynamoDBAttribute(attributeName = "NAT_OFFLOADING")
	public String getNatOffloading() {
		return natOffloading;
	}

	
	public void setNatOffloading(String natOffloading) {
		this.natOffloading = natOffloading;
	}

	@DynamoDBAttribute(attributeName = "DRAM_CONTROLLER")
	public String getDramController() {
		return dramController;
	}

	
	public void setDramController(String dramController) {
		this.dramController = dramController;
	}

	@DynamoDBAttribute(attributeName = "TOTAL_MEMORY")
	public String getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(String totalMemory) {
		this.totalMemory = totalMemory;
	}

	@DynamoDBAttribute(attributeName = "FREE_MEMORY")
	public String getFreeMemory() {
		return freeMemory;
	}

	public void setFreeMemory(String freeMemory) {
		this.freeMemory = freeMemory;
	}

	@DynamoDBAttribute(attributeName = "LAN_CONNECTION_SUPPORTED")
	public Integer getLanPortSupported() {
		return lanPortSupported;
	}

	public void setLanPortSupported(Integer lanPortSupported) {
		this.lanPortSupported = lanPortSupported;
	}

	@DynamoDBAttribute(attributeName = "LAN_INTERFACE_SUPPORTED")
	public String getLanInterfaceSupported() {
		return lanInterfaceSupported;
	}

	public void setLanInterfaceSupported(String lanInterfaceSupported) {
		this.lanInterfaceSupported = lanInterfaceSupported;
	}

	@DynamoDBAttribute(attributeName = "LAN_CONNECTOR")
	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	@DynamoDBAttribute(attributeName = "USB_PORT_SUPPORTED")
	public Integer getUsbPortSupported() {
		return usbPortSupported;
	}

	public void setUsbPortSupported(Integer usbPortSupported) {
		this.usbPortSupported = usbPortSupported;
	}

	@DynamoDBAttribute(attributeName = "USB_TYPE_SUPPORTED")
	public String getUsbTypeSupported() {
		return usbTypeSupported;
	}

	public void setUsbTypeSupported(String usbTypeSupported) {
		this.usbTypeSupported = usbTypeSupported;
	}

	@DynamoDBAttribute(attributeName = "USB_SPEED_SUPPORTED")
	public String getUsbSpeedSupported() {
		return usbSpeedSupported;
	}

	public void setUsbSpeedSupported(String usbSpeedSupported) {
		this.usbSpeedSupported = usbSpeedSupported;
	}

	@DynamoDBAttribute(attributeName = "POWER_ADAPTER")
	public String getPowerAdapter() {
		return powerAdapter;
	}

	public void setPowerAdapter(String powerAdapter) {
		this.powerAdapter = powerAdapter;
	}

	@DynamoDBAttribute(attributeName = "VISUAL_INDICATOR_SUPPORTED")
	public String getVisualIndicatorSupported() {
		return visualIndicatorSupported;
	}

	public void setVisualIndicatorSupported(String visualIndicatorSupported) {
		this.visualIndicatorSupported = visualIndicatorSupported;
	}

	
	@DynamoDBAttribute(attributeName = "RADIOS_SUPPORTED")
	public List<HWRadio> getHwRadio() {
		return hwRadio;
	}
	public void setHwRadio(List<HWRadio> hwRadio) {
		this.hwRadio = hwRadio;
	}


	@DynamoDBAttribute(attributeName = "DBDC_SUPPORTED")
	public String getDbdcSupported() {
		return dbdcSupported;
	}
	public void setDbdcSupported(String dbdcSupported) {
		this.dbdcSupported = dbdcSupported;
	}

	@DynamoDBAttribute(attributeName = "HW_TYPE")
	public String getHwType() {
		return hwType;
	}

	public void setHwType(String hwType) {
		this.hwType = hwType;
	}
	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	

	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	@DynamoDBAttribute(attributeName = "LAST_UPDATE_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	
}
