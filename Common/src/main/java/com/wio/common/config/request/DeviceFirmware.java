package com.wio.common.config.request;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

@DynamoDBDocument
public class DeviceFirmware 
{
	private String fwID;
	private String filename;
	private String version;
	private String versionType;
	private String checksumType;
	private String checksum;
	private String edgeURL;
	
	private String uploadDate;

	@DynamoDBHashKey(attributeName = "FW_ID")
	public String getFwID() {
		return fwID;
	}

	public void setFwID(String fwID) {
		this.fwID = fwID;
	}

	@DynamoDBAttribute(attributeName = "FILE_NAME")
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@DynamoDBAttribute(attributeName = "VERSION")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@DynamoDBAttribute(attributeName = "VERSION_TYPE")
	public String getVersionType() {
		return versionType;
	}

	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}

	@DynamoDBAttribute(attributeName = "CHECKSUM_TYPE")
	public String getChecksumType() {
		return checksumType;
	}

	public void setChecksumType(String checksumType) {
		this.checksumType = checksumType;
	}

	@DynamoDBAttribute(attributeName = "CHECKSUM")
	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	@DynamoDBAttribute(attributeName = "EDGE_URL")
	public String getEdgeURL() {
		return edgeURL;
	}

	public void setEdgeURL(String edgeURL) {
		this.edgeURL = edgeURL;
	}

	@DynamoDBAttribute(attributeName = "UPLOAD_DATE")
	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	
}
