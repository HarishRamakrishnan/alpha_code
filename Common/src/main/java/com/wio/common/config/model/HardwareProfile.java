package com.wio.common.config.model;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * APProfile object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.HARDWARE_PROFILE)
public class HardwareProfile 
{
	private String hpID;
	private String hpName;
	private String hpTopic;
	private String hwID;
	private String serviceName;
	private String model;
	
	private List<HPRadioSettings> radioSettings;	
	private List<PortSettings> portSettings;	

	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;
	
	public HardwareProfile() {

    }
	
	
	@DynamoDBHashKey(attributeName = "HP_ID")
	public String getHpID() {
		return hpID;
	}

	public void setHpID(String hpID) {
		this.hpID = hpID;
	}

	@DynamoDBAttribute(attributeName = "HW_ID")
	public String getHwID() {
		return hwID;
	}

	public void setHwID(String hwID) {
		this.hwID = hwID;
	}

	@DynamoDBAttribute(attributeName = "MODEL")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}	
	
	@DynamoDBAttribute(attributeName = "HP_NAME")
	public String getHpName() {
		return hpName;
	}

	public void setHpName(String hpName) {
		this.hpName = hpName;
	}
	
	@DynamoDBAttribute(attributeName = "HP_TOPIC")
	public String getHpTopic() {
		return hpTopic;
	}
	public void setHpTopic(String hpTopic) {
		this.hpTopic = hpTopic;
	}
	

	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@DynamoDBAttribute(attributeName = "RADIO_SETTINGS")
	public List<HPRadioSettings> getRadioSettings() {
		return radioSettings;
	}

	public void setRadioSettings(List<HPRadioSettings> radioSettings) {
		this.radioSettings = radioSettings;
	}

	@DynamoDBAttribute(attributeName="PORT_SETTINGS")
	public List<PortSettings> getPortSettings() {
		return portSettings;
	}

	public void setPortSettings(List<PortSettings> portSettings) {
		this.portSettings = portSettings;
	}
	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@DynamoDBAttribute(attributeName="CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}


	@DynamoDBAttribute(attributeName="LAST_UPDATE_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	
}
