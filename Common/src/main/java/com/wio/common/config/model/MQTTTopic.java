package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.MQTT_TOPIC)
public class MQTTTopic 
{
	private String topic;
	private String publisher;
	private String subscriber;
	private String description;
	private String path;
	private int mqttQoS;
	
	
	@DynamoDBHashKey(attributeName = "TOPIC")
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	@DynamoDBAttribute(attributeName = "PUBLISHER")
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	@DynamoDBAttribute(attributeName = "SUBSCRIBER")
	public String getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}
	
	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@DynamoDBAttribute(attributeName = "PATH")
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	@DynamoDBAttribute(attributeName = "QOS")
	public int getMqttQoS() {
		return mqttQoS;
	}
	public void setMqttQoS(int mqttQoS) {
		this.mqttQoS = mqttQoS;
	}
	
		
}
