package com.wio.common.config.request;

public class ServiceRequest {

	private String serviceName;
	private String description;
    private String email;
    private String address;
	private String city;
	private String pincode;
	private String country;
	private String acsURL;
	private String acsUsername;
	private String acsPassword;
	private String cpeGatewayURL;
	private String cpeUsername;
	private String cpePassword;
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAcsURL() {
		return acsURL;
	}
	public void setAcsURL(String acsURL) {
		this.acsURL = acsURL;
	}
	public String getAcsUsername() {
		return acsUsername;
	}
	public void setAcsUsername(String acsUsername) {
		this.acsUsername = acsUsername;
	}
	public String getAcsPassword() {
		return acsPassword;
	}
	public void setAcsPassword(String acsPassword) {
		this.acsPassword = acsPassword;
	}
	public String getCpeGatewayURL() {
		return cpeGatewayURL;
	}
	public void setCpeGatewayURL(String cpeGatewayURL) {
		this.cpeGatewayURL = cpeGatewayURL;
	}
	public String getCpeUsername() {
		return cpeUsername;
	}
	public void setCpeUsername(String cpeUsername) {
		this.cpeUsername = cpeUsername;
	}
	public String getCpePassword() {
		return cpePassword;
	}
	public void setCpePassword(String cpePassword) {
		this.cpePassword = cpePassword;
	}

	
	
	
}
