package com.wio.common.config.model;

import java.nio.ByteBuffer;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.SERVICE)
public class Service 
{	
	private String serviceName;
	private String description;
    private String email;
    private String address;
	private String city;
	private String pincode;
	private String country;
	private String acsURL;
	private String acsUsername;
	private ByteBuffer acsPassword;
	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;	
	private String cpeGatewayURL;
	private String cpeUsername;
	private ByteBuffer cpePassword;
	@JsonIgnore
	private ByteBuffer acsPwdSalt;
	@JsonIgnore
	private ByteBuffer cpePwdSalt;
    
    public Service() {

    }


    @DynamoDBHashKey(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	

    @DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

	@DynamoDBAttribute(attributeName = "EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	@DynamoDBAttribute(attributeName = "ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	
	@DynamoDBAttribute(attributeName = "CITY")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	
	@DynamoDBAttribute(attributeName = "PINCODE")
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	
	@DynamoDBAttribute(attributeName = "COUNTRY")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	
	@DynamoDBAttribute(attributeName = "ACS_URL")
	public String getAcsURL() {
		return acsURL;
	}
	public void setAcsURL(String acsURL) {
		this.acsURL = acsURL;
	}

	
	@DynamoDBAttribute(attributeName = "ACS_USERNAME")
	public String getAcsUsername() {
		return acsUsername;
	}
	public void setAcsUsername(String acsUsername) {
		this.acsUsername = acsUsername;
	}

	
	@DynamoDBAttribute(attributeName = "ACS_PASSWORD")
	public ByteBuffer getAcsPassword() {
		return acsPassword;
	}
	public void setAcsPassword(ByteBuffer acsPassword) {
		this.acsPassword = acsPassword;
	}

	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	
	@DynamoDBAttribute(attributeName = "LAST_UPDATED_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
    
	@DynamoDBAttribute(attributeName = "CPE_GATEWAY_URL")
	public String getCpeGatewayURL() {
		return cpeGatewayURL;
	}
	public void setCpeGatewayURL(String cpeGatewayURL) {
		this.cpeGatewayURL = cpeGatewayURL;
	}

	@DynamoDBAttribute(attributeName = "CPE_USERNAME")
	public String getCpeUsername() {
		return cpeUsername;
	}
	public void setCpeUsername(String cpeUsername) {
		this.cpeUsername = cpeUsername;
	}

	@DynamoDBAttribute(attributeName = "CPE_PASSWORD")
	public ByteBuffer getCpePassword() {
		return cpePassword;
	}
	public void setCpePassword(ByteBuffer cpePassword) {
		this.cpePassword = cpePassword;
	}

	@DynamoDBAttribute(attributeName = "ACS_PWD_SALT")
	public ByteBuffer getAcsPwdSalt() {
		return acsPwdSalt;
	}

	public void setAcsPwdSalt(ByteBuffer acsPwdSalt) {
		this.acsPwdSalt = acsPwdSalt;
	}

	@DynamoDBAttribute(attributeName = "CPE_PWD_SALT")
	public ByteBuffer getCpePwdSalt() {
		return cpePwdSalt;
	}

	public void setCpePwdSalt(ByteBuffer cpePwdSalt) {
		this.cpePwdSalt = cpePwdSalt;
	}
	
}
