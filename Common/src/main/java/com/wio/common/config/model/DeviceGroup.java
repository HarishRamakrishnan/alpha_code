package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.DEVICE_GROUP)
public class DeviceGroup 
{
	private String dgID;
	private String dgName;
	private String dgType;
	private int level;
	private String parentDG;
	private String serviceName;
	private String networkID;
	private String mqttTopic;
	private String createdBy;
	private String creationTime;
	private String lastUpdateTime;	
		

	public DeviceGroup() {

    }


    @DynamoDBHashKey(attributeName = "DG_ID")
	public String getDgID() {
		return dgID;
	}
	public void setDgID(String dgID) {
		this.dgID = dgID;
	}

	
	@DynamoDBAttribute(attributeName = "LEVEL")
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
	
    @DynamoDBAttribute(attributeName = "DG_NAME")
	public String getDgName() {
		return dgName;
	}
	public void setDgName(String dgName) {
		this.dgName = dgName;
	}


	@DynamoDBAttribute(attributeName = "DG_TYPE")
	public String getDgType() {
		return dgType;
	}
	public void setDgType(String dgType) {
		this.dgType = dgType;
	}


	@DynamoDBAttribute(attributeName = "PARENT_DG")
	public String getParentDG() {
		return parentDG;
	}
	public void setParentDG(String parentDG) {
		this.parentDG = parentDG;
	}


	@DynamoDBAttribute(attributeName = "SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	@DynamoDBAttribute(attributeName = "NETWORK_ID")
	public String getNetworkID() {
		return networkID;
	}
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}


	@DynamoDBAttribute(attributeName = "MQTT_TOPIC")
	public String getMqttTopic() {
		return mqttTopic;
	}
	public void setMqttTopic(String mqttTopic) {
		this.mqttTopic = mqttTopic;
	}
	
	@DynamoDBAttribute(attributeName = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}


	@DynamoDBAttribute(attributeName = "LAST_UPDATED_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
    
}
