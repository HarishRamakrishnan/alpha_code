package com.wio.common.config.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

@DynamoDBTable(tableName = DynamoDBObject.DG_WIFI_MAP)
public class DGWifiMap
{	
	private String dgID;
	private String deviceID;
	private String wifiID;
	private String description;
	
	public DGWifiMap() {
	}
	
	@DynamoDBHashKey(attributeName = "DG_ID")
	public String getDgID() {
		return dgID;
	}
	public void setDgID(String dgID) {
		this.dgID = dgID;
	}
	
	@DynamoDBRangeKey(attributeName = "WIFI_ID")
	public String getWifiID() {
		return wifiID;
	}
	public void setWifiID(String wifiID) {
		this.wifiID = wifiID;
	}

	@DynamoDBAttribute(attributeName = "DESCRIPTION")
	public String getDescription() {
		return description;
	}	
	public void setDescription(String description) {
		this.description = description;
	}

	@DynamoDBAttribute(attributeName = "DEVICE_ID")
	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
}