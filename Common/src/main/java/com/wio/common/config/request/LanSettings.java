package com.wio.common.config.request;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

@DynamoDBDocument
public class LanSettings {
	private String portID;
	private String dhcp; 
	private String ipV4;
	private String ipV6;
	private String gateway;
	private String subnetMask;
	private String dnsPrimary;
	private String dnsSecondary;
	private String ifName;
	private String name;
	private String portNo;
	private String vlanNo;
	private String index;
	   
	private NetworkSettings networkSettings;

	public LanSettings() {
    }

	@DynamoDBHashKey(attributeName = "PORT_ID")
	public String getPortID() {
		return portID;
	}
	public void setPortID(String portID) {
		this.portID = portID;
	}
	
	@DynamoDBHashKey(attributeName = "PORT_NO")
	public String getPortNo() {
		return portNo;
	}
	public void setPortNo(String portNo) {
		this.portNo = portNo;
	}

	@DynamoDBAttribute(attributeName = "VLAN_NO")
	public String getVlanNo() {
		return vlanNo;
	}
	public void setVlanNo(String vlanNo) {
		this.vlanNo = vlanNo;
	}


	@DynamoDBAttribute(attributeName = "IPV4")
	public String getIpV4() {
		return ipV4;
	}
	public void setIpV4(String ipV4) {
		this.ipV4 = ipV4;
	}


	@DynamoDBAttribute(attributeName = "IPV6")
	public String getIpV6() {
		return ipV6;
	}
	public void setIpV6(String ipV6) {
		this.ipV6 = ipV6;
	}


	@DynamoDBAttribute(attributeName = "GATEWAY")
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}


	@DynamoDBAttribute(attributeName = "SUBNET_MASK")
	public String getSubnetMask() {
		return subnetMask;
	}
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}


	@DynamoDBAttribute(attributeName = "DNS_PRIMARY")
	public String getDnsPrimary() {
		return dnsPrimary;
	}
	public void setDnsPrimary(String dnsPrimary) {
		this.dnsPrimary = dnsPrimary;
	}


	@DynamoDBAttribute(attributeName = "DNS_SECONDARY")
	public String getDnsSecondary() {
		return dnsSecondary;
	}
	public void setDnsSecondary(String dnsSecondary) {
		this.dnsSecondary = dnsSecondary;
	}	
    
	@DynamoDBAttribute(attributeName = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@DynamoDBAttribute(attributeName = "IFNAME")
	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}	
	@DynamoDBAttribute(attributeName = "DHCP")
	public String getDhcp() {
		return dhcp;
	}

	public void setDhcp(String dhcp) {
		this.dhcp = dhcp;
	}

	@DynamoDBAttribute(attributeName = "INDEX")
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	
	@DynamoDBAttribute(attributeName = "NETWORK_SETTINGS")
	public NetworkSettings getNetworkSettings() {
		return networkSettings;
	}
	
	public void setNetworkSettings(NetworkSettings networkSettings) {
		this.networkSettings = networkSettings;
	}

	@Override
	public String toString() {
		return "LanSettings [portID=" + portID + ", dhcp=" + dhcp + ", ipV4=" + ipV4 + ", ipV6=" + ipV6 + ", gateway="
				+ gateway + ", subnetMask=" + subnetMask + ", dnsPrimary=" + dnsPrimary + ", dnsSecondary="
				+ dnsSecondary + ", ifName=" + ifName + ", name=" + name + ", portNo=" + portNo + ", vlanNo=" + vlanNo
				+ ", index=" + index + ", networkSettings=" + networkSettings + "]";
	}
	
    
}
