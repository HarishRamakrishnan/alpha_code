package com.wio.common.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.wio.common.validation.RequestValidator;


public class PasswordHelper {
	
	private static PasswordHelper _inst = new PasswordHelper();
	
	private PasswordHelper() {
	}
	
	public static PasswordHelper getInstance() {
		return _inst;
	}
	
    /**
     * Verifies a login attempt against a password from the data store
     *
     * @param attemptedPassword The unencrypted password used by the user when logging into the service
     * @param encryptedPassword The encrypted password from the data store
     * @param salt              The salt for the encrypted password from the data store
     * @return True if the passwords match, false otherwise.
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public boolean authenticate(String attemptedPassword, byte[] encryptedPassword, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Encrypt the clear-text password using the same salt that was used to
        // encrypt the original password
        byte[] encryptedAttemptedPassword = getEncryptedPassword(attemptedPassword, salt);

        // Authentication succeeds if encrypted password that the user entered
        // is equal to the stored hash
        return Arrays.equals(encryptedPassword, encryptedAttemptedPassword);
    }

    /**
     * Encrypts the given password with a salt
     *
     * @param password The password string to be encrypted
     * @param salt     A randomly generated salt for the password encryption
     * @return The byte[] containing the encrypted password
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public byte[] getEncryptedPassword(String password, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        String algorithm = "PBKDF2WithHmacSHA1";
        int derivedKeyLength = 160;
        // The NIST recommends at least 1,000 iterations:
        // http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
        int iterations = 20000;

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);

        SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

        return f.generateSecret(spec).getEncoded();
    }

    /**
     * Generates a random 8 byte salt using the SecureRandom with the SHA1PRNG
     *
     * @return The generated salt
     * @throws NoSuchAlgorithmException
     */
    public byte[] generateSalt() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

        byte[] salt = new byte[8];
        random.nextBytes(salt);

        return salt;
    }
    
    public Map<String, byte[]> getEncPassword(String inpPassword) {
    	byte[] salt = null;
    	byte[] pwd  = null;
    	Map<String, byte[]> saltPWDMap = new HashMap<>();
    	if(RequestValidator.isValidString(inpPassword)) {
			try {
				salt = generateSalt();
			} catch (NoSuchAlgorithmException e) {
				System.out.println("Algorithim not provided to generate salt. Cause - "+e.getMessage());
			}
			if(salt != null && salt.length>0) {
				//input.setAcsPwdSalt(new String (acsSalt));
				saltPWDMap.put("SALT", salt);
				pwd = getEncPwd(inpPassword, salt);
				if(pwd != null && pwd.length > 0)
					saltPWDMap.put("ENC_PWD", pwd);
					
			}
        }
		return saltPWDMap;
    }
    
    /**
     * This method will generate encrypted password.
     * @param string
     * @param salt
     * @return
     */
    private byte[] getEncPwd(String string, byte[] salt) {
    	
    	if(RequestValidator.isValidString(string) && RequestValidator.isValidString(new String(salt))) {
    		try {
				return getEncryptedPassword(string, salt);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				System.out.println(
						"Algorithim not provided to generate Encrypted Password. Cause - "
								+ e.getMessage());
			}
    	}
    	
		return null;
		
	}
    
}
