package com.wio.common.generic.dao;

import java.util.List;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.wio.common.exception.DAOException;
import com.wio.common.staticinfo.SystemConfigMessages;
import com.wio.common.validation.RequestValidator;

public class DDBGenericDAO implements GenericDAO {
	
	private static AmazonDynamoDB ddbClient = AmazonDynamoDBClientBuilder.standard()
			.withRegion(Regions.fromName(SystemConfigMessages.SYSTEM_AWS_REGION)).build();

	private static DDBGenericDAO instance = null;

	private DynamoDB dynamo;

	public static DDBGenericDAO getInstance() {
		if (instance == null) {
			instance = new DDBGenericDAO();
		}
		return instance;
	}

	private DDBGenericDAO() {
		//ddbClient.setRegion(Region.getRegion(Regions.fromName(SystemConfigMessages.SYSTEM_AWS_REGION)));
		// ddbClient.setSignerRegionOverride(SystemConfigMessages.SYSTEM_AWS_REGION);
		dynamo = new DynamoDB(ddbClient);
	}

	protected DynamoDBMapper getMapper() {
		return new DynamoDBMapper(ddbClient);
	}

	public DynamoDBMapper dynamoDbMapper(final DynamoDBMapperConfig dynamoDBMapperConfig) {
		return new DynamoDBMapper(ddbClient, dynamoDBMapperConfig);
	}

	public DynamoDBMapperConfig dynamoDBMapperConfig(String tableName, String prefix) {
		return new DynamoDBMapperConfig(new TableNameOverride(tableName).withTableNamePrefix(prefix));
	}

	@Override
	public <T> void saveGenericObjects(List<T> genericObjects) throws DAOException {
		getMapper().batchSave(genericObjects);
	}

	@Override
	public <T> void saveGenericObject(T genericObject) throws DAOException {
		getMapper().save(genericObject);
	}

	@Override
	public <T> void saveGenericObjectWithPrefix(T genericObject, String tableName, String prefix) throws DAOException {

		dynamoDbMapper(dynamoDBMapperConfig(tableName, prefix)).save(genericObject);
	}

	@Override
	public <T> T getGenericObject(Class<T> genericObject, String hashKey) throws DAOException {
		return getMapper().load(genericObject, hashKey);
	}

	@Override
	public <T> T getGenericObject(Class<T> genericObject, String hashKey, String rangeKey) throws DAOException {
		return getMapper().load(genericObject, hashKey, rangeKey);
	}

	@Override
	public <T> List<T> getGenericObjects(Class<T> genericModel, List<FilterCondition> filterConditions)
			throws DAOException {
		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();

		if (null == filterConditions || filterConditions.isEmpty()) {
			System.out.println("No Query Filters Found !");
		} else {
			/*
			 * Map<String, AttributeValue> actuals = new HashMap<String, AttributeValue>();
			 * List<String> values = new ArrayList<String>();
			 * 
			 * for(FilterCondition condition : filterConditions) {
			 * if(condition.getParam().trim() != "" || condition.getParam() != null) {
			 * values.add(condition.getValue());
			 * 
			 * 
			 * actuals.put(condition.getParam(), new AttributeValue().withSS(values)); }
			 * 
			 * }
			 * 
			 * actuals.forEach((key, value)->{
			 * 
			 * });
			 * 
			 * 
			 * 
			 * 
			 * List<AttributeValue> attributeValuesList = new ArrayList<AttributeValue>();
			 * List<String> columns = new ArrayList<String>();
			 * 
			 * for(FilterCondition condition : filterConditions) {
			 * if(condition.getParam().trim() != "" || condition.getParam() != null) {
			 * if(!columns.isEmpty()) { columns.forEach(item->{
			 * if(condition.getParam().equals(item)) {
			 * System.out.println("Repeating column: "+condition.getParam());
			 * attributeValuesList.add(new AttributeValue().withSS(columns)); } else {
			 * 
			 * } }); } else { attributeValuesList.add(new
			 * AttributeValue().withS(condition.getValue()));
			 * 
			 * scanExpression.addFilterCondition(condition.getParam(), new Condition()
			 * .withComparisonOperator(condition.getOperator())
			 * .withAttributeValueList(attributeValuesList)); }
			 * 
			 * columns.add(condition.getParam());
			 * 
			 * 
			 * scanExpression.addFilterCondition(condition.getParam(), new Condition()
			 * .withComparisonOperator(condition.getOperator())
			 * .withAttributeValueList(attributeValuesList));
			 * 
			 * 
			 * 
			 * columns.add(condition.getParam()); } }
			 */

			//////
			System.out.println(filterConditions.size() + " Query filters found");
			for (FilterCondition condition : filterConditions) {
				if (condition.getParam().trim() != "" || condition.getParam() != null) {
					System.out.println("Filter Applied for:: " + condition.getParam() + " = " + condition.getValue());
					scanExpression.addFilterCondition(condition.getParam(),
							new Condition().withComparisonOperator(condition.getOperator())
									.withAttributeValueList(new AttributeValue().withS(condition.getValue())));				
				}
			}

		}

		return (List<T>) getMapper().scan(genericModel, scanExpression);
	}

	@Override
	public <T> void updateGenericObject(Object genericObject) throws DAOException {
		getMapper().save(genericObject);
	}

	@Override
	public <T> void deleteGenericObject(Object genericObject) throws DAOException {
		getMapper().delete(genericObject);
	}

	public Item scanItemFromTable(String tableName, String pkName, Object pkValue) throws DAOException {

		Table table = null;
		Item documentItem = null;
		table = dynamo.getTable(tableName);

		if (table == null) {
			System.out.println("Table doesn't exist." + tableName);
			return documentItem;
		} else {
			documentItem = table.getItem(new GetItemSpec().withPrimaryKey(pkName, pkValue));
			if (documentItem == null) {
				System.out.println(pkValue + " - doesn't exist in the table - " + tableName);
				return documentItem;
			}
		}
		return documentItem;
	}
	
	public void updateDBItem(String tableName, Item latestItem) {

		try {
			Table table = null;
			table = dynamo.getTable(tableName);
			// Write the item to the table
			PutItemOutcome outcome = table.putItem(latestItem);
			// log.info(outcome.getItem().toJSONPretty());
		} catch (Exception e) {
			System.out.println("Update DB failed for table " + tableName + " for item " + latestItem);
		}

	}

	@Override
	public <T> void deleteGenericObjects(List<T> genericObject) throws DAOException {
		getMapper().batchDelete(genericObject);
		
	}

	@Override
	public <T> T getGenericObjectWithPrefix(Class<T> genericObject, String hashKey, String tableName, String prefix)
			throws DAOException {
		return dynamoDbMapper(dynamoDBMapperConfig(tableName, prefix)).load(genericObject, hashKey);
	}
	
	public <T> List<T> getGenericObjects(Class<T> genericModel, List<FilterCondition> filterConditions, Boolean consistentRead)
			throws DAOException {
		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();

		if (null == filterConditions || filterConditions.isEmpty()) {
			System.out.println("No Query Filters Found !");
		} else {
			
			System.out.println(filterConditions.size() + " Query filters found");
			for (FilterCondition condition : filterConditions) {
				if (RequestValidator.isValidString(condition.getParam())) {
					System.out.println("Filter Applied for:: " + condition.getParam() + " = " + condition.getValue());
					scanExpression.addFilterCondition(condition.getParam(),
							new Condition().withComparisonOperator(condition.getOperator())
									.withAttributeValueList(new AttributeValue().withS(condition.getValue())));
					scanExpression.withConsistentRead(consistentRead);
					
				}
			}

		}

		return (List<T>) getMapper().scan(genericModel, scanExpression);
	}


	

}
