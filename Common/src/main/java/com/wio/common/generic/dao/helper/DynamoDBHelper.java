package com.wio.common.generic.dao.helper;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.wio.common.config.model.ConfiguredValuesMO;
import com.wio.common.config.model.Device;
import com.wio.common.config.model.DeviceRadioSettings;
import com.wio.common.config.model.LANSettings;
import com.wio.common.config.model.MQTTTopic;
import com.wio.common.config.model.Network;
import com.wio.common.config.model.WiFi;
import com.wio.common.config.request.DeviceConfiguration;
import com.wio.common.config.request.LanSettings;
import com.wio.common.config.request.NetworkSettings;
import com.wio.common.config.request.WiFiSettings;
import com.wio.common.exception.DAOException;
import com.wio.common.exception.InvalidArugumentException;
import com.wio.common.generic.dao.DDBGenericDAO;
import com.wio.common.generic.dao.GenericDAO;
import com.wio.common.generic.dao.GenericDAOFactory;
import com.wio.common.validation.RequestValidator;

public class DynamoDBHelper {

	private static DynamoDBHelper _instance = new DynamoDBHelper();
	private DDBGenericDAO genDao = DDBGenericDAO.getInstance();

	private DynamoDBHelper() {}
	
	public static DynamoDBHelper getInstance() {
		return _instance;
	}
	
	private List<MQTTTopic> getSharedSubscriberTopics() throws DAOException {
		List<MQTTTopic> subTopics = null;

		subTopics = genDao.getGenericObjects(MQTTTopic.class, null);
		if (null == subTopics)
			throw new DAOException("No topic subscriptions found !");

		return subTopics;
	}
	
	public MQTTTopic getSubscriptionTopics(String subscriber) throws DAOException, InvalidArugumentException {
		// Publishing shared subscriber topics to device default topic
		
		if(RequestValidator.isEmptyField(subscriber)) {
			throw new InvalidArugumentException("Need subscriber name to get the topic path. E.g. - Config/Auth/Log etc.");
		}
		List<MQTTTopic> pubTopics = getSharedSubscriberTopics();

		for (MQTTTopic mqttTopic : pubTopics) {
			if (mqttTopic.getTopic().equals(subscriber)) {
				return mqttTopic;
			}
		}
		return null;
	}
	
	
	
	public ConfiguredValuesMO createConfigUpdTransaction(DeviceConfiguration configChanges, Device device, String status,
			String source) throws InvalidArugumentException {
		
		if(configChanges == null) {
			throw new InvalidArugumentException("Null DeviceConfiguration from - "+source);
		}
		
		Gson gson = new Gson();
		System.out.println("The Final Device configuration ==>> "+gson.toJson(configChanges).toString());
		
		ConfiguredValuesMO configValues = new ConfiguredValuesMO();
		if(!RequestValidator.isValidString(status)) {
			System.out.println("Status not provided hence, Default status 'Configured' will be used for device - "
					+ configChanges.getDeviceId());
			configValues.setStatus("Configured");
		}else {
			
			configValues.setStatus(status);
		}
		
		configValues.setConfiguration(configChanges);
		configValues.setDeviceId(configChanges.getDeviceId());
		configValues.setFwVersion(device.getFirmware());
		configValues.setScriptAgent(device.getScriptAgent());
		configValues.setDefaultTopic(device.getDefaultTopic());
		configValues.setCreatedBy(source);
		try {
			genDao.saveGenericObject(configValues);
			configValues.getTransactionId();
			System.out.println("Generated transaction id = "+configValues.getTransactionId());
		} catch (DAOException e) {
			System.err.println("Unable to save the latest config changes from ACS to CONFIG_UPDATES table."+e.toString());
			e.printStackTrace();
		}
		return configValues;
		
	}
	
    public <E, T> List<E> getListItem(List<T> runningList, List<T> configureList) {
    	
    	 List<LanSettings> confLanSettingsList = new ArrayList<>();	
         List<WiFiSettings> confWifiSettingsList = new ArrayList<>();		
         List<DeviceRadioSettings> confRadioSettingsList = new ArrayList<>();
    	
			switch (runningList.get(0).getClass().getSimpleName()) {
			case "LANSettings":
				for (int i = 0; i < runningList.size(); i++) {
					
					LANSettings runningLan = (LANSettings) runningList.get(i);
					LANSettings configLan = (LANSettings) configureList.get(i);
					
					if (runningLan.getIndex().equals(configLan.getIndex())
							&& !runningList.get(i).equals(configureList.get(i))) {
						confLanSettingsList.add(populateConfLanSetting(configLan));
					}
				}
				return (List<E>) confLanSettingsList;
			case "WiFi":
				for (int i = 0; i < runningList.size(); i++) {
					
					WiFi runningWifi = (WiFi) runningList.get(i);
					WiFi configWifi = (WiFi) configureList.get(i);
					
					if (runningWifi.getIndex().equals(configWifi.getIndex())
							&& !runningList.get(i).equals(configureList.get(i))) {
						confWifiSettingsList.add(populateConfWifiSetting(configWifi));
					}
				}

				return (List<E>)confWifiSettingsList;
			case "DeviceRadioSettings":

				for (int i = 0; i < runningList.size(); i++) {
					
					DeviceRadioSettings runningRadioSetting = (DeviceRadioSettings) runningList.get(i);
					DeviceRadioSettings configRadioSetting = (DeviceRadioSettings) configureList.get(i);
					
					if (runningRadioSetting.getIndex().equals(configRadioSetting.getIndex())
							&& !runningList.get(i).equals(configureList.get(i))) {
						confRadioSettingsList.add(configRadioSetting);
					}
				}
				return (List<E>)confRadioSettingsList;
			
			default:
				break;
				
			}
    	
		return null;
    	
    }
    
    public List<LanSettings> getLanSetting(List<LANSettings> lanSettings){
    	System.out.println("DynamoDBHelper LANSettings ==>> "+lanSettings);
    	List<LanSettings> newLanSettings = new ArrayList<>();
    	lanSettings.forEach(lan -> {
    	 newLanSettings.add(populateConfLanSetting(lan));
    	});	
    	return newLanSettings;
    	
    }
    
    public List<LANSettings> getLANSetting(List<LanSettings> lanSettings){
    	
    	List<LANSettings> newLanSettings = new ArrayList<>();
    	lanSettings.forEach(lan -> {
    	 newLanSettings.add(populateConfLANSetting(lan));
    	});	
    	return newLanSettings;
    	
    }

    public WiFiSettings populateConfWifiSetting(WiFi configWifi) {
    	WiFiSettings newWiFiSetting = new WiFiSettings();
    	newWiFiSetting.setWifiID(configWifi.getWifiID());
		//wifi.setNetworkID(TR69Helper.wavesParameters.getWifiSetting().get(param.getIndex()).getWifi().getNetworkSettings().getNetworkID());
    	newWiFiSetting.setWifiName(configWifi.getWifiName());
    	newWiFiSetting.setServiceName(configWifi.getServiceName());
    	newWiFiSetting.setSsid(configWifi.getSsid());
    	newWiFiSetting.setHiddenSSID(configWifi.getHiddenSSID());
    	newWiFiSetting.setCountry(configWifi.getCountry());
    	newWiFiSetting.setNetworkUser(configWifi.getNetworkUser());
    	newWiFiSetting.setAuthenticationType(configWifi.getAuthenticationType());
    	newWiFiSetting.setEncryptionType(configWifi.getEncryptionType());
    	newWiFiSetting.setPassPhrase(configWifi.getPassPhrase());
    	newWiFiSetting.setEnableWireless(configWifi.getEnableWireless());
    	NetworkSettings networkSettings = populateNetworkSettings(configWifi.getNetworkID());
    	newWiFiSetting.setNetworkSettings(networkSettings);
		return null;
	}

	public LanSettings populateConfLanSetting(LANSettings lanSettings) {
		LanSettings newLanSettings = new LanSettings();
		newLanSettings.setDhcp(lanSettings.getDhcp());
		newLanSettings.setDnsPrimary(lanSettings.getDnsPrimary());
		newLanSettings.setDnsSecondary(lanSettings.getDnsSecondary());
		newLanSettings.setGateway(lanSettings.getGateway());
		newLanSettings.setIfName(lanSettings.getIfName());
		newLanSettings.setIndex(lanSettings.getIndex());
		newLanSettings.setIpV4(lanSettings.getIpV4());
		newLanSettings.setIpV6(lanSettings.getIpV6());
		newLanSettings.setName(lanSettings.getName());
		newLanSettings.setPortID(lanSettings.getPortID());
		newLanSettings.setPortNo(lanSettings.getPortNo());
		newLanSettings.setVlanNo(lanSettings.getVlanNo());
		newLanSettings.setSubnetMask(lanSettings.getSubnetMask());
		
		NetworkSettings networkSettings = populateNetworkSettings(lanSettings.getNetworkID());
		newLanSettings.setNetworkSettings(networkSettings);
		return newLanSettings;
	}
	
	public LANSettings populateConfLANSetting(LanSettings lanSettings) {
		LANSettings newLanSettings = new LANSettings();
		newLanSettings.setDhcp(lanSettings.getDhcp());
		newLanSettings.setDnsPrimary(lanSettings.getDnsPrimary());
		newLanSettings.setDnsSecondary(lanSettings.getDnsSecondary());
		newLanSettings.setGateway(lanSettings.getGateway());
		newLanSettings.setIfName(lanSettings.getIfName());
		newLanSettings.setIndex(lanSettings.getIndex());
		newLanSettings.setIpV4(lanSettings.getIpV4());
		newLanSettings.setIpV6(lanSettings.getIpV6());
		newLanSettings.setName(lanSettings.getName());
		newLanSettings.setPortID(lanSettings.getPortID());
		newLanSettings.setPortNo(lanSettings.getPortNo());
		newLanSettings.setVlanNo(lanSettings.getVlanNo());
		newLanSettings.setSubnetMask(lanSettings.getSubnetMask());
		newLanSettings.setNetworkID(lanSettings.getNetworkSettings().getNetworkID());
		return newLanSettings;
	}

	private NetworkSettings populateNetworkSettings(String networkID) {
		
		NetworkSettings networkSettings = new NetworkSettings();
		Network network = null;
		try {
			network = genDao.getGenericObject(Network.class, networkID);
			if(network == null) {
				System.out.println("No network exist for networkID = "+networkID);
				return null;
			}
		} catch (DAOException e) {
			System.out.println("Could not able to fetch Network for networkID = "+networkID);
			e.printStackTrace();
		}
		
		networkSettings.setDhcpServer(network.getDhcpServer());
		networkSettings.setNetworkID(network.getNetworkID());
		networkSettings.setServiceName(network.getServiceName());
		networkSettings.setNetworkName(network.getNetworkName());
		networkSettings.setNetworkType(network.getNetworkType());
		networkSettings.setZtEnable(network.getZtEnable());
		networkSettings.setDhcpServer(network.getDhcpServer());
		networkSettings.setIpPoolStart(network.getIpPoolStart());
		networkSettings.setIpPoolEnd(network.getIpPoolEnd());
		networkSettings.setLeaseTime(network.getLeaseTime());	
		networkSettings.setMacFilterList(network.getMacFilterList());
		
		
		return networkSettings;
	}
	
	public ConfiguredValuesMO modifyConfigUpdTransaction(String transId, String status) {
		
		
		return null;
		
	}
	
	
	
	
	
}
