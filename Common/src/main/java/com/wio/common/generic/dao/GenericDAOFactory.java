package com.wio.common.generic.dao;

/**
 * The DAO Factory object to abstract the implementation of DAO interfaces.
 */
public class GenericDAOFactory {
    /**
     * Contains the implementations of the DAO objects. By default we only have a DynamoDB implementation
     */
    public enum DAOType {
        DynamoDB
    }
    
    public static GenericDAO getGenericDAO() {
		// TODO Auto-generated method stub
        return getGenericDAO(DAOType.DynamoDB);
	}
	
    public static GenericDAO getGenericDAO(DAOType daoType) {
    	GenericDAO dao = null;
        switch (daoType) {
            case DynamoDB:
                dao = DDBGenericDAO.getInstance();
                break;
        }

        return dao;
    }
    
}
	
