package com.wio.common.generic.dao;

import java.util.List;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;

public class FilterCondition2 
{
	private String param;
	private List<AttributeValue> values;
	private ComparisonOperator operator;
	
	
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	
	public List<AttributeValue> getValues() {
		return values;
	}
	public void setValues(List<AttributeValue> values) {
		this.values = values;
	}
	
	public ComparisonOperator getOperator() {
		return operator;
	}
	public void setOperator(ComparisonOperator operator) {
		this.operator = operator;
	}
	
}
