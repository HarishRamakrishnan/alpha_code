package com.wio.common.aws.api.es;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.amazonaws.services.elasticsearch.AWSElasticsearch;
import com.amazonaws.services.elasticsearch.AWSElasticsearchClientBuilder;
import com.amazonaws.services.elasticsearch.model.CreateElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.CreateElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.DomainInfo;
import com.amazonaws.services.elasticsearch.model.EBSOptions;
import com.amazonaws.services.elasticsearch.model.ElasticsearchClusterConfig;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesRequest;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesResult;
import com.wio.common.staticinfo.SystemConfigMessages;

public class ElasticSearchAPI {
	static AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
			.withRegion(SystemConfigMessages.SYSTEM_AWS_REGION).build();

	public static void createDomain(String domainName) {
		// AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
		// .withCredentials(new
		// ProfileCredentialsProvider()).withRegion(SystemConfigMessages.SYSTEM_AWS_REGION).build();

		// AWSElasticsearch awsElasticsearch =
		// AWSElasticsearchClientBuilder.standard().withRegion(SystemConfigMessages.SYSTEM_AWS_REGION).build();

		EBSOptions eBSOptions = new EBSOptions();
		eBSOptions.setEBSEnabled(true);
		eBSOptions.setVolumeType("gp2");
		eBSOptions.setVolumeSize(10);

		String accessPolicies = "{\"Version\": \"2012-10-17\","
				+ "\"Statement\": [{\"Effect\": \"Allow\","
				+ "\"Principal\": {\"AWS\": [\"*\"]},"
				+ "\"Action\": [\"es:*\"],"
				+ "\"Resource\": \"arn:aws:es:us-east-1:528719264515:domain/test/*\"}]}";

		ElasticsearchClusterConfig clusterConfig = new ElasticsearchClusterConfig();
		clusterConfig.setInstanceType("t2.small.elasticsearch");
		clusterConfig.setInstanceCount(1);
		clusterConfig.setDedicatedMasterEnabled(false);
		clusterConfig.setZoneAwarenessEnabled(false);
		
		
		CreateElasticsearchDomainRequest request = new CreateElasticsearchDomainRequest();
		request.setDomainName(domainName);
		request.setElasticsearchVersion("5.1");
		request.setEBSOptions(eBSOptions);
		request.setAccessPolicies(accessPolicies);
		request.setElasticsearchClusterConfig(clusterConfig);
		
		CreateElasticsearchDomainResult result = awsElasticsearch.createElasticsearchDomain(request);

		System.out.println(result.getDomainStatus());
		System.out.println(result.getSdkHttpMetadata());
		System.out.println(result.getSdkResponseMetadata());	
	}

	public static String getESEndpoint(String domainName) {
		// AWSElasticsearch awsElasticsearch = AWSElasticsearchClientBuilder.standard()
		// .withCredentials(new
		// ProfileCredentialsProvider()).withRegion(SystemConfigMessages.SYSTEM_AWS_REGION).build();
		
		ListDomainNamesResult listDomainNamesResult = awsElasticsearch.listDomainNames(new ListDomainNamesRequest());
		String endPoint = "";
		
		for (DomainInfo domain : listDomainNamesResult.getDomainNames()) {
			if (domain.getDomainName().equals(domainName)) {
				DescribeElasticsearchDomainResult esDomainResult = awsElasticsearch.describeElasticsearchDomain(
			            new DescribeElasticsearchDomainRequest().withDomainName(domain.getDomainName()));	
			    endPoint = esDomainResult.getDomainStatus().getEndpoint();
			    break;
			}
		}		
		System.out.println(endPoint);
	
		return endPoint;
	}
	
    public static String updateES(HttpPost httpPost) throws ClientProtocolException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

            @Override
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				System.out.println(response.getStatusLine().getReasonPhrase());
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {

                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        try {
        	httpClient.execute(httpPost, responseHandler);
        } catch (Exception e) {
        	System.out.println("Exception is " +e.getMessage());
        	return "FAILURE";
        }
		return "SUCCESS";
    }
    
	/*
	public static void  processMessage(JsonObject jsonObject ,String query) throws ClientProtocolException, IOException, DAOException {

		if(jsonObject!=null){

			String host = null;
			String deviceName = null;
			TreeMap<String, String> awsHeaders = new TreeMap<String, String>();
			String deviceId = jsonObject.get("deviceId").getAsString();
			String payload=jsonObject.toString();

			GenericDAO  genDAO = GenericDAOFactory.getGenericDAO();

			if(deviceId!=null ){	
				Device device = genDAO.getGenericObject(Device.class, deviceId);
				if(device!=null){
					String serviceName = device.getServiceName();
					deviceName = device.getDeviceName();
					host=HttpClientUtil.getEndpoint(serviceName);		
					awsHeaders.put("host", host);	
					String elasticSearchEndpoint = "https://" + host + query +deviceName;
						HttpClientUtil.postRequest(payload, elasticSearchEndpoint, awsHeaders,query+deviceName);

				}

			}
		}

	}

	 */

}
