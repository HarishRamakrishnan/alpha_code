package com.wio.common.Enums;

import java.io.ObjectStreamException;
import java.lang.reflect.Method;
import java.util.Iterator;


/**
 * Defines the abstract base class for all enums.
 */
public abstract class Enum implements  IEnum, java.io.Serializable , Comparable		
{
    /**
     * The ordinal value of the enum.  This value will be saved to the
     * database.
     */
    protected int              m_ordinal = 0;

    /**
     * A textual description of the enum.  This data item is marked as 'transient'
     * as it doesn't need to serialised as the ordinal value will used to identify
     * the different enum types.
     */
    private transient String   m_description = null;
    //private final String   m_description;

    /**
     * The default constructor has been made private as Java will automatically
     * create a public default constructor and this class shouldn't be created.
     */
    protected Enum()
    {
        m_ordinal = 0;
        m_description = null;
    }

    /**
     * A protected constructor which takes an Enums ordinal value and description.
     * This constructor is defined as protected to ensure that only derived classes
     * can setup Enums.
     *
     * @param ordinal the ordinal value of the enum.
     * @param description the description of the enum.
     */
    protected Enum(int ordinal, String description)
    {
        m_ordinal     = ordinal;
        m_description = description;
    }

    /**
     * Returns a string representation of the object.  The descrption of the Enum
     * will be used as string representation of the object.
     *
     * @return  a string representation of the object.
     */
    public final String toString()
    {
        return m_description;
    }

    /**
     * Indicates whether some other object is "equal to" this Enum.
     *
     * An Enum is only equal if it is of the same type and the ordinals
     * contain the same value.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is the same as the obj
     *          argument; <code>false</code> otherwise.
     */
    public final boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if ((object == null) || (this.getClass() != object.getClass()))
        {
            return false;
        }

        return (this.getOrdinal() == ((Enum)object).getOrdinal()) ? true : false;
    }

    /**
     * Returns the ordinal value of the Enum.
     *
     * @return  the ordinal value of the Enum.
     */
    public final int getOrdinal()
    {
        return m_ordinal;
    }

      /**
     * Returns the text description of the Enum.
     * Used by GUI to populate lists etc
     *
     * @return  the ordinal value of the Enum.
     */
    public final String getDescription()
    {
        return m_description;
    }

    /**
     * Returns a hash code value for the object. This method simnply returns
     * the ordinal value of the Enum.
     *
     * @return  a hash code value for this object.
     * @see     java.lang.Object#hashCode()
     */
    public final int hashCode()
    {
        return getOrdinal();
    }
    
    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.<p>
     */
    public int compareTo(Object o) {
        Enum v = (Enum) o ;
        int thisVal = getOrdinal();
        int anotherVal = v.getOrdinal();
        return (thisVal<anotherVal ? -1 : (thisVal==anotherVal ? 0 : 1));
    }
     
    /**
     * Resolves derived Enum instances being deserialized to the an
     * pre-defined derived Enum instance.  This method ensures that
     * only 1 derived Enum instance for each enumeration entry is
     * ever created. Therefore when comparing derived Enum instances
     * the == and != operators can be used.
     *
     * @return the resolved derived Enum Object.
     *
     * @throws ObjectStreamException never thrown, but mandated by the
     * serialization specification.
     */
    abstract protected Object readResolve() throws java.io.ObjectStreamException;
    
    /**
     * This API returns the Implemented Enum Object for the given ordinal.
     * 
     * The derived classes must implement this method to return proper Enum object.
     * 
     * @param ordinal
     * @return
     */
    public static IEnum getEnum(Integer ordinal){
    	return null;
    }

    // Return the enum instance of the type enumClass and with ordinal value = val 
    public static Enum getInstance(Class enumClass, int val)
    {
        try {
            Method[] list = enumClass.getDeclaredMethods();
            for (int i = 0; i < list.length; i++)
            {
                String name = list[i].getName();
                Class[] types = list[i].getParameterTypes();
                if (name.startsWith("get") && types != null && types.length == 1 && types[0].getName().equals("int")) // ex: TruthValue.getTruthValue(int);
                    return (Enum) list[i].invoke(enumClass, val);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * An iterator over a Enum.  The <tt>remove</tt> operation is not
     * supported by this Iterator.
     *
     * @see java.util.Iterator
     */
    protected class Itr implements Iterator
    {
        /**
         * Index of element to be returned by subsequent call to next.
         */
        private int m_cursor = 0;

        /**
         * The array of Enums to iterate over.
         */
        private final Enum[] m_values;

        /**
         * A constructor that takes the starting iteration point and the
         * array to iterate over.
         */
        public Itr(Enum value, Enum[] values)
        {
            m_cursor = value.getOrdinal();
            m_values = values;
        }

        /**
         * Returns <tt>true</tt> if the iteration has more elements. (In other
         * words, returns <tt>true</tt> if <tt>next</tt> would return an element
         * rather than throwing an exception.)
         *
         * @return <tt>true</tt> if the iterator has more elements.
         */
        public boolean hasNext()
        {
            return m_cursor < m_values.length;
        }

        /**
         * Returns the next element in the interation.
         *
         * @return the next element in the iteration.
         * @exception java.util.NoSuchElementException iteration has no more elements.
         */
        public Object next()
        {
            try
            {
                Object next = m_values[m_cursor];
                m_cursor++;
                return next;
            }
            catch(IndexOutOfBoundsException e)
            {
                throw new java.util.NoSuchElementException();
            }
        }

        /**
         * This method always throw UnsupportedOperationException as the
         * <tt>remove</tt> operation is not supported by this Iterator
         *
         * @exception UnsupportedOperationException if the <tt>remove</tt>
         *		  operation is not supported by this Iterator.
         */
        public void remove()
        {
            throw new java.lang.UnsupportedOperationException();
        }
    }

    /**
     * Returns an iterator over the elements contained in this Enum that
     * exist on the switch.
     *
     * @return an {@link java.util.Iterator} over the elements contained
     * in this Enum.
     */
    abstract public Iterator iterator();
}

