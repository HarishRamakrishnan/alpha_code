package com.wio.common.helper;

import java.util.ArrayList;
import java.util.List;

public class ActionMapper {
	
	//Lambda functions
	public static final String USER_MGMT_LAMBDA = "com.wio.core.user.handler.UserRequestRouter::handler";	
	public static final String CONFIGURATION_MGMT_LAMBDA = "com.wio.core.config.handler.ConfigRequestRouter::handler"; 	
	public static final String FIRMWARE_UPGRADE_MGMT_LAMBDA = "com.wio.core.firmware.handler.FirmwareRequestRouter::handler";	
	public static final String INTIAL_PROVISIONING_MGMT_LAMBDA = "com.wio.core.provision.handler.ProvisionRequestRouter::handler";
	public static final String SYSTEM_STATUS_LAMBDA = "com.wio.core.system.handler.SystemRequestRouter::handler";
	
	public static final String seperator = "/";
	public static final String MANAGE = "Manage"; 
    public static final String MODIFY = "Modify"; 
    public static final String VIEW = "View"; 
	
	// User Management Action type
	public static final String USER_MGMT_ADD_USER = "addUser";
	public static final String USER_MGMT_MODIFY_USER = "modifyUser";
	public static final String USER_MGMT_DELETE_USER = "deleteUser";
	public static final String USER_MGMT_VIEW_USER = "viewUser";
	
	public static final String USER_MGMT_ADD_ROLE = "addRole";
	public static final String USER_MGMT_DELETE_ROLE = "deleteRole";	 
	public static final String USER_MGMT_VIEW_ROLE = "viewRole";	

	public static final String USER_MGMT_ADD_PRIVILEGED_ACTION = "addPrivilegedAction";
	public static final String USER_MGMT_DELETE_PRIVILEGED_ACTION = "deletePrivilegedAction";
	public static final String USER_MGMT_VIEW_PRIVILEGED_ACTION = "viewPrivilegedAction";
	
	public static final String USER_MGMT_ADD_ROLE_PRIVILEGE = "addRolePrivilege";
	public static final String USER_MGMT_VIEW_ROLE_PRIVILEGE = "viewRolePrivilege";
	public static final String USER_MGMT_DELETE_ROLE_PRIVILEGE = "deleteRolePrivilege";
	
	/*public static final String USER_MGMT_ASSIGN_PRIVILEGE = "assignPrivilege";
	public static final String USER_MGMT_UNASSIGN_PRIVILEGE = "unAssignPrivilege";
	public static final String USER_MGMT_VIEW_ROLE_PRIVILEGE = "viewPrivilegeMap";
	
	public static final String USER_MGMT_ADD_PRIVILEGE = "addPrivilege";
	public static final String USER_MGMT_DELETE_PRIVILEGE = "deletePrivilege";
	public static final String USER_MGMT_VIEW_PRIVILEGE = "viewPrivilege";
	
	public static final String USER_MGMT_VIEW_ACTION = "viewAction";

	public static final String USER_MGMT_MAP_ACTION_PRIVILEGE = "mapActionPrivilege";
	public static final String USER_MGMT_UNMAP_ACTION_PRIVILEGE = "unmapActionPrivilege";
	public static final String USER_MGMT_VIEW_ACTION_PRIVILEGE = "viewActionPrivilege";*/
	
	
	// Login Management Action type
	public static final String SECURITY_MGMT_LOGIN = "login";
	public static final String SECURITY_MGMT_GENERATE_MOBILE_OTP = "genMobileOTP";
	public static final String SECURITY_MGMT_VALIDATE_MOBILE_OTP = "valMobileOTP";
	
	//ConfigurationManagement Action Types
	public static final String CONFIGURATION_MGMT_ADD_HARWARE ="addHardware";
	public static final String CONFIGURATION_MGMT_MODIFY_HARWARE ="modifyHardware";
	public static final String CONFIGURATION_MGMT_DELETE_HARWARE ="deleteHardware";
	public static final String CONFIGURATION_MGMT_VIEW_HARWARE ="viewHardware";
	
	public static final String CONFIGURATION_MGMT_ADD_HARWARE_PROFILE ="addHardwareProfile";
	public static final String CONFIGURATION_MGMT_MODIFY_HARWARE_PROFILE ="modifyHardwareProfile";
	public static final String CONFIGURATION_MGMT_DELETE_HARWARE_PROFILE ="deleteHardwareProfile";
	public static final String CONFIGURATION_MGMT_VIEW_HARWARE_PROFILE ="viewHardwareProfile";
	
	public static final String CONFIGURATION_MGMT_ADD_DEVICE ="addDevice";
	public static final String CONFIGURATION_MGMT_MODIFY_DEVICE ="modifyDevice";
	public static final String CONFIGURATION_MGMT_DELETE_DEVICE ="deleteDevice";
	public static final String CONFIGURATION_MGMT_VIEW_DEVICE ="viewDevice";
	
	public static final String CONFIGURATION_MGMT_ADD_SERVICE = "addService";
	public static final String CONFIGURATION_MGMT_MODIFY_SERVICE = "modifyService";
	public static final String CONFIGURATION_MGMT_DELETE_SERVICE = "deleteService";
	public static final String CONFIGURATION_MGMT_VIEW_SERVICE = "viewService";
		
	public static final String CONFIGURATION_MGMT_ADD_WIFI = "addWiFi";
	public static final String CONFIGURATION_MGMT_MODIFY_WIFI = "modifyWiFi";
	public static final String CONFIGURATION_MGMT_VIEW_WIFI = "viewWiFi";
	public static final String CONFIGURATION_MGMT_DELETE_WIFI = "deleteWiFi";
	
	public static final String CONFIGURATION_MGMT_ADD_NETWORK = "addNetwork";
	public static final String CONFIGURATION_MGMT_MODIFY_NETWORK = "modifyNetwork";
	public static final String CONFIGURATION_MGMT_DELETE_NETWORK = "deleteNetwork";
	public static final String CONFIGURATION_MGMT_VIEW_NETWORK = "viewNetwork";
	
	public static final String CONFIGURATION_MGMT_ADD_DG = "addDG";
	public static final String CONFIGURATION_MGMT_MODIFY_DG = "modifyDG";
	public static final String CONFIGURATION_MGMT_DELETE_DG = "deleteDG";
	public static final String CONFIGURATION_MGMT_VIEW_DG = "viewDG";
	
	public static final String CONFIGURATION_MGMT_DEVICE_DG_MAP ="mapDeviceDG";
	public static final String CONFIGURATION_MGMT_DEVICE_DG_UNMAP ="unmapDeviceDG";
	
	public static final String CONFIGURATION_MGMT_HARDWARE_SERVICE_MAP ="hwServiceMap";	
	public static final String CONFIGURATION_MGMT_HARDWARE_SERVICE_UNMAP ="hwServiceUnMap";	
	public static final String CONFIGURATION_MGMT_VIEW_HARDWARE_SERVICE_MAP ="viewHwServiceMap";	
	
	public static final String CONFIG_DEVICE_SERVICE_MAP ="assignDevices";	
	public static final String CONFIG_DEVICE_SERVICE_UNMAP ="unassignDevices";	
	
	public static final String CONFIG_DEVICE_HP_MAP ="assignDevicesToHP";	
	
	public static final String CONFIGURATION_MGMT_DG_WIFI_MAP ="dgWifiMap";	
	public static final String CONFIGURATION_MGMT_DG_WIFI_UNMAP ="dgWifiUnMap";	
	public static final String CONFIGURATION_MGMT_VIEW_DG_WIFI_MAP ="viewDgWifiMap";
	
	public static final String CONFIGURATION_MGMT_GET_DEVICE_CONFIGURATION = "getDeviceConfig";	
	public static final String CONFIGURATION_MGMT_WIRELESS_CONFIGURATION = "fetchWirelessConfiguration";
	
	public static final String CONFIGURATION_MGMT_GET_DEVICE_TOPICS = "getDeviceTopics";
	
	public static final String CONFIGURATION_MGMT_ADD_CONFIG = "addDeviceConfig";
	public static final String CONFIGURATION_MGMT_MODIFY_CONFIG = "modifyDeviceConfig";
	public static final String CONFIGURATION_MGMT_DELETE_CONFIG = "deleteDeviceConfig";
	// Initial Provisioning 
	public static final String INITIAL_PROVISIONING_VALIDATE_DEVICE ="validateDevice";
	
	//MQTT web hook
	public static final String INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_REGISTER ="auth_on_register";
	public static final String INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_SUBSCRIBE ="auth_on_subscribe";
	
	//Firmware upgrade
	public static final String FIRMWARE_UPLOAD="firmwareUpload";
	public static final String FIRMWARE_VIEW="getFirmware";
	public static final String FIRMWARE_DELETE="deleteFirmware";
	
	
	//System Status
	public static final String FETCH_ENV_STATUS = "getEnvStatus";
	
	
	public static List<String> provisionActions()
	{
		List<String> actions = new ArrayList<>();
		
		actions.add(INITIAL_PROVISIONING_VALIDATE_DEVICE);
		actions.add(INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_REGISTER);
		actions.add(INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_SUBSCRIBE);
		actions.add(FETCH_ENV_STATUS);
		actions.add(SECURITY_MGMT_GENERATE_MOBILE_OTP);
		actions.add(SECURITY_MGMT_VALIDATE_MOBILE_OTP);
		
		return actions;
	}
	
	
	public static String createUserId(String service, String userName){
		
		return service+seperator+userName; 
	}

	
	public static String getInvokingLambdaFunction(String action)
	{		
		switch(action)
		{
			case USER_MGMT_ADD_USER:
			case USER_MGMT_MODIFY_USER:
			case USER_MGMT_DELETE_USER:
			case USER_MGMT_VIEW_USER:
				
			case USER_MGMT_ADD_ROLE:
			case USER_MGMT_DELETE_ROLE:	
			case USER_MGMT_VIEW_ROLE:				
				
			case USER_MGMT_ADD_PRIVILEGED_ACTION:
			case USER_MGMT_DELETE_PRIVILEGED_ACTION:
			case USER_MGMT_VIEW_PRIVILEGED_ACTION:
				
			case USER_MGMT_ADD_ROLE_PRIVILEGE:
			case USER_MGMT_DELETE_ROLE_PRIVILEGE:
			case USER_MGMT_VIEW_ROLE_PRIVILEGE:
				return  USER_MGMT_LAMBDA;
		
			case CONFIGURATION_MGMT_ADD_HARWARE:
			case CONFIGURATION_MGMT_MODIFY_HARWARE:
			case CONFIGURATION_MGMT_DELETE_HARWARE:
			case CONFIGURATION_MGMT_VIEW_HARWARE:
				
			case CONFIGURATION_MGMT_ADD_HARWARE_PROFILE:
			case CONFIGURATION_MGMT_MODIFY_HARWARE_PROFILE:
			case CONFIGURATION_MGMT_DELETE_HARWARE_PROFILE:
			case CONFIGURATION_MGMT_VIEW_HARWARE_PROFILE:
				
			case CONFIGURATION_MGMT_ADD_DEVICE:
			case CONFIGURATION_MGMT_MODIFY_DEVICE:
			case CONFIGURATION_MGMT_DELETE_DEVICE:
			case CONFIGURATION_MGMT_VIEW_DEVICE:
				
			case CONFIGURATION_MGMT_ADD_SERVICE:	
			case CONFIGURATION_MGMT_MODIFY_SERVICE:	
			case CONFIGURATION_MGMT_DELETE_SERVICE:	
			case CONFIGURATION_MGMT_VIEW_SERVICE:
				
			case CONFIGURATION_MGMT_ADD_DG:
			case CONFIGURATION_MGMT_MODIFY_DG:
			case CONFIGURATION_MGMT_DELETE_DG:
			case CONFIGURATION_MGMT_VIEW_DG:
				
			case CONFIGURATION_MGMT_ADD_NETWORK:	
			case CONFIGURATION_MGMT_MODIFY_NETWORK:	
			case CONFIGURATION_MGMT_DELETE_NETWORK:	
			case CONFIGURATION_MGMT_VIEW_NETWORK:
				
			case CONFIGURATION_MGMT_ADD_WIFI:
			case CONFIGURATION_MGMT_MODIFY_WIFI:
			case CONFIGURATION_MGMT_VIEW_WIFI:
			case CONFIGURATION_MGMT_DELETE_WIFI:
											
			case CONFIGURATION_MGMT_DEVICE_DG_MAP:
			case CONFIGURATION_MGMT_DEVICE_DG_UNMAP:
			
			case CONFIGURATION_MGMT_ADD_CONFIG: 
			case CONFIGURATION_MGMT_MODIFY_CONFIG:
			case CONFIGURATION_MGMT_DELETE_CONFIG: 
				
			case CONFIGURATION_MGMT_HARDWARE_SERVICE_MAP:
			case CONFIGURATION_MGMT_HARDWARE_SERVICE_UNMAP:
			case CONFIGURATION_MGMT_VIEW_HARDWARE_SERVICE_MAP:
				
			case CONFIG_DEVICE_SERVICE_MAP:
			case CONFIG_DEVICE_SERVICE_UNMAP:
			
			case CONFIG_DEVICE_HP_MAP:
				
			case CONFIGURATION_MGMT_DG_WIFI_MAP:
			case CONFIGURATION_MGMT_DG_WIFI_UNMAP:
			case CONFIGURATION_MGMT_VIEW_DG_WIFI_MAP:
				
			case CONFIGURATION_MGMT_GET_DEVICE_CONFIGURATION:
			case CONFIGURATION_MGMT_WIRELESS_CONFIGURATION:
				
			case CONFIGURATION_MGMT_GET_DEVICE_TOPICS:
					return  CONFIGURATION_MGMT_LAMBDA;
					
			case FIRMWARE_UPLOAD:
			case FIRMWARE_VIEW:
				return  FIRMWARE_UPGRADE_MGMT_LAMBDA;
				
			case FETCH_ENV_STATUS:
				return SYSTEM_STATUS_LAMBDA;
				
    		case INITIAL_PROVISIONING_VALIDATE_DEVICE:
    		case INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_REGISTER:
    		case INITIAL_PROVISIONING_WEBHOOK_AUTH_ON_SUBSCRIBE:
    			return INTIAL_PROVISIONING_MGMT_LAMBDA;
    			
			default:
				return null;
		}
		
				
	}
	
	public static String getUserMgmtPrivilegeforManage(String userRole){
		
		return MANAGE +userRole;
	}
	
	public static String getUserMgmtPrivilegeforModify(String userRole){
		
		if(userRole!=null){
		 return MODIFY +userRole;
		}else{
	     return "ModifyUser";
		}
	}
	
	public static String getUserMgmtPrivilegeforView(String userRole){
		
		if(userRole!=null){
			 return VIEW +userRole;
			}else{
		     return "ViewUser";
			}
	}
}
