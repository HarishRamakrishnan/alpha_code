package com.wio.common.staticinfo;

/**
 * Configuration parameters for the DynamoDB DAO objects
 */
public class DynamoDBObject 
{
	//User level tables   
    public static final String USER = "USER";
    public static final String PRIVILEGED_ACTION = "PRIVILEGED_ACTION";
    public static final String ROLE_PRIVILEGE = "ROLE_PRIVILEGE";
    public static final String USER_ROLE = "USER_ROLE";
    public static final String USER_PRIVILEGE = "USER_PRIVILEGE";
    public static final String USER_ACTION = "USER_ACTION";
    public static final String ROLE_PRIVILEGE_MAP = "ROLE_PRIVILEGE_MAP";
    public static final String ACTION_PRIVILEGE_MAP = "ACTION_PRIVILEGE_MAP";
    
    //Security credentials tables
    public static final String SECURITY_TOKEN = "SECURITY_TOKEN";
    public static final String IAM_USER_CREDENTIALS = "IAM_CLIENT_CREDENTIALS";
    public static final String OTP_TRANSACTIONS = "OTP_TRANSACTIONS";
    
    
    //Configuration Tables    
    public static final String HARDWARE = "HARDWARE";
    public static final String HARDWARE_PROFILE = "HARDWARE_PROFILE";
    public static final String DEVICE = "DEVICE";
    public static final String DEVICE_CONFIG = "DEVICE_CONFIG";
    public static final String SERVICE = "SERVICE";
    public static final String DEVICE_GROUP = "DEVICE_GROUP";
    public static final String NETWORK = "NETWORK";
    public static final String WIFI = "WIFI";
    public static final String HW_SERVICE_MAP = "HW_SERVICE_MAP";
    public static final String DG_WIFI_MAP = "DG_WIFI_MAP";
    public static final String FIRMWARE = "FIRMWARE";
    public static final String FIRMWARE_UPDATES = "FIRMWARE_UPDATES";
    public static final String CONFIG_SOURCE = "CONFIG_SOURCE";
    public static final String TABLE_PREFIX = "CONFIGURED_";
    
    public static final String MQTT_CLIENT = "MQTT_CLIENT";
    public static final String MQTT_TOPIC = "MQTT_TOPIC";
    
    //Monitoring
    public static final String AUTO_TEST_RUN = "TEST_RUN";
    public static final String AUTO_TEST_SUITE = "TEST_SUITE";
    public static final String AUTO_TEST_RESULTS = "TEST_RESULTS";
    public static final String AUTOMATION_TEST_RESULTS = "AUTOMATION_TEST_RESULTS";
}
