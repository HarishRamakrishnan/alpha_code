package com.wio.common.staticinfo;

import java.util.Properties;

import com.wio.common.util.UtilProperties;

public class SystemConfigMessages {
	
	
	static UtilProperties utilProp=null;
	static Properties prop=new Properties();
	
	static
	{
		/*if(null == System.getenv("REGION"))
		{
			try 
			{
				throw new InternalErrorException("Lambda Environment Variable 'REGION' Not Found !");
			} 
			catch (InternalErrorException e) 
			{
				e.printStackTrace();
			}
		}	*/		
		
		utilProp = new UtilProperties();
		prop = utilProp.loadProperties();	
	}
		
	public static final String SYSTEM_AWS_REGION = System.getenv("REGION");
	public static final String SYSTEM_AWS_ACCOUNT = prop.getProperty("aws.account");
	
	public static final String SYSTEM_SECURITY_TOKEN_TIMEOUT = prop.getProperty("securitytoken.timeout");

	public static final String OTP_TIMEOUT = prop.getProperty("otp.timeout");
	
	public static final String SYSTEM_DATE_FORMAT = prop.getProperty("dateFormat");
	public static final String SYSTEM_S3_BUCKET_FIRMWAREUPLOAD = prop.getProperty("s3.bucket.firmwareupload");
	public static final String SYSTEM_S3_BUCKET_LAMBDA = prop.getProperty("s3.bucket.lambda");
	
	public static final String SYSTEM_FIRMWARE_DOWNLOAD_S3_URL = prop.getProperty("firmware.download.s3.url");
	public static final String SYSTEM_HTTP_PROTOCOL = prop.getProperty("http");
	
	public static final String SYSTEM_AWS_SQS_ARN = prop.getProperty("aws.sqsarn");

	
	//MQTT Broker
	public static final String MQTT_BROKER_URI = prop.getProperty("mqtt.broker.uri");
	public static final String MQTT_BROKER_USERNAME = prop.getProperty("mqtt.broker.username");
	public static final String MQTT_BROKER_PASSWORD = prop.getProperty("mqtt.broker.password");
}
