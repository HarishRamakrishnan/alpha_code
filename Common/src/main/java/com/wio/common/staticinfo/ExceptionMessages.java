package com.wio.common.staticinfo;

import java.util.Properties;

import com.wio.common.util.UtilProperties;

/**
 * Static list of error messages when exceptions are thrown
 */
public class ExceptionMessages {
	
	static UtilProperties utilProp=null;
	static Properties prop=new Properties();
	
	static
	{
		utilProp = new UtilProperties();
		prop = utilProp.loadProperties();	
	}
	
	public static final String EX_INVALID_USERNAME = prop.getProperty("exception.message.invalidUsername");
	
	public static final String EX_NO_TOKEN = prop.getProperty("exception.message.noToken");
	public static final String EX_TOKEN_EXPIRED = prop.getProperty("exception.message.tokenExpired");
	
	public static final String EX_AUTHENTICATION_FAILED = prop.getProperty("exception.message.authenticationFailed");
	public static final String EX_EMPTY_TOKEN = prop.getProperty("exception.message.emptyToken");
	public static final String EX_EMPTY_USER = prop.getProperty("exception.message.emptyUser");
	
	
	public static final String EX_USER_EXIST = prop.getProperty("exception.message.usernameAlreadyExist");
	public static final String EX_INVALID_CREDENTIALS = prop.getProperty("exception.message.invalidCredentials");
	public static final String EX_INVALID_PASSWORD = prop.getProperty("exception.message.invalidPassword");
	public static final String EX_INVALID_PRIVILEGE = prop.getProperty("exception.message.invalidPrivilege");
	public static final String EX_AUTHORIZATION_FAILED = prop.getProperty("exception.message.authorizationFailed");
	public static final String EX_PRIVILEGE_ROLE_MAPPED = prop.getProperty("exception.message.privilegeAlreadyAssigned");
	public static final String EX_PRIVILEGE_ROLE_NOT_MAPPED = prop.getProperty("exception.message.privilegeNotAssigned");
	
	public static final String EX_INVALID_INPUT = prop.getProperty("exception.message.invalidInput");
	public static final String EX_EMPTY_FIELD = prop.getProperty("exception.message.emptyField");
    public static final String EX_PWD_SALT = prop.getProperty("exception.message.passwordSlat");
    public static final String EX_PWD_ENCRYPT = prop.getProperty("exception.message.passwordEncrypt");
    public static final String EX_PWD_SAVE = prop.getProperty("exception.message.passwordSave");
    public static final String EX_NO_COGNITO_IDENTITY = prop.getProperty("exception.message.noCognitoIdentity");
    public static final String EX_DAO_ERROR = prop.getProperty("exception.message.DAOError");
    public static final String ROLE_DAO_ERROR = prop.getProperty("exception.message.roleDAOError");
    
    public static final String EX_INVALID_EMAIL = prop.getProperty("exception.message.invalidEmail");
    public static final String EX_INVALID_NUMBER = prop.getProperty("exception.message.invalidNumber");
    public static final String EX_INVALID_IP = prop.getProperty("exception.message.invalidIP");
    public static final String EX_INVALID_ROLE = prop.getProperty("exception.message.invalidRole");
    
    public static final String EX_INVALID_AP = prop.getProperty("exception.message.invalidAP");
    public static final String EX_INVALID_DG = prop.getProperty("exception.message.invalidDG");
    public static final String EX_INVALID_NZ = prop.getProperty("exception.message.invalidNZ");
    public static final String EX_INVALID_NW = prop.getProperty("exception.message.invalidNW");
    public static final String EX_INVALID_SERVICE = prop.getProperty("exception.message.invalidService");
    public static final String EX_INVALID_SSID = prop.getProperty("exception.message.invalidSSID");
    public static final String EX_INVALID_PRESHAREDKEY = prop.getProperty("exception.message.invalidPreSharedKey");
    public static final String EX_INVALID_AUTH_MODE = prop.getProperty("exception.message.authenticationMode");
    
    public static final String EX_REL_DEPENDENCY = prop.getProperty("exception.message.dependencyRelation");
    public static final String EX_REL_DEPENDENCY_ROLE = prop.getProperty("exception.message.roleDependency");
    public static final String EX_REL_DEPENDENCY_PRIVILEGE = prop.getProperty("exception.message.privilegeDependency");
}
