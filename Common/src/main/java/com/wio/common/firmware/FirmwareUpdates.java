package com.wio.common.firmware;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;


@DynamoDBTable(tableName = DynamoDBObject.FIRMWARE_UPDATES)
public class FirmwareUpdates 
{	
	private String transactionID;
	private String fwID;
	private String hpID;
	private String deviceID;
	private String status;
	
	private String lastUpdateTime;
	private String creationTime;
	
		
    public FirmwareUpdates() {

    }

    
    @DynamoDBHashKey(attributeName = "TRANSACTION_ID")
    public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	
	@DynamoDBRangeKey(attributeName = "DEVICE_ID")
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	
	
	
	@DynamoDBAttribute(attributeName = "FW_ID")	
    public String getFwID() {
		return fwID;
	}
	public void setFwID(String fwID) {
		this.fwID = fwID;
	}


	@DynamoDBAttribute(attributeName = "HP_ID")
	public String getHpID() {
		return hpID;
	}
	public void setHpID(String hpID) {
		this.hpID = hpID;
	}

	
	@DynamoDBAttribute(attributeName = "STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@DynamoDBAttribute(attributeName = "LAST_UPDATE_TIME")
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	
	@DynamoDBAttribute(attributeName = "CREATION_TIME")
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	
	
}
