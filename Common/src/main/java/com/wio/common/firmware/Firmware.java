package com.wio.common.firmware;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.wio.common.staticinfo.DynamoDBObject;

/**
 * AccessPoint object model - this class is annotated to be used with the DynamoDB object mapper
 */
@DynamoDBTable(tableName = DynamoDBObject.FIRMWARE)
public class Firmware 
{	
	private String fwID;
	private String hpID;
	private String hpTopic;
	private String status;
	private String filename;
	private String version;
	private String versionType;
	private String source;
	private String checksumType;
	private String checksum;
	private String s3URL;
	private String s3BucketName;
	private String s3KeyName;
	private String edgeURL;
	
	private String uploadedBy;
	private String uploadDate;
	
		
    public Firmware() {

    }

    
    @DynamoDBHashKey(attributeName = "FW_ID")
    public String getFwID() {
		return fwID;
	}
	public void setFwID(String fwID) {
		this.fwID = fwID;
	}


	@DynamoDBAttribute(attributeName = "FILE_NAME")
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}


	@DynamoDBAttribute(attributeName = "VERSION")
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}


	@DynamoDBAttribute(attributeName = "VERSION_TYPE")
	public String getVersionType() {
		return versionType;
	}
	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}

	
	@DynamoDBAttribute(attributeName = "SOURCE_URL")
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}


	@DynamoDBAttribute(attributeName = "HP_ID")
	public String getHpID() {
		return hpID;
	}
	public void setHpID(String hpID) {
		this.hpID = hpID;
	}


	@DynamoDBAttribute(attributeName = "HP_TOPIC")
	public String getHpTopic() {
		return hpTopic;
	}
	public void setHpTopic(String hpTopic) {
		this.hpTopic = hpTopic;
	}
	
	
	@DynamoDBAttribute(attributeName = "S3_URL")
	public String getS3URL() {
		return s3URL;
	}
	public void setS3URL(String s3url) {
		s3URL = s3url;
	}


	@DynamoDBAttribute(attributeName = "EDGE_URL")
	public String getEdgeURL() {
		return edgeURL;
	}
	public void setEdgeURL(String edgeURL) {
		this.edgeURL = edgeURL;
	}

	
	@DynamoDBAttribute(attributeName = "UPLOADED_BY")
	public String getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}


	@DynamoDBAttribute(attributeName = "UPLOAD_DATE")
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	@DynamoDBAttribute(attributeName = "S3_BUCKET_NAME")
	public String getS3BucketName() {
		return s3BucketName;
	}
	public void setS3BucketName(String s3BucketName) {
		this.s3BucketName = s3BucketName;
	}

	@DynamoDBAttribute(attributeName = "S3_KEY_NAME")
	public String getS3KeyName() {
		return s3KeyName;
	}
	public void setS3KeyName(String s3KeyName) {
		this.s3KeyName = s3KeyName;
	}


	@DynamoDBAttribute(attributeName = "CHECKSUM_TYPE")
	public String getChecksumType() {
		return checksumType;
	}
	public void setChecksumType(String checksumType) {
		this.checksumType = checksumType;
	}


	@DynamoDBAttribute(attributeName = "CHECKSUM")
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}


	@DynamoDBAttribute(attributeName = "STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
	
	
	
}
