package com.wio.common.rulebook;

import com.deliveredtechnologies.rulebook.FactMap;
import com.deliveredtechnologies.rulebook.NameValueReferableMap;
import com.deliveredtechnologies.rulebook.lang.RuleBookBuilder;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.wio.common.config.request.DeviceConfigurationRequest;

public class WIOConfigRulebook 
{
	
	public static void main(String args[])
	{
		NameValueReferableMap factMap = new FactMap();
		factMap.setValue("username", "rajesh");
		factMap.setValue("password", "pwd");
		
		sampleValidation(factMap);
	}
	
	
	
	
	public static void sampleValidation(NameValueReferableMap facts) 
	{	
		RuleBook ruleBook = RuleBookBuilder.create()
				  .addRule(rule -> rule.withFactType(String.class)
				    .when(f -> f.getValue("username").equals("rajesh") && f.getValue("password").equals("pwd"))
				    .then(f -> 
				    {
				    	System.out.println("Success");
				    	System.out.println("Success");
				    }))
				  .build();
				
		ruleBook.run(facts);		    
		ruleBook.getResult().isPresent();
	}
	
	
	
	
	public RulebookResult validate(DeviceConfigurationRequest config) 
	{
		RulebookResult result = new RulebookResult();
		
		boolean success = true;
		
		
		
		if(success)
		{
			result.setResult(true);
			result.setMessage("All values successfully validated !");			
		}
		else
		{
			result.setResult(false);
			result.setParam("SSID");
			result.setMessage("Wifi SSID is not in proper format !");			
		}		
		
		return result;
	}
	
	
	

}
