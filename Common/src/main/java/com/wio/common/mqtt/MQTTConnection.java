package com.wio.common.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MQTTConnection {	
	/*private static String serverURI = SystemConfigMessages.MQTT_BROKER_URI;
	private static String username = SystemConfigMessages.MQTT_BROKER_USERNAME;
	private static String password = SystemConfigMessages.MQTT_BROKER_PASSWORD;*/
	
	//private static String serverURI = "tcp://52.23.182.90:1883";
	private static String serverURI = System.getenv("MQTT_BROKER_URI");
	private static String username = System.getenv("MQTT_BROKER_USERNAME");
	private static String password = System.getenv("MQTT_BROKER_PASSWORD");
	
	public MqttClient getConnection(String clientId, int keepAliveInterval, boolean automaticReconnect, 
			boolean cleanSession) {
		MemoryPersistence persistence = new MemoryPersistence();		
		MqttClient client = null;
		
		try {
			
			client = new MqttClient(serverURI, clientId, persistence);
			
			MqttConnectOptions connectOptions = new MqttConnectOptions();
	            
            connectOptions.setCleanSession(cleanSession);
            connectOptions.setUserName(username);
            connectOptions.setPassword(password.toCharArray());
            
            if(keepAliveInterval !=0)
            	connectOptions.setKeepAliveInterval(keepAliveInterval);
            
            if(automaticReconnect)
            	connectOptions.setAutomaticReconnect(automaticReconnect);
            	


            //this.client.setCallback(this);
            
            System.out.println("Client ID:: "+clientId);
            System.out.println("Connecting to broker: "+serverURI);
            
            client.connect(connectOptions);
            
            System.out.println("Connected");
		} 	catch (MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return client;
	}
}