package com.wio.common.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MQTTPublisher {
	public static String publishMessage(MqttClient connection, String messageContent, String topic, int qos,
			boolean isRetained) {
		
		if(publishMessage(connection,messageContent.getBytes(),topic,qos,isRetained)) {
			return "SUCCESS";
		}else {
			return "FAILURE";
		}
	}
	
	public static String publishMessage(MqttClient connection, MqttMessage messageContent, String topic, int qos,
			boolean isRetained) {
		
		if(publishMessage(connection,messageContent.getPayload(),topic,qos,isRetained)) {
			return "SUCCESS";
		}else {
			return "FAILURE";
		}
		
	}
	
	public static boolean publishMessage(MqttClient connection, byte[] mesg, String topic, int qos,
			boolean isRetained) {
		
		boolean flag = false;
		try {
			System.out.println("Going to Publish message.");

			MqttMessage message = new MqttMessage(mesg);
			message.setQos(qos);
			message.setRetained(isRetained);

			if (isRetained) {
				connection.publish(topic, message.getPayload(), qos, isRetained);
			}else {
				connection.publish(topic, message);
			}
			flag = true;
			System.out.println("Message published.");
			connection.disconnect();
			System.out.println("Disconnected.");
			return flag;
		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
			flag = false;
		}
		
		return flag ;
	}

}
